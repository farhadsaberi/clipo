import 'package:clipo/features/home/provider/main_provider.dart';
import 'package:clipo/features/language/language_provider.dart';
import 'package:clipo/res/strings.dart';
import 'package:clipo/routers/router_manger.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

import 'features/login/ui/login_provider.dart';
import 'generated/l10n.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (BuildContext context) => LoginProvider(),
          ),
          ChangeNotifierProvider(
            create: (BuildContext context) => MainProvider(),
          ),
          ChangeNotifierProvider(
            create: (BuildContext context) => LanguageProvider(),
          ),
        ],
        child: Consumer<LanguageProvider>(builder: (context, model, child) {
          return MaterialApp(
            title: Strings.appName,
            onGenerateRoute: RouterManager.generateRoute,
            initialRoute: RouteName.splash,
            debugShowCheckedModeBanner: false,
            locale: model.appLocal,
            supportedLocales: [
              Locale('en', ''),
              Locale('ar', ''),
            ],
            localizationsDelegates: [
              S.delegate,
              GlobalCupertinoLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
          );
        }));
  }
}


