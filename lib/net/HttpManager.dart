import 'dart:io';

import 'package:clipo/entity/base_response.dart';
import 'package:dio/dio.dart';

import 'dart:collection';

import 'code.dart';
import 'interceptors/error_interceptor.dart';
import 'interceptors/header_interceptor.dart';
import 'interceptors/log_interceptor.dart';
import 'interceptors/response_interceptor.dart';
import 'interceptors/token_interceptor.dart';
import 'result_data.dart';

class HttpManager {
  static const CONTENT_TYPE_JSON = "application/json";
  static const CONTENT_TYPE_FORM = "application/x-www-form-urlencoded";

  static const POST = "post";
  static const GET = "get";
  static const PUT = "put";
  static const DELETE = "delete";

  Dio _dio = new Dio();

  Dio getDio() => _dio;

  final TokenInterceptors _tokenInterceptors = new TokenInterceptors();

  HttpManager() {
    _dio.interceptors.add(new HeaderInterceptors());

    _dio.interceptors.add(_tokenInterceptors);

    _dio.interceptors.add(new LogsInterceptors());

    _dio.interceptors.add(new ErrorInterceptors(_dio));

    _dio.interceptors.add(new ResponseInterceptors(_dio));
  }

  Future<ResultData> netFetch(
      url, params, Map<String, dynamic> header, Options option,
      {noTip = false, File uploadFile, Function(int) progressCallback}) async {
    Map<String, dynamic> headers = new HashMap();
    if (header != null) {
      headers.addAll(header);
    }

    if (option != null) {
      option.headers = headers;
    } else {
      option = new Options(method: "get");
      option.headers = headers;
    }

    if (uploadFile != null) {
      option.method = "POST";
    }

    resultError(DioError e) {
      Response errorResponse;
      if (e.response != null) {
        errorResponse = e.response;
      } else {
        errorResponse = new Response(statusCode: -10);
      }
      if (e.type == DioErrorType.CONNECT_TIMEOUT ||
          e.type == DioErrorType.RECEIVE_TIMEOUT) {
        errorResponse.statusCode = Code.NETWORK_TIMEOUT;
      }else if(errorResponse.statusCode>0 && errorResponse.statusCode>=400){
        BaseResponse baseResponse = BaseResponse.fromJson(errorResponse.data);
        return  ResultData(
            baseResponse.translatedMessage,
            false,
            errorResponse.statusCode);
      }
      return new ResultData(
          Code.errorHandleFunction(errorResponse.statusCode,
              e.response != null ? e.message : "Failed host lookup", noTip),
          false,
          errorResponse.statusCode);
    }

    Response response;
    try {
      response = await _dio.request(
        url,
        data: uploadFile != null
            ? FormData.fromMap({
          "File": await MultipartFile.fromFile(
            uploadFile.path,
          )
        })
            : params,
        options: option,
        onSendProgress: uploadFile != null
            ? (int sent, int total) {
          progressCallback(((sent / total) * 100).toInt());
        }
            : null,
      );
    } on DioError catch (e) {
      return resultError(e);
    }
    if (response.data is DioError) {
      return resultError(response.data);
    }
    return response.data;
  }

  clearAuthorization() {
    _tokenInterceptors.clearAuthorization();
  }

  getAuthorization() async {
    return _tokenInterceptors.getAuthorization();
  }
}

final HttpManager httpManager = new HttpManager();

class NotSuccessException implements Exception {
  String message;


  @override
  String toString() {
    return 'NotExpectedException{respData: $message}';
  }
}

class UnAuthorizedException implements Exception {
  const UnAuthorizedException();

  @override
  String toString() => 'UnAuthorizedException';
}

