import 'package:clipo/utils/utils.dart';
import 'package:dio/dio.dart';

class HeaderInterceptors extends InterceptorsWrapper {
  @override
  onRequest(RequestOptions options) async {
    options.connectTimeout = 30000;
    options.headers["lang"] = Utils.getLanguage();
    return options;
  }
}
