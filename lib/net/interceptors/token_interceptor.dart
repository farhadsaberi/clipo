import 'package:clipo/config/constant.dart';
import 'package:clipo/local/local_storage.dart';
import 'package:dio/dio.dart';

class TokenInterceptors extends InterceptorsWrapper {
  String _token;

  @override
  onRequest(RequestOptions options) async {
    final String accessToken = await getAuthorization();
    if (accessToken != null && accessToken.isNotEmpty) {
      options.headers['Authorization'] = 'Token $accessToken';
    }
    return options;
  }

  @override
  onResponse(Response response) async {
    try {
      var responseJson = response.data;
      if (response.statusCode == 200 && responseJson["Data"] != null) {
        if (responseJson["Data"]["AccessToken"] != null) {
          print("data is  $response");
          _token = responseJson["Data"]["AccessToken"];
        }
      }
    } catch (e) {
      print(e);
    }
    return response;
  }

  clearAuthorization() {
    this._token = null;
    LocalStorage.remove(Constant.TOKEN_KEY);
  }

  getAuthorization() async {
    String token = await LocalStorage.get(Constant.TOKEN_KEY);
    if (token != null) {
      this._token = token;
      return token;
    } else {}
  }
}
