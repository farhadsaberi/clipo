import 'package:clipo/config/constant.dart';

class Api {
  static const String host = Constant.API;

  static const String CONTENT_SERIE = "";

  static String home() {
    return "$host/home/app";
  }

  static String contentSerieParts(String content, int page, int pageSize) {
    return "$host/contentSerie/$content/parts?page=$page&pageSize=$pageSize";
  }

  static String contentSerie(String content, int page, int pageSize) {
    return "$host/home/contentserie/$content?page=$page&pageSize=$pageSize";
  }

  static String content(String content, {int contentId}) {
    return "$host/content/$content/all?${contentId != null ? "contentId=${contentId.toInt()}&excludedContentId=${contentId.toInt()}" : ""}";
  }

  static String category() {
    return "$host/category/all";
  }

  static String subCategory(
      String content, bool isFree, int page, int pageSize) {
    return "$host/contentserie/$content?isFree=$isFree&page=$page&pageSize=$pageSize";
  }

  static String search(String content, int type, int page, int pageSize) {
    return "$host/search/?query=$content&type=$type&page=$page&pageSize=$pageSize";
  }

  static String loginWithPassword() {
    return "$host/auth/login/username?remindme=true";
  }
  static String loginWithMsinsdnAndPassword() {
    return "$host/auth/login/msisdn?remindme=true";
  }

  static String register() {
    return "$host/auth/otp";
  }

  static String verificationCode() {
    return "$host/auth/login/Otp?remindme=true";
  }

  static String forgotPassword() {
    return "$host/auth/resendpassword";
    return "$host/auth/login/password/forgot";
  }

  static String addBookmark(String contentId) {
    return "$host/content/$contentId/bookmark";
  }

  static String removeBookmark(String contentId) {
    return "$host/content/$contentId/bookmark/remove";
  }

  static String postContentSeriesRate() {
    return "$host/contentserie/Rate";
  }

  static String changePassword() {
//    return "$host/auth/login/password/change";
    return "$host/auth/login/password/change2";
  }

  static String resetPassword() {
    return "$host/auth/login/password/reset";
  }

  static String getProfile() {
    return "$host/profile";
  }

  static String setNickName() {
    return "$host/profile/nickname/set";
  }

  static String getBookmarks(int page, int pageSize) {
    return "$host/profile/bookmarks?page=$page&pageSize=$pageSize";
  }

  static String setComment(String contentSerieId) {
    return "$host/contentSerie/$contentSerieId/comment/post";
  }

  static String getContentSerieComments(String contentSerieId,int page, int pageSize) {
    return "$host/contentSerie/$contentSerieId/comments?page=$page&pageSize=$pageSize";
  }

  static String unSubscribe() {
    return "$host/purchase/cancel";
  }

}
