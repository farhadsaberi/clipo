import 'dart:io';

import 'package:clipo/generated/l10n.dart';
import 'package:clipo/net/HttpManager.dart';
import 'package:clipo/net/result_data.dart';
import 'package:clipo/utils/toast.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'view_state.dart';

export 'view_state.dart';

class BaseViewState with ChangeNotifier {
  bool _disposed = false;

  ViewState _viewState;

  BaseViewState({ViewState viewState})
      : _viewState = viewState ?? ViewState.success {
    debugPrint('ViewStateModel---constructor--->$runtimeType');
  }

  ViewState get viewState => _viewState;

  set viewState(ViewState viewState) {
    _viewStateError = null;
    _viewState = viewState;
    notifyListeners();
  }

  ViewStateError _viewStateError;

  ViewStateError get viewStateError => _viewStateError;

  bool get isLoading => viewState == ViewState.refresh;

  bool get isSuccess => viewState == ViewState.success;

  bool get isEmpty => viewState == ViewState.empty;

  bool get isError => viewState == ViewState.error;

  void setSuccess() {
    viewState = ViewState.success;
  }

  void setRefresh() {
    viewState = ViewState.refresh;
  }

  void setEmpty() {
    viewState = ViewState.empty;
  }

  void setError(dynamic e, {String message}) {
    ViewStateErrorType errorType = ViewStateErrorType.defaultError;
    if (e is ResultData) {
      if (e.code == 401) {
        errorType = ViewStateErrorType.unauthorizedError;
      } else if (e.code == 500) {
        message = e.data;
      } else if (e.code < 0) {
        errorType = ViewStateErrorType.networkTimeOutError;
        message = e.data;
      }else if(e.code == 406){
        message = e.data;
      }else{
        message = e.data;
      }
    } else{
      print("farhad error ${e}");
    }

    viewState = ViewState.error;
    _viewStateError = ViewStateError(
      errorType,
      message: message,
      errorMessage: e.toString(),
    );
    printErrorStack(e, e.data);
    onError(viewStateError);
  }

  void onError(ViewStateError viewStateError) {}

  showErrorMessage(context, {String message}) {
    if (viewStateError != null || message != null) {
      if (viewStateError.isNetworkTimeOut) {
        message ??= S.of(context).view_state_message_error;
      } else {
        message ??= viewStateError.message;
      }
      Future.microtask(() {
        Toast.show(message);
      });
    }
  }

  @override
  String toString() {
    return 'BaseModel{_viewState: $viewState, _viewStateError: $_viewStateError}';
  }

  @override
  void notifyListeners() {
    if (!_disposed) {
      super.notifyListeners();
    }
  }

  @override
  void dispose() {
    _disposed = true;
    debugPrint('view_state_model dispose -->$runtimeType');
    super.dispose();
  }
}

printErrorStack(e, s) {
  debugPrint('''
<-----↓↓↓↓↓↓↓↓↓↓-----error-----↓↓↓↓↓↓↓↓↓↓----->
$e
<-----↑↑↑↑↑↑↑↑↑↑-----error-----↑↑↑↑↑↑↑↑↑↑----->''');
  if (s != null) debugPrint('''
<-----↓↓↓↓↓↓↓↓↓↓-----trace-----↓↓↓↓↓↓↓↓↓↓----->
$s
<-----↑↑↑↑↑↑↑↑↑↑-----trace-----↑↑↑↑↑↑↑↑↑↑----->
    ''');
}
