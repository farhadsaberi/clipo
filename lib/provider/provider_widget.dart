import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomProviderWidget<T extends ChangeNotifier> extends StatefulWidget {
  final ValueWidgetBuilder<T> builder;
  final T provider;
  final Widget child;
  final Function(T model) onProviderReady;
  final bool reuseExisting;

  CustomProviderWidget({
    Key key,
    @required this.builder,
    @required this.provider,
    this.child,
    this.onProviderReady,
    this.reuseExisting = false,
  }) : super(key: key);

  _CustomProviderWidgetState<T> createState() => _CustomProviderWidgetState<T>();
}

class _CustomProviderWidgetState<T extends ChangeNotifier>
    extends State<CustomProviderWidget<T>> {
  T provider;

  @override
  void initState() {
    provider = widget.provider;
//    if (widget.onProviderReady != null) {
//      widget.onProviderReady(provider);
//    }
    widget.onProviderReady?.call(provider);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.reuseExisting) {
      return ChangeNotifierProvider<T>.value(
        value: provider,
        child: Consumer<T>(
          builder: widget.builder,
          child: widget.child,
        ),
      );
    }

    return ChangeNotifierProvider(
      create: (context) => provider,
      child: Consumer<T>(
        builder: widget.builder,
        child: widget.child,
      ),
    );
  }
}
