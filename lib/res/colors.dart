import 'package:flutter/material.dart';
class ColorRes {
  ColorRes._();

  static const Color app_main = purpleColor;
  static const Color dark_app_main = purpleColor;

  static const Color bg_color = Color(0xFFF5F5F5);
  static const Color dark_bg_color = Color(0xFF18191A);

  static const Color withe = Color(0xFFFFFFFF);
  static const Color dark_material_bg = Color(0xFF303233);

  static const Color text = Color(0xFF333333);
  static const Color black_text = Color(0xFF000000);
  static const Color dark_text = Color(0xFFB8B8B8);


  static const Color circleBackground = Color(0xffC4C4C4);
  static const Color lightTextColor = Color(0xff007AFF);
  static const Color blueTextColor = Color(0xff007AFF);
  static const Color purpleColor = Color(0xff7A16C8);


  static const Color text_gray = Color(0xFF9E9E9E);
  static const Color dark_text_gray = Color(0xFF666666);

  static const Color bg_gray = Color(0xFFF6F6F6);
  static const Color bg_gray_light = Color(0xFFF5F5F5);

  static const Color text_gray_light = Color(0xFFcccccc);
  static const Color dark_button_text = Color(0xFFF2F2F2);

  static const Color bg_gray_ = Color(0xFFFAFAFA);
  static const Color dark_bg_gray_ = Color(0xFF242526);

  static const Color text_disabled = Color(0xFFD4E2FA);
  static const Color dark_text_disabled = Color(0xFFCEDBF2);

  static const Color button_disabled = Color(0xFF96BBFA);
  static const Color dark_button_disabled = Color(0xFF83A5E0);

  static const Color unselected_item_color = Color(0xffbfbfbf);
  static const Color dark_unselected_item_color = Color(0xFF4D4D4D);


  static const Color appbar_color = Color(0xffE5E5E5);
}
