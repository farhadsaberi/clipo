import 'package:clipo/config/constant.dart';
import 'package:clipo/entity/country_item.dart';
import 'package:clipo/utils/theme_utils.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_actions/keyboard_actions_config.dart';
import 'package:keyboard_actions/keyboard_actions_item.dart';
import 'package:url_launcher/url_launcher.dart';

class Utils {
  static KeyboardActionsConfig getKeyboardActionsConfig(
      BuildContext context, List<FocusNode> list) {
    return KeyboardActionsConfig(
      keyboardBarColor: ThemeUtils.getKeyboardActionsColor(context),
      nextFocus: true,
      actions: List.generate(
          list.length,
          (i) => KeyboardActionsItem(
                focusNode: list[i],
                toolbarButtons: [
                  (node) {
                    return GestureDetector(
                      onTap: () => node.unfocus(),
                      child: const Padding(
                        padding: EdgeInsets.only(right: 16.0),
                        child: Text('shut down'),
                      ),
                    );
                  },
                ],
              )),
    );
  }

  static String validateMobile(String mobile) {
    Pattern pattern = "^((\\+|00)(\\d{1,3})[\\s-]?)?(\\d{11})\$";
    RegExp regExp = RegExp(pattern);
    if (regExp.hasMatch(mobile)) {
      return null;
    }
    return 'Invalidate mobile';
  }

  static String removeFirstCharactersMobile(String mobile) {
//    if (mobile.startsWith("0")) {
//      return mobile.substring(1);
//    }
    return mobile;
  }

  static void openLink(String url) async {
    if (await canLaunch(url) != null) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static List<CountryItem> countryItems() {
    return [
      CountryItem(1, "+201", "eg"),
      CountryItem(2, "05", "ae"),
//      CountryItem(3, "+966", "sa"),
    ];
  }

  static String getLanguage() {
    switch (langID) {
      case 1:
        return "en-US";
      case 2:
        return "ar-SA";
    }
    return "en-US";
  }

  static String getLanguageWebsite() {
    switch (langID) {
      case 1:
        return "en";
      case 2:
        return "ar";
    }
    return "en";
  }
}
