

import 'package:clipo/res/colors.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart' as FlutterToast;

class Toast {
  static void show(String msg) {
    if (msg == null) {
      return;
    }
    FlutterToast.Fluttertoast.showToast(
      msg: msg,
      toastLength: FlutterToast.Toast.LENGTH_SHORT,
      backgroundColor: ColorRes.dark_bg_gray_,
      textColor: Colors.white,
    );
  }

}
