import 'package:clipo/config/constant.dart';
import 'package:clipo/entity/country_item.dart';
import 'package:clipo/res/gaps.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';

import '../utils.dart';
import 'load_image.dart';

class CountryDropdown extends StatefulWidget {
  final ValueSetter<CountryItem> onSubmitPressed;

  CountryDropdown({this.onSubmitPressed});

  @override
  _CountryDropdownState createState() => _CountryDropdownState();
}

class _CountryDropdownState extends State<CountryDropdown> {
  List<DropdownMenuItem<CountryItem>> _dropdownMenuItems;

  CountryItem _selectedItem;

  @override
  void initState() {
    super.initState();
    _dropdownMenuItems = buildDropDownMenuItems(Utils.countryItems());
    _selectedItem = _dropdownMenuItems[0].value;
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButton<CountryItem>(
        underline: SizedBox(),
        value: _selectedItem,
        items: _dropdownMenuItems,
        onChanged: (value) {
          widget.onSubmitPressed(value);
          setState(() {
            _selectedItem = value;
          });
        });
  }

  List<DropdownMenuItem<CountryItem>> buildDropDownMenuItems(List listItems) {
    List<DropdownMenuItem<CountryItem>> items = List();
    for (CountryItem listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              LoadImage(
                Constant.assetsImages + "flags/" + listItem.path,
                width: ScreenUtil().setWidth(40),
                height: ScreenUtil().setWidth(20),
                fit: BoxFit.cover,
              ),
              Gaps.hGap10,
              Text(listItem.name,
                  style: TextStyle(fontSize: ScreenUtil().setSp(14))),
            ],
          ),
          value: listItem,
        ),
      );
    }
    return items;
  }
}
