import 'dart:async';

import 'package:clipo/entity/content_response.dart';
import 'package:clipo/features/login/ui/register_screen.dart';
import 'package:clipo/features/player/player_provider.dart';
import 'package:clipo/generated/l10n.dart';
import 'package:clipo/res/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';

import '../dialog_helper.dart';
import '../navigator.dart';
import 'load_image.dart';

class PlayerWidget extends StatefulWidget {
  @override
  __PlayerState createState() => __PlayerState();
}

class __PlayerState extends State<PlayerWidget> {
  String videoUrl = "";
  bool fullscreened = false;

  final Completer<InAppWebViewController> _controller =
  Completer<InAppWebViewController>();
  InAppWebViewController _webViewController;

  @override
  Widget build(BuildContext context) {
    return Selector<PlayerProvider, Tuple4<String, PlayLink, bool, String>>(
        builder: (context, data, child) {
          String base_video ="http://vivideo-app.com/Content/play?url=";
          if (data.item2.link != videoUrl ) {
            videoUrl = base_video+ data.item2.link;
            if (_webViewController!=null) {
              _webViewController.loadUrl(url:videoUrl);
            }
            if (!data.item3) {
              videoUrl = "";
            }
          }
          if (data.item3) {
            fullscreened=true;
            return AspectRatio(
              aspectRatio: 16/9,
              child: InAppWebView(
                initialOptions: InAppWebViewGroupOptions(
                  android: AndroidInAppWebViewOptions(
                    useWideViewPort: false,
                  ),
                  ios: IOSInAppWebViewOptions(
                    enableViewportScale: false,
                  ),
                  crossPlatform: InAppWebViewOptions(
                    debuggingEnabled: true,
                    javaScriptEnabled: true,
                  ),
                ),

                onWebViewCreated: (InAppWebViewController webViewController) {
                  _controller.complete(webViewController);
                  _webViewController = webViewController;

                  if (data.item3) {
                    _webViewController.loadUrl(url:base_video+data.item2.link);
                  }
                },
                onLoadStart: (InAppWebViewController webViewController,String url) {
                  _webViewController = webViewController;
                  print('Page started loading: $url');
                },
                onLoadStop: (InAppWebViewController webViewController,String url) {
                  _webViewController = webViewController;
                  print('Page finished loading: $url');
                },

              ),
            );
          }
          else {
            fullscreened=false;
            return Container(
              child: InkWell(
                onTap: () async {
                  if (await DialogHelper.showLoginDialog(
                      context,
                      S.of(context).need_to_sign_in,
                      S.of(context).confirm,
                      S.of(context).cancel)) {
                    NavigatorUtils.push(context,
                        withNavBar: false,
                        screen: RegisterScreen(
                          disableAction: true,
                        ));
                  }
                },
                child: AspectRatio(
                  aspectRatio: 16 / 9,
                  child: Stack(
                    children: <Widget>[
                      LoadImage(
                        data.item4,
                        fit: BoxFit.fill,
                        width: double.infinity,
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Icon(
                          Icons.play_circle_outline,
                          color: ColorRes.withe,
                          size: 80,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }
        },
        selector: (buildContext, provider) => Tuple4(provider.imageVideo,
            provider.latestPlayer, provider.canPlay, provider.imageVideo));
  }
}
