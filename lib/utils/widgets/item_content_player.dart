
import 'package:clipo/res/colors.dart';
import 'package:clipo/res/gaps.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';

import '../image_utils.dart';

class ItemContentPlayer extends StatelessWidget {
  final dynamic data;
  final Function onTap;

  const ItemContentPlayer({@required this.data, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        elevation: 3,
        margin: EdgeInsets.symmetric(
          vertical: 8.0,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            8.0,
          ),
        ),
        child: Column(
          children: <Widget>[
            Container(
              width: ScreenUtil().setWidth(160),
              height: ScreenUtil().setHeight(70),
              margin: EdgeInsets.only(left: 2, right: 2, top: 4),
              decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.circular(ScreenUtil().setWidth(7)),
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: ImageUtils.getImageProvider(data.imageUrl),
                ),
              ),
            ),
            Gaps.vGap12,
            Row(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(5.0),
                  child: Container(
                    margin: EdgeInsets.only(left: 5, right: 6),
                    color: ColorRes.bg_gray_light,
                    child: Icon(
                      Icons.play_arrow,
                      color: ColorRes.purpleColor,
                    ),
                  ),
                ),
                Container(
                  //alignment: Alignment.bottomCenter,
                  width: ScreenUtil().setWidth(140),
                  padding: EdgeInsets.all(ScreenUtil().setWidth(5)),
                  child: Text(
                    data.title,
                    maxLines: 2,
                    //textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(13),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
