import 'package:clipo/generated/l10n.dart';
import 'package:clipo/res/colors.dart';
import 'package:clipo/res/dimens.dart';
import 'package:clipo/res/gaps.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import '../theme_utils.dart';

class SearchBar extends StatefulWidget implements PreferredSizeWidget {
  const SearchBar({
    Key key,
    this.hintText = '',
    this.backImg = 'assets/images/ic_back_black.png',
    this.onPressed,
    this.autoFocus = false,
    @required this.focusNode,
  }) : super(key: key);

  final String backImg;
  final String hintText;
  final bool autoFocus;
  final FocusNode focusNode;
  final Function(String) onPressed;

  @override
  _SearchBarState createState() => _SearchBarState();

  @override
  Size get preferredSize => const Size.fromHeight(48.0);
}

class _SearchBarState extends State<SearchBar> {
  final TextEditingController _controller = TextEditingController();
  @override
  void dispose() {
    widget.focusNode.dispose();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bool isDark = ThemeUtils.isDark(context);
    final Color iconColor =
        isDark ? ColorRes.text_gray : ColorRes.text_gray;

    Widget textField = Expanded(
      child: Container(
        height: 40.0,
        decoration: BoxDecoration(
          color: isDark ? ColorRes.withe : ColorRes.withe,
          borderRadius: BorderRadius.circular(4.0),
        ),
        child: TextField(
          autofocus: widget.autoFocus,
          controller: _controller,
          focusNode: widget.focusNode,
          showCursor: true,
          maxLines: 1,
          onChanged: (text){
            setState(() {
            });
          },
          textInputAction: TextInputAction.search,
          onSubmitted: (val) {
            widget.focusNode.unfocus();
            widget.onPressed(val);
          },
          decoration: InputDecoration(
            border: InputBorder.none,
            icon: Icon(
              Icons.search,
              color: iconColor,
            ),
            hintText: widget.hintText,
            suffixIcon: GestureDetector(
              child: Semantics(
                label: 'Empty',
                child: Visibility(
                  visible: _controller.text.length > 0,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 16.0, top: 8.0, bottom: 8.0),
                    child: Icon(Icons.remove_circle, color: iconColor),
                  ),
                ),
              ),
              onTap: () {
                setState(() {
                });
                /// https://github.com/flutter/flutter/issues/35848
                SchedulerBinding.instance.addPostFrameCallback((_) {
                  _controller.text = '';
                  _controller.clear();
//                  FocusScope.of(context).unfocus();
                });
              },
            ),
          ),
        ),
      ),
    );

    Widget search = Theme(
      data: Theme.of(context).copyWith(
        buttonTheme: ButtonThemeData(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          height: 32.0,
          minWidth: 44.0,
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4.0),
          ),
        ),
      ),
      child: FlatButton(
        textColor: isDark ? ColorRes.dark_button_text : Colors.white,
        color: isDark ? ColorRes.purpleColor : ColorRes.purpleColor,
        onPressed: () {
          setState(() {
          });
          widget.focusNode.unfocus();
          widget.onPressed(_controller.text);
        },
        child: Text(S.of(context).search,
            style: TextStyle(fontSize: Dimens.font_sp14)),
      ),
    );

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: isDark ? SystemUiOverlayStyle.light : SystemUiOverlayStyle.dark,
      child: Material(
        color: ThemeUtils.getBackgroundColor(context),
        child: SafeArea(
          child: Container(
            child: Row(
              children: <Widget>[
                textField,
                Gaps.hGap8,
                search,
                Gaps.hGap16,
              ],
            ),
          ),
        ),
      ),
    );
  }
}

