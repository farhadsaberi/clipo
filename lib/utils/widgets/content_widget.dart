import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:flutter/material.dart';

class ContentWidget extends StatelessWidget {
  final String title;
  final Widget child;

  const ContentWidget({@required this.title, @required this.child});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: title,
      ),
      body: SingleChildScrollView(
        child: child,
      ),
    );
  }
}
