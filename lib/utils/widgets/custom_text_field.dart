import 'dart:async';

import 'package:clipo/config/constant.dart';
import 'package:clipo/res/colors.dart';
import 'package:clipo/res/dimens.dart';
import 'package:clipo/res/gaps.dart';
import 'package:clipo/utils/image_utils.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextField extends StatefulWidget {
  const CustomTextField(
      {Key key,
      @required this.controller,
      this.maxLength = 16,
      this.autoFocus = false,
      this.keyboardType = TextInputType.text,
      this.hintText = '',
      this.focusNode,
      this.isInputPwd = false,
      this.getVCode,
      this.vCodeText,
      })
      : super(key: key);

  final TextEditingController controller;
  final int maxLength;
  final bool autoFocus;
  final TextInputType keyboardType;
  final String hintText;
  final String vCodeText;
  final FocusNode focusNode;
  final bool isInputPwd;
  final Future<bool> Function() getVCode;


  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool _isShowPwd = false;
  bool _isShowDelete = false;
  bool _clickable = true;

  final int _second = 30;
  int _currentSecond;
  StreamSubscription _subscription;

  @override
  void initState() {
    _isShowDelete = widget.controller.text.isEmpty;
    widget.controller.addListener(isEmpty);
    super.initState();
  }

  void isEmpty() {
    bool isEmpty = widget.controller.text.isEmpty;
    if (isEmpty != _isShowDelete) {
      setState(() {
        _isShowDelete = isEmpty;
      });
    }
  }

  @override
  void dispose() {
    widget.controller?.removeListener(isEmpty);
    _subscription?.cancel();
    super.dispose();
  }

  Future _getVCode() async {
    bool isSuccess = await widget.getVCode();
    if (isSuccess != null && isSuccess) {
      setState(() {
        _currentSecond = _second;
        _clickable = false;
      });
      _subscription = Stream.periodic(const Duration(seconds: 1), (int i) => i)
          .take(_second)
          .listen((int i) {
        setState(() {
          _currentSecond = _second - i - 1;
          _clickable = _currentSecond < 1;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    final bool isDark = themeData.brightness == Brightness.dark;

    final TextField textField = TextField(
      focusNode: widget.focusNode,
      maxLength: widget.maxLength,
      obscureText: widget.isInputPwd ? !_isShowPwd : false,
      autofocus: widget.autoFocus,
      controller: widget.controller,
      textInputAction: TextInputAction.done,
      keyboardType: widget.keyboardType,
      inputFormatters: (widget.keyboardType == TextInputType.number ||
              widget.keyboardType == TextInputType.phone)
          ? [WhitelistingTextInputFormatter(RegExp('[0-9]'))]
          : [BlacklistingTextInputFormatter(RegExp('[\u4e00-\u9fa5]'))],
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(vertical: 16.0),
        hintText: widget.hintText,
        counterText: '',
        hintStyle: TextStyle(fontSize: 14),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: themeData.primaryColor,
            width: 0.8,
          ),
        ),
// Todo fix border
//        enabledBorder: UnderlineInputBorder(
//          borderSide: BorderSide(
//            color: Theme.of(context).dividerTheme.color,
//            width: 0.8,
//          ),
//        ),
      ),
    );

    Widget clear = GestureDetector(
      child: Container(
        width: 18,
        height: 40,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: ImageUtils.getAssetImage(
                    Constant.assetsImages + 'login/icon_delete'),
                fit: BoxFit.fitWidth)),
      ),
      onTap: () => widget.controller.text = '',
    );

    Widget pwdVisible = GestureDetector(
      child: Container(
        width: 18,
        height: 40,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: ImageUtils.getAssetImage(_isShowPwd
                    ? Constant.assetsImages + 'login/icon_display'
                    : Constant.assetsImages + 'login/icon_hide'),
                fit: BoxFit.fitWidth)),
      ),
      onTap: () {
        setState(() {
          _isShowPwd = !_isShowPwd;
        });
      },
    );

    Widget getVCodeButton = Theme(
      data: Theme.of(context).copyWith(
        buttonTheme: const ButtonThemeData(
          padding: EdgeInsets.symmetric(horizontal: 8.0),
          height: 26.0,
          minWidth: 76.0,
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        ),
      ),
      child: FlatButton(
        onPressed: _clickable ? _getVCode : null,
        textColor: themeData.primaryColor,
        color: Colors.transparent,
        disabledTextColor: isDark ? ColorRes.dark_text : Colors.white,
        disabledColor:
            isDark ? ColorRes.dark_text_gray : ColorRes.text_gray_light,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(1.0),
          side: BorderSide(
            color: _clickable ? themeData.primaryColor : Colors.transparent,
            width: 0.8,
          ),
        ),
        child: Text(
          _clickable ? widget.vCodeText ?? "" : '（$_currentSecond s）',
          style: TextStyle(fontSize: Dimens.font_sp12),
        ),
      ),
    );

    return Stack(
      alignment: Alignment.centerRight,
      children: <Widget>[
        textField,
        Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            if (_isShowDelete) Gaps.empty else clear,
            if (!widget.isInputPwd) Gaps.empty else Gaps.hGap15,
            if (!widget.isInputPwd) Gaps.empty else pwdVisible,
            if (widget.getVCode == null) Gaps.empty else Gaps.hGap15,
            if (widget.getVCode == null) Gaps.empty else getVCodeButton,
          ],
        )
      ],
    );
  }
}
