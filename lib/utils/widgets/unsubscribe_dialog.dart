import 'package:clipo/res/colors.dart';
import 'package:clipo/res/gaps.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../change_notifier_manage.dart';
import 'custom_button.dart';
import 'custom_text_field.dart';

class UnsubscribeDialog extends StatefulWidget {
  final String title;
  final String submitButton;
  final String alternativeButton;
  final VoidCallback onAlternativePressed;

  UnsubscribeDialog(
      {@required this.title,
      @required this.submitButton,
      this.alternativeButton = "",
      this.onAlternativePressed});

  @override
  _RatingDialogState createState() => new _RatingDialogState();
}

class _RatingDialogState extends State<UnsubscribeDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: ColorRes.bg_color,
      contentPadding: EdgeInsets.all(20),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(
            Icons.error,
            size: ScreenUtil().setHeight(60),
            color: Colors.redAccent,
          ),
          Gaps.vGap32,
          Text(widget.title,
              style: TextStyle(
                  color: ColorRes.black_text,
                  fontSize: 14,
                  fontWeight: FontWeight.bold)),
          Gaps.vGap15,
          Gaps.vGap64,
          CustomButton(
            onPressed:() {
              Navigator.pop(context);
              widget.onAlternativePressed();
            },
            text: widget.submitButton,
            radius: 14,
          ),
        ],
      ),
    );
  }
}
