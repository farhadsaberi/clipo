import 'package:clipo/config/constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';

Widget circleAvatarLogo(){
  return Container(
    width: ScreenUtil().setWidth(100),
    height: ScreenUtil().setWidth(100),
    child: Image.asset(Constant.assetsImages+"/logo.png"),
  );
}
