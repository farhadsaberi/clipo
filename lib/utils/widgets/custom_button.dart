import 'package:clipo/res/colors.dart';
import 'package:clipo/res/dimens.dart';
import 'package:flutter/material.dart';

import '../theme_utils.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({
    Key key,
    this.text = '',
    this.radius,
    this.child,
    @required this.onPressed,
  }) : super(key: key);

  final String text;
  final double radius;
  final Widget child;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    final bool isDark = ThemeUtils.isDark(context);
    return FlatButton(
      onPressed: onPressed,
      textColor: isDark ? ColorRes.dark_button_text : Colors.white,
      color: isDark ? ColorRes.dark_app_main : ColorRes.app_main,
      disabledTextColor:
          isDark ? ColorRes.dark_text_disabled : ColorRes.text_disabled,
      disabledColor:
          isDark ? ColorRes.dark_button_disabled : ColorRes.button_disabled,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius ?? 0)),
      child: Container(
        height: 48,
        width: double.infinity,
        alignment: Alignment.center,
        child: child ?? textWidget(),
      ),
    );
  }

  Widget textWidget() {
    return Text(
      text,
      style: TextStyle(fontSize: Dimens.font_sp18),
    );
  }
}
