library rating_dialog;

import 'package:clipo/res/gaps.dart';
import 'package:flutter/material.dart';

import '../change_notifier_manage.dart';
import 'custom_button.dart';
import 'custom_text_field.dart';

class CommentDialog extends StatefulWidget {
  final String title;
  final String hint;
  final String submitButton;
  final String alternativeButton;
  final ValueSetter<String> onSubmitPressed;
  final VoidCallback onAlternativePressed;

  CommentDialog(
      {@required this.title,
      @required this.hint,
      @required this.onSubmitPressed,
      @required this.submitButton,
      this.alternativeButton = "",
      this.onAlternativePressed});

  @override
  _RatingDialogState createState() => new _RatingDialogState();
}

class _RatingDialogState extends State<CommentDialog>
    with ChangeNotifierMixin<CommentDialog> {
  final TextEditingController _textController = TextEditingController();
  final FocusNode _nodeText1 = FocusNode();
  bool _clickable = false;

  @override
  Map<ChangeNotifier, List<VoidCallback>> changeNotifier() {
    final List<VoidCallback> callbacks = [_verify];
    return {
      _textController: callbacks,
      _nodeText1: null,
    };
  }

  void _verify() {
    final String name = _textController.text;
    bool clickable = true;
    if (name.isEmpty) {
      clickable = false;
    }
    if (clickable != _clickable) {
      setState(() {
        _clickable = clickable;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.all(20),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
      content:  Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(widget.title,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
          Gaps.vGap15,
          CustomTextField(
            focusNode: _nodeText1,
            controller: _textController,
            maxLength: 20,
            hintText: widget.hint,
          ),
          Gaps.vGap64,
          CustomButton(
            onPressed: _clickable
                ? () {
                    Navigator.pop(context);
                    widget.onSubmitPressed(_textController.text);
                  }
                : null,
            text: widget.submitButton,
            radius: 14,
          ),
        ],
      ),
    );
  }
}
