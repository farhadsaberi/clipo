import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class NavigatorUtils {
  static void push(BuildContext context,
      {bool replace = false,
      String path,
      bool clearStack = false,
      bool custom = true,
      bool withNavBar = true,
      Widget screen}) {
    if (custom) {
      pushNewScreen(
        context,
        screen: screen,
        withNavBar: withNavBar,
      );
    } else {
      Navigator.pushNamed(context, path);
    }
  }
}
