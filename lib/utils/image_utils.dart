import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ImageUtils {
  
  static ImageProvider getAssetImage(String name, {ImageFormat format = ImageFormat.png}) {
    return AssetImage(getImgPath(name, format: format));
  }

  static String getImgPath(String name, {ImageFormat format = ImageFormat.png}) {
    return '$name.${format.value}';
  }

  static ImageProvider getImageProvider(String imageUrl, {String holderImg = 'none'}) {
    if (imageUrl.isEmpty) {
      return AssetImage(getImgPath(holderImg));
    }
    return CachedNetworkImageProvider(imageUrl);
  }
}

enum ImageFormat {
  png,
  jpg,
  gif,
  svg
}

extension ImageFormatExtension on ImageFormat {
  String get value => ['png', 'jpg', 'gif','svg'][index];
}
