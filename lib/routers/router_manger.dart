import 'package:clipo/features/home/ui/main_page.dart';
import 'package:clipo/features/login/ui/forgot_password_screen.dart';
import 'package:clipo/features/login/ui/login_screen.dart';
import 'package:clipo/features/login/ui/register_screen.dart';
import 'package:clipo/features/login/ui/verification_code_screen.dart';
import 'package:clipo/features/splash/splash.dart';
import 'package:flutter/material.dart';

class RouteName {
  static const String splash = 'splash';
  static const String login = 'login';
  static const String register = 'register';
  static const String forgotPassword = 'forgotPassword';
  static const String verificationCode = 'verificationCode';
  static const String playerPage = 'playerPage';
  static const String home = '/';
}

class RouterManager {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case RouteName.login:
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case RouteName.verificationCode:
        final Map arguments = settings.arguments as Map;
        return MaterialPageRoute(
            builder: (_) => VerificationCodeScreen(
                  arguments['mobile'],
                  code: arguments['code'],
                ));
      case RouteName.forgotPassword:
        return MaterialPageRoute(builder: (_) => ForgotPasswordScreen());
      case RouteName.register:
        return MaterialPageRoute(builder: (_) => RegisterScreen());
      case RouteName.home:
        return MaterialPageRoute(builder: (_) => MainPage());
      case RouteName.splash:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }
}

class PopRoute extends PopupRoute {
  final Duration _duration = Duration(milliseconds: 300);
  Widget child;

  PopRoute({@required this.child});

  @override
  Color get barrierColor => null;

  @override
  bool get barrierDismissible => true;

  @override
  String get barrierLabel => null;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return child;
  }

  @override
  Duration get transitionDuration => _duration;
}
