// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Clipo`
  String get app_name {
    return Intl.message(
      'Clipo',
      name: 'app_name',
      desc: '',
      args: [],
    );
  }

  /// `Clipo`
  String get app_name_persian {
    return Intl.message(
      'Clipo',
      name: 'app_name_persian',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get action_settings {
    return Intl.message(
      'Settings',
      name: 'action_settings',
      desc: '',
      args: [],
    );
  }

  /// `Please check your device's internet connection.`
  String get check_connection {
    return Intl.message(
      'Please check your device\'s internet connection.',
      name: 'check_connection',
      desc: '',
      args: [],
    );
  }

  /// `Press the return key again to exit.`
  String get ask_exit {
    return Intl.message(
      'Press the return key again to exit.',
      name: 'ask_exit',
      desc: '',
      args: [],
    );
  }

  /// `Try again`
  String get try_again {
    return Intl.message(
      'Try again',
      name: 'try_again',
      desc: '',
      args: [],
    );
  }

  /// `There is a problem connecting to server. Please try again.`
  String get text_try_again {
    return Intl.message(
      'There is a problem connecting to server. Please try again.',
      name: 'text_try_again',
      desc: '',
      args: [],
    );
  }

  /// `Try again`
  String get btn_try_again {
    return Intl.message(
      'Try again',
      name: 'btn_try_again',
      desc: '',
      args: [],
    );
  }

  /// `There was a problem connecting`
  String get error_dto_default_message {
    return Intl.message(
      'There was a problem connecting',
      name: 'error_dto_default_message',
      desc: '',
      args: [],
    );
  }

  /// `unable to connect to the internet`
  String get internet_connection_exception {
    return Intl.message(
      'unable to connect to the internet',
      name: 'internet_connection_exception',
      desc: '',
      args: [],
    );
  }

  /// `submit`
  String get submit {
    return Intl.message(
      'submit',
      name: 'submit',
      desc: '',
      args: [],
    );
  }

  /// `save`
  String get save {
    return Intl.message(
      'save',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `Categorize videos`
  String get nav_item_groups {
    return Intl.message(
      'Categorize videos',
      name: 'nav_item_groups',
      desc: '',
      args: [],
    );
  }

  /// `inbox`
  String get nav_item_inbox {
    return Intl.message(
      'inbox',
      name: 'nav_item_inbox',
      desc: '',
      args: [],
    );
  }

  /// `Buy packages`
  String get nav_item_purchase_bundle {
    return Intl.message(
      'Buy packages',
      name: 'nav_item_purchase_bundle',
      desc: '',
      args: [],
    );
  }

  /// `recommend to friends`
  String get nav_item_share_with_friends {
    return Intl.message(
      'recommend to friends',
      name: 'nav_item_share_with_friends',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get nav_item_setting {
    return Intl.message(
      'Settings',
      name: 'nav_item_setting',
      desc: '',
      args: [],
    );
  }

  /// `support`
  String get nav_item_support {
    return Intl.message(
      'support',
      name: 'nav_item_support',
      desc: '',
      args: [],
    );
  }

  /// `There is currently no data available to display.`
  String get data_not_found {
    return Intl.message(
      'There is currently no data available to display.',
      name: 'data_not_found',
      desc: '',
      args: [],
    );
  }

  /// `Unfortunately, no item was found with the phrase you were looking for.`
  String get search_result_empty {
    return Intl.message(
      'Unfortunately, no item was found with the phrase you were looking for.',
      name: 'search_result_empty',
      desc: '',
      args: [],
    );
  }

  /// `View all`
  String get more {
    return Intl.message(
      'View all',
      name: 'more',
      desc: '',
      args: [],
    );
  }

  /// `Buy`
  String get tab_shopping {
    return Intl.message(
      'Buy',
      name: 'tab_shopping',
      desc: '',
      args: [],
    );
  }

  /// `Profile`
  String get tab_profile {
    return Intl.message(
      'Profile',
      name: 'tab_profile',
      desc: '',
      args: [],
    );
  }

  /// `Home`
  String get tab_home {
    return Intl.message(
      'Home',
      name: 'tab_home',
      desc: '',
      args: [],
    );
  }

  /// `Categories`
  String get tab_categories {
    return Intl.message(
      'Categories',
      name: 'tab_categories',
      desc: '',
      args: [],
    );
  }

  /// `search`
  String get tab_search {
    return Intl.message(
      'search',
      name: 'tab_search',
      desc: '',
      args: [],
    );
  }

  /// `Change password`
  String get change_password {
    return Intl.message(
      'Change password',
      name: 'change_password',
      desc: '',
      args: [],
    );
  }

  /// `Submit your name`
  String get submit_name {
    return Intl.message(
      'Submit your name',
      name: 'submit_name',
      desc: '',
      args: [],
    );
  }

  /// `Language`
  String get language {
    return Intl.message(
      'Language',
      name: 'language',
      desc: '',
      args: [],
    );
  }

  /// `Bookmarked`
  String get bookmarked {
    return Intl.message(
      'Bookmarked',
      name: 'bookmarked',
      desc: '',
      args: [],
    );
  }

  /// `About us`
  String get about_us {
    return Intl.message(
      'About us',
      name: 'about_us',
      desc: '',
      args: [],
    );
  }

  /// `User privacy`
  String get user_privacy {
    return Intl.message(
      'User privacy',
      name: 'user_privacy',
      desc: '',
      args: [],
    );
  }

  /// `Rules`
  String get rules {
    return Intl.message(
      'Rules',
      name: 'rules',
      desc: '',
      args: [],
    );
  }

  /// `Exit`
  String get exit {
    return Intl.message(
      'Exit',
      name: 'exit',
      desc: '',
      args: [],
    );
  }

  /// `To perform this task, you must first log in to your account`
  String get need_to_sign_in {
    return Intl.message(
      'To perform this task, you must first log in to your account',
      name: 'need_to_sign_in',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Confirm`
  String get confirm {
    return Intl.message(
      'Confirm',
      name: 'confirm',
      desc: '',
      args: [],
    );
  }

  /// `Courses: %s`
  String get content_series_count {
    return Intl.message(
      'Courses: %s',
      name: 'content_series_count',
      desc: '',
      args: [],
    );
  }

  /// `Videos: %s`
  String get content_count {
    return Intl.message(
      'Videos: %s',
      name: 'content_count',
      desc: '',
      args: [],
    );
  }

  /// `Birthday`
  String get birth_day {
    return Intl.message(
      'Birthday',
      name: 'birth_day',
      desc: '',
      args: [],
    );
  }

  /// `Birth month`
  String get birth_month {
    return Intl.message(
      'Birth month',
      name: 'birth_month',
      desc: '',
      args: [],
    );
  }

  /// `Year of birth`
  String get birth_year {
    return Intl.message(
      'Year of birth',
      name: 'birth_year',
      desc: '',
      args: [],
    );
  }

  /// `Gender`
  String get gender {
    return Intl.message(
      'Gender',
      name: 'gender',
      desc: '',
      args: [],
    );
  }

  /// `Previous password`
  String get login_password {
    return Intl.message(
      'Previous password',
      name: 'login_password',
      desc: '',
      args: [],
    );
  }

  /// `New password`
  String get new_login_password {
    return Intl.message(
      'New password',
      name: 'new_login_password',
      desc: '',
      args: [],
    );
  }

  /// `Repeat new password`
  String get retry_login_password {
    return Intl.message(
      'Repeat new password',
      name: 'retry_login_password',
      desc: '',
      args: [],
    );
  }

  /// `Register a new password`
  String get pass_register {
    return Intl.message(
      'Register a new password',
      name: 'pass_register',
      desc: '',
      args: [],
    );
  }

  /// `Full name`
  String get username {
    return Intl.message(
      'Full name',
      name: 'username',
      desc: '',
      args: [],
    );
  }

  /// `User name`
  String get username_register {
    return Intl.message(
      'User name',
      name: 'username_register',
      desc: '',
      args: [],
    );
  }

  /// `Username and password recovery`
  String get username_and_password_recovery {
    return Intl.message(
      'Username and password recovery',
      name: 'username_and_password_recovery',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get email {
    return Intl.message(
      'Email',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `Favorites`
  String get favorites {
    return Intl.message(
      'Favorites',
      name: 'favorites',
      desc: '',
      args: [],
    );
  }

  /// `Producer: %s`
  String get producer_name {
    return Intl.message(
      'Producer: %s',
      name: 'producer_name',
      desc: '',
      args: [],
    );
  }

  /// `Description: %s`
  String get description {
    return Intl.message(
      'Description: %s',
      name: 'description',
      desc: '',
      args: [],
    );
  }

  /// `Phone number`
  String get phone_number_hint {
    return Intl.message(
      'Phone number',
      name: 'phone_number_hint',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password_hint {
    return Intl.message(
      'Password',
      name: 'password_hint',
      desc: '',
      args: [],
    );
  }

  /// `Temporary password`
  String get temporary_code_hint {
    return Intl.message(
      'Temporary password',
      name: 'temporary_code_hint',
      desc: '',
      args: [],
    );
  }

  /// `Your username and password have been sent, please check your SMS`
  String get check_sms {
    return Intl.message(
      'Your username and password have been sent, please check your SMS',
      name: 'check_sms',
      desc: '',
      args: [],
    );
  }

  /// `verification code`
  String get activation_temporary_code_hint {
    return Intl.message(
      'verification code',
      name: 'activation_temporary_code_hint',
      desc: '',
      args: [],
    );
  }

  /// `verification code`
  String get forgot_code_hint {
    return Intl.message(
      'verification code',
      name: 'forgot_code_hint',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get login {
    return Intl.message(
      'Login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Register`
  String get register {
    return Intl.message(
      'Register',
      name: 'register',
      desc: '',
      args: [],
    );
  }

  /// `Forgot your password?`
  String get forgot_password {
    return Intl.message(
      'Forgot your password?',
      name: 'forgot_password',
      desc: '',
      args: [],
    );
  }

  /// `Recover it`
  String get recover_it {
    return Intl.message(
      'Recover it',
      name: 'recover_it',
      desc: '',
      args: [],
    );
  }

  /// `Or`
  String get or {
    return Intl.message(
      'Or',
      name: 'or',
      desc: '',
      args: [],
    );
  }

  /// `Send`
  String get send {
    return Intl.message(
      'Send',
      name: 'send',
      desc: '',
      args: [],
    );
  }

  /// `Similar Courses`
  String get similar_courses {
    return Intl.message(
      'Similar Courses',
      name: 'similar_courses',
      desc: '',
      args: [],
    );
  }

  /// `Do you want to log out of your account?`
  String get are_u_sure_exit {
    return Intl.message(
      'Do you want to log out of your account?',
      name: 'are_u_sure_exit',
      desc: '',
      args: [],
    );
  }

  /// `Yes`
  String get yes {
    return Intl.message(
      'Yes',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `No`
  String get no {
    return Intl.message(
      'No',
      name: 'no',
      desc: '',
      args: [],
    );
  }

  /// `Rate`
  String get rate {
    return Intl.message(
      'Rate',
      name: 'rate',
      desc: '',
      args: [],
    );
  }

  /// `English`
  String get lang_english {
    return Intl.message(
      'English',
      name: 'lang_english',
      desc: '',
      args: [],
    );
  }

  /// `Arabic`
  String get lang_arabic {
    return Intl.message(
      'Arabic',
      name: 'lang_arabic',
      desc: '',
      args: [],
    );
  }

  /// `New password`
  String get new_password_hint {
    return Intl.message(
      'New password',
      name: 'new_password_hint',
      desc: '',
      args: [],
    );
  }

  /// `Current password`
  String get current_password_hint {
    return Intl.message(
      'Current password',
      name: 'current_password_hint',
      desc: '',
      args: [],
    );
  }

  /// `Reenter new password`
  String get repeat_new_password_hint {
    return Intl.message(
      'Reenter new password',
      name: 'repeat_new_password_hint',
      desc: '',
      args: [],
    );
  }

  /// `Please set a display name for yourself. Your comments about the videos will be published with this name.`
  String get nick_name_description {
    return Intl.message(
      'Please set a display name for yourself. Your comments about the videos will be published with this name.',
      name: 'nick_name_description',
      desc: '',
      args: [],
    );
  }

  /// `Sign up`
  String get sign_up {
    return Intl.message(
      'Sign up',
      name: 'sign_up',
      desc: '',
      args: [],
    );
  }

  /// `login as guest >`
  String get login_guest {
    return Intl.message(
      'login as guest >',
      name: 'login_guest',
      desc: '',
      args: [],
    );
  }

  /// `Comments`
  String get comments {
    return Intl.message(
      'Comments',
      name: 'comments',
      desc: '',
      args: [],
    );
  }

  /// `write your comment here`
  String get write_comment {
    return Intl.message(
      'write your comment here',
      name: 'write_comment',
      desc: '',
      args: [],
    );
  }

  /// `Read All`
  String get read_all {
    return Intl.message(
      'Read All',
      name: 'read_all',
      desc: '',
      args: [],
    );
  }

  /// `Get verification code`
  String get getVerificationCode {
    return Intl.message(
      'Get verification code',
      name: 'getVerificationCode',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a valid mobile number.`
  String get error_input_phone_number {
    return Intl.message(
      'Please enter a valid mobile number.',
      name: 'error_input_phone_number',
      desc: '',
      args: [],
    );
  }

  /// `Dont have an account?`
  String get dont_have_account {
    return Intl.message(
      'Dont have an account?',
      name: 'dont_have_account',
      desc: '',
      args: [],
    );
  }

  /// `Create account`
  String get create_account {
    return Intl.message(
      'Create account',
      name: 'create_account',
      desc: '',
      args: [],
    );
  }

  /// `Phone number:`
  String get phone_number {
    return Intl.message(
      'Phone number:',
      name: 'phone_number',
      desc: '',
      args: [],
    );
  }

  /// `Verification code`
  String get verification_code {
    return Intl.message(
      'Verification code',
      name: 'verification_code',
      desc: '',
      args: [],
    );
  }

  /// `Enter the code sent to your phone`
  String get enter_code {
    return Intl.message(
      'Enter the code sent to your phone',
      name: 'enter_code',
      desc: '',
      args: [],
    );
  }

  /// `Code`
  String get code {
    return Intl.message(
      'Code',
      name: 'code',
      desc: '',
      args: [],
    );
  }

  /// `Continue`
  String get continue_ {
    return Intl.message(
      'Continue',
      name: 'continue_',
      desc: '',
      args: [],
    );
  }

  /// `Enter your mobile phone number to receive a code`
  String get enter_your_mobile_code {
    return Intl.message(
      'Enter your mobile phone number to receive a code',
      name: 'enter_your_mobile_code',
      desc: '',
      args: [],
    );
  }

  /// `Back`
  String get back {
    return Intl.message(
      'Back',
      name: 'back',
      desc: '',
      args: [],
    );
  }

  /// `Videos`
  String get videos {
    return Intl.message(
      'Videos',
      name: 'videos',
      desc: '',
      args: [],
    );
  }

  /// `Courses`
  String get course {
    return Intl.message(
      'Courses',
      name: 'course',
      desc: '',
      args: [],
    );
  }

  /// `Home`
  String get home {
    return Intl.message(
      'Home',
      name: 'home',
      desc: '',
      args: [],
    );
  }

  /// `Category`
  String get category {
    return Intl.message(
      'Category',
      name: 'category',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get search {
    return Intl.message(
      'Search',
      name: 'search',
      desc: '',
      args: [],
    );
  }

  /// `Profile`
  String get profile {
    return Intl.message(
      'Profile',
      name: 'profile',
      desc: '',
      args: [],
    );
  }

  /// `loading...`
  String get loading {
    return Intl.message(
      'loading...',
      name: 'loading',
      desc: '',
      args: [],
    );
  }

  /// `Load Failed`
  String get view_state_message_error {
    return Intl.message(
      'Load Failed',
      name: 'view_state_message_error',
      desc: '',
      args: [],
    );
  }

  /// `Load Failed,Check network `
  String get view_state_message_network_error {
    return Intl.message(
      'Load Failed,Check network ',
      name: 'view_state_message_network_error',
      desc: '',
      args: [],
    );
  }

  /// `Nothing Found`
  String get view_state_message_empty {
    return Intl.message(
      'Nothing Found',
      name: 'view_state_message_empty',
      desc: '',
      args: [],
    );
  }

  /// `Not sign in yet`
  String get view_state_message_unAuth {
    return Intl.message(
      'Not sign in yet',
      name: 'view_state_message_unAuth',
      desc: '',
      args: [],
    );
  }

  /// `Refresh`
  String get view_state_button_refresh {
    return Intl.message(
      'Refresh',
      name: 'view_state_button_refresh',
      desc: '',
      args: [],
    );
  }

  /// `Retry`
  String get view_state_button_retry {
    return Intl.message(
      'Retry',
      name: 'view_state_button_retry',
      desc: '',
      args: [],
    );
  }

  /// `Sign In`
  String get view_state_button_login {
    return Intl.message(
      'Sign In',
      name: 'view_state_button_login',
      desc: '',
      args: [],
    );
  }

  /// `Your phone number or password is incorrect`
  String get error_phone_or_password {
    return Intl.message(
      'Your phone number or password is incorrect',
      name: 'error_phone_or_password',
      desc: '',
      args: [],
    );
  }

  /// `Resend activation code`
  String get send_registration_code_again {
    return Intl.message(
      'Resend activation code',
      name: 'send_registration_code_again',
      desc: '',
      args: [],
    );
  }

  /// `Your code is incorrect`
  String get error_code {
    return Intl.message(
      'Your code is incorrect',
      name: 'error_code',
      desc: '',
      args: [],
    );
  }

  /// `Submit`
  String get submit_rate {
    return Intl.message(
      'Submit',
      name: 'submit_rate',
      desc: '',
      args: [],
    );
  }

  /// `Language`
  String get change_language {
    return Intl.message(
      'Language',
      name: 'change_language',
      desc: '',
      args: [],
    );
  }

  /// `Register`
  String get activation {
    return Intl.message(
      'Register',
      name: 'activation',
      desc: '',
      args: [],
    );
  }

  /// `Unsubscribe`
  String get unsubscribe {
    return Intl.message(
      'Unsubscribe',
      name: 'unsubscribe',
      desc: '',
      args: [],
    );
  }

  /// `To unsubscribe, click the below button`
  String get unsubscribe_message {
    return Intl.message(
      'To unsubscribe, click the below button',
      name: 'unsubscribe_message',
      desc: '',
      args: [],
    );
  }

  /// `Weekly subscription: 5 SAR +3 days trial\nTo unsubscribe anytime, send U11 to 705731`
  String get information_vas {
    return Intl.message(
      'Weekly subscription: 5 SAR +3 days trial\nTo unsubscribe anytime, send U11 to 705731',
      name: 'information_vas',
      desc: '',
      args: [],
    );
  }

  /// `Enter your mobile phone number.`
  String get registration_page_description {
    return Intl.message(
      'Enter your mobile phone number.',
      name: 'registration_page_description',
      desc: '',
      args: [],
    );
  }

  /// `Already have an account?`
  String get already_have_an_account {
    return Intl.message(
      'Already have an account?',
      name: 'already_have_an_account',
      desc: '',
      args: [],
    );
  }

  /// `The length of the entered text is more than 150 characters.`
  String get error_comment_length {
    return Intl.message(
      'The length of the entered text is more than 150 characters.',
      name: 'error_comment_length',
      desc: '',
      args: [],
    );
  }

  /// `The length of the entered text is more than 3 characters.`
  String get error_comment_length_less {
    return Intl.message(
      'The length of the entered text is more than 3 characters.',
      name: 'error_comment_length_less',
      desc: '',
      args: [],
    );
  }

  /// `Your comment has been successfully registered and will be displayed by the admin after being approved.`
  String get comment_successfully_sent {
    return Intl.message(
      'Your comment has been successfully registered and will be displayed by the admin after being approved.',
      name: 'comment_successfully_sent',
      desc: '',
      args: [],
    );
  }

  /// `Your name has been successfully sent.`
  String get nick_name_successfully_sent {
    return Intl.message(
      'Your name has been successfully sent.',
      name: 'nick_name_successfully_sent',
      desc: '',
      args: [],
    );
  }

  /// `Your password has been successfully changed.`
  String get change_password_successfully {
    return Intl.message(
      'Your password has been successfully changed.',
      name: 'change_password_successfully',
      desc: '',
      args: [],
    );
  }

  /// `Clipo is a multimedia system with documented content that allows users to receive content in video format. The quality and speed of receiving this content depend on the user's Internet speed. This system includes different categories in different fields. By installing the software or logging in to the website, each user can easily view all the free videos. With special membership, users can see all the videos at the lowest price, at any time and any place with minimum internet usage.`
  String get about_us_content {
    return Intl.message(
      'Clipo is a multimedia system with documented content that allows users to receive content in video format. The quality and speed of receiving this content depend on the user\'s Internet speed. This system includes different categories in different fields. By installing the software or logging in to the website, each user can easily view all the free videos. With special membership, users can see all the videos at the lowest price, at any time and any place with minimum internet usage.',
      name: 'about_us_content',
      desc: '',
      args: [],
    );
  }

  /// `Respecting the privacy of its users, Clipo  requests users to register with their mobile number to purchase, register, or use certain features of the website. Clipo  may also use some information to communicate with customers and market research. Clipo  may send SMS to members of the site to inform them of events and news, special services , courses, and promotions.  ???? is the only number that  Clipo  uses to sends SMS to its users and customers. Therefore, sending any text message under the title of Clipo with any other number is a violation and abuse of the name of the Clipo, and if you receive such a text message, please inform us with info@Clipo.com for legal action. Clipo   may edit reviews and comments submitted by users according to the rules of the website. Also, if the user's comment or message is subject to instances of illegal content, Clipo  can use the recorded information to pursue legal action. It is the responsibility of the user to maintain the password, and users should not disclose it to anyone else to prevent any possible misuse. Video considers the personal identity of users confidential. Clipo does not transfer the user's personal information to any other person or organization unless it's required by law to provide it to the competent authorities. Clipo, like other websites, uses IP collection and cookies, but the protocol, server, and security layers of Clipo and data management practices protect users' information and prevent unauthorized access. Clipo  does its best to protect and maintain users' information and privacy and hopes to provide a safe, comfortable, and enjoyable experience for all users. Users can email info@Clipo.com to share their problems, criticisms, or suggestions.`
  String get privacy_content {
    return Intl.message(
      'Respecting the privacy of its users, Clipo  requests users to register with their mobile number to purchase, register, or use certain features of the website. Clipo  may also use some information to communicate with customers and market research. Clipo  may send SMS to members of the site to inform them of events and news, special services , courses, and promotions.  ???? is the only number that  Clipo  uses to sends SMS to its users and customers. Therefore, sending any text message under the title of Clipo with any other number is a violation and abuse of the name of the Clipo, and if you receive such a text message, please inform us with info@Clipo.com for legal action. Clipo   may edit reviews and comments submitted by users according to the rules of the website. Also, if the user\'s comment or message is subject to instances of illegal content, Clipo  can use the recorded information to pursue legal action. It is the responsibility of the user to maintain the password, and users should not disclose it to anyone else to prevent any possible misuse. Video considers the personal identity of users confidential. Clipo does not transfer the user\'s personal information to any other person or organization unless it\'s required by law to provide it to the competent authorities. Clipo, like other websites, uses IP collection and cookies, but the protocol, server, and security layers of Clipo and data management practices protect users\' information and prevent unauthorized access. Clipo  does its best to protect and maintain users\' information and privacy and hopes to provide a safe, comfortable, and enjoyable experience for all users. Users can email info@Clipo.com to share their problems, criticisms, or suggestions.',
      name: 'privacy_content',
      desc: '',
      args: [],
    );
  }

  /// `Using the Clipo platform means that you have carefully read all the following terms and conditions and have agreed to it. All videos in different services have been bought from reliable and standard sources and they have been prepared for the optimal use of the audience. All rights to the broadcast and publish  of these videos belong to Clipo , and any duplication or publication of these videos for commercial or non-commercial use will be prosecuted. All comments will be reviewed after submission and will be prevented from being published if they involve insults to religions, customs, ethnicity, accents, or contrary to custom or law. Users should avoid improper use of the software in such a way that the company's infrastructure is compromised, and the company can restrict the access and membership of these users if necessary. All the content of this software is prepared according to the law, and the use and application of this software must be in accordance with these laws.`
  String get policy_content {
    return Intl.message(
      'Using the Clipo platform means that you have carefully read all the following terms and conditions and have agreed to it. All videos in different services have been bought from reliable and standard sources and they have been prepared for the optimal use of the audience. All rights to the broadcast and publish  of these videos belong to Clipo , and any duplication or publication of these videos for commercial or non-commercial use will be prosecuted. All comments will be reviewed after submission and will be prevented from being published if they involve insults to religions, customs, ethnicity, accents, or contrary to custom or law. Users should avoid improper use of the software in such a way that the company\'s infrastructure is compromised, and the company can restrict the access and membership of these users if necessary. All the content of this software is prepared according to the law, and the use and application of this software must be in accordance with these laws.',
      name: 'policy_content',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'ar'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}