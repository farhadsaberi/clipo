int langID = 1;

class Constant {
  static const String API = "http://api.clipo-app.com/api";
  static const String assetsImages = 'assets/images/';
  static const bool DEBUG = true;
  static const String MOBILE = "mobile";
  static const String TOKEN_KEY = "token_key";
  static const String LANGUAGE_CODE = "language_code";
  static const String REFRESH_TOKEN_KEY = "refresh_token_key";
  static const String ORIGIN = "Laning,S3";
  static const String COUNTRY_CODE = "996";
}
