import 'package:clipo/config/constant.dart';
import 'package:clipo/entity/bookmark_response.dart';
import 'package:clipo/entity/category_response.dart';
import 'package:clipo/entity/content_response.dart';
import 'package:clipo/entity/content_serie.dart';
import 'package:clipo/entity/content_serie_part.dart';
import 'package:clipo/entity/forgot_password_response.dart';
import 'package:clipo/entity/home_response.dart';
import 'package:clipo/entity/login_response.dart';
import 'package:clipo/entity/register_response.dart';
import 'package:clipo/entity/search_response.dart';
import 'package:clipo/entity/sub_category_response.dart';
import 'package:clipo/net/HttpManager.dart';
import 'package:clipo/net/api.dart';
import 'package:clipo/net/result_data.dart';
import 'package:dio/dio.dart';

class Repository {
  Future unSubscribe() async {
    String url = Api.unSubscribe();
    var res = await httpManager.netFetch(
        url, null, null, Options(method: HttpManager.POST));
    if (res != null && res.result) {
      return Future.value();
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }
  Future<HomeResponse> getHomeList() async {
    String url = Api.home();
    var res = await httpManager.netFetch(
        url, null, null, Options(method: HttpManager.GET));
    if (res != null && res.result) {
      return Future.value(HomeResponse.fromJson(res.data));
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future<ContentSerie> getContentSerie(
      String key, int page, int pageSize) async {
    String url = Api.contentSerie(key, page, pageSize);
    var res = await httpManager.netFetch(
        url, null, null, Options(method: HttpManager.GET));
    if (res != null && res.result) {
      return Future.value(ContentSerie.fromJson(res.data));
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future<ContentSerieParts> contentSeriePart(
      String content, int page, int pageSize) async {
    String url = Api.contentSerieParts(content, page, pageSize);
    var res = await httpManager.netFetch(
        url, null, null, Options(method: HttpManager.GET));
    if (res != null && res.result) {
      return Future.value(ContentSerieParts.fromJson(res.data));
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future<ContentResponse> content(String content, {int contentId}) async {
    String url = Api.content(content, contentId: contentId ?? null);
    var res = await httpManager.netFetch(
        url, null, null, Options(method: HttpManager.GET));
    if (res != null && res.result) {
      return Future.value(ContentResponse.fromJson(res.data));
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future<List<CategoryResponse>> getCategory() async {
    String url = Api.category();
    var res = await httpManager.netFetch(
        url, null, null, Options(method: HttpManager.GET));
    if (res != null && res.result) {
      final List userList =
      res.data.map((map) => CategoryResponse.fromJson(map)).toList();
      return Future.value(userList.cast<CategoryResponse>());
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future<SubCategoryResponse> getSubCategory(
      String key, int page, int pageSize) async {
    String url = Api.subCategory(key, false, page, pageSize);
    var res = await httpManager.netFetch(
        url, null, null, Options(method: HttpManager.GET));
    if (res != null && res.result) {
      return Future.value(SubCategoryResponse.fromJson(res.data));
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future<SearchResponse> search(String key, int page, int pageSize) async {
    String url = Api.search(key, 0, page, pageSize);
    var res = await httpManager.netFetch(
        url, null, null, Options(method: HttpManager.GET));
    if (res != null && res.result) {
      return Future.value(SearchResponse.fromJson(res.data));
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future<LoginResponse> loginWithPassword(
      String msisdn, String password) async {
    String url = Api.loginWithPassword();
    var res = await httpManager.netFetch(
        url,
        {"code": password, "msisdn": msisdn},
        null,
        Options(method: HttpManager.POST));
    if (res != null && res.result) {
      return Future.value(LoginResponse.fromJson(res.data));
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }
  Future<LoginResponse> loginWithMsinsdnAndPassword(
      String msisdn, String password) async {
    String url = Api.loginWithMsinsdnAndPassword();
    var res = await httpManager.netFetch(
        url,
        {"code": password, "msisdn": msisdn},
        null,
        Options(method: HttpManager.POST));
    if (res != null && res.result) {
      return Future.value(LoginResponse.fromJson(res.data));
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future<LoginResponse> register(String msisdn) async {
    String url = Api.register();
    var res = await httpManager.netFetch(
        url,
        {"origin": Constant.ORIGIN, "msisdn": msisdn},
        null,
        Options(method: HttpManager.POST));
    if (res != null && res.result) {
      return Future.value(LoginResponse.fromJson(res.data));
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future<LoginResponse> verificationCode(String msisdn, String code) async {
    String url = Api.verificationCode();
    var res = await httpManager.netFetch(url, {"code": code, "msisdn": msisdn},
        null, Options(method: HttpManager.POST));
    if (res != null && res.result) {
      return Future.value(LoginResponse.fromJson(res.data));
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future<ForgotPasswordResponse> forgotPassword(String msisdn) async {
    String url = Api.forgotPassword();
    var res = await httpManager.netFetch(
        url, {"msisdn": msisdn}, null, Options(method: HttpManager.POST));
    if (res != null && res.result) {
      try{
        return Future.value(ForgotPasswordResponse.fromJson(res.data));
      }catch(ex){
        return Future.value();
      }
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future addBookmark(String contentId) async {
    String url = Api.addBookmark(contentId);
    var res = await httpManager.netFetch(
        url, null, null, Options(method: HttpManager.POST));
    if (res != null && res.result) {
      return Future.value();
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future removeBookmark(String contentId) async {
    String url = Api.removeBookmark(contentId);
    var res = await httpManager.netFetch(
        url, null, null, Options(method: HttpManager.DELETE));
    if (res != null && res.result) {
      return Future.value();
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future postContentSeriesRate(String contentSerieId, double rate) async {
    String url = Api.postContentSeriesRate();
    var res = await httpManager.netFetch(
        url,
        {"contentSerieId": contentSerieId, "id": 0, "rate": rate, "userId": 0},
        null,
        Options(method: HttpManager.POST));
    if (res != null && res.result) {
      return Future.value();
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future changePassword(String msisdn, String oldPassword, String newPassword,
      String confirmPassword) async {
    String url = Api.changePassword();
    var res = await httpManager.netFetch(
        url,
        {
          "msisdn": msisdn,
          "oldPassword": oldPassword,
          "newPassword": newPassword,
          "confirmPassword": confirmPassword
        },
        null,
        Options(method: HttpManager.POST));
    if (res != null && res.result) {
      return Future.value();
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future resetPassword(String msisdn, String code, String password,
      String repeatPassword) async {
    String url = Api.resetPassword();
    var res = await httpManager.netFetch(
        url,
        {
          "msisdn": msisdn,
          "code": code,
          "password": password,
          "repeatPassword": repeatPassword
        },
        null,
        Options(method: HttpManager.POST));
    if (res != null && res.result) {
      return Future.value();
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future getProfile() async {
    String url = Api.getProfile();
    var res = await httpManager.netFetch(
        url, null, null, Options(method: HttpManager.GET));
    if (res != null && res.result) {
      return Future.value();
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future setNickName(String nickName) async {
    String url = Api.setNickName();
    var res = await httpManager.netFetch(
        url, {"nickName": nickName}, null, Options(method: HttpManager.POST));
    if (res != null && res.result) {
      return Future.value();
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future<BookmarkResponse> getBookmarks(int page, int pageSize) async {
    String url = Api.getBookmarks(page, pageSize);
    var res = await httpManager.netFetch(
        url, null, null, Options(method: HttpManager.GET));
    if (res != null && res.result) {
      return Future.value(BookmarkResponse.fromJson(res.data));
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future setComment(String contentId, String comment) async {
    String url = Api.setComment(contentId);
    var res = await httpManager.netFetch(
        url, {"text": comment}, null, Options(method: HttpManager.POST));
    if (res != null && res.result) {
      return Future.value();
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future getContentSerieComments(
      String contentId, int page, int pageSize) async {
    String url = Api.getContentSerieComments(contentId, page, pageSize);
    var res = await httpManager.netFetch(
        url, null, null, Options(method: HttpManager.GET));
    if (res != null && res.result) {
      return Future.value();
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }
}
