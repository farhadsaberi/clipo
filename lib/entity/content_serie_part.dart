// To parse this JSON data, do
//
//     final contentSerieParts = contentSeriePartsFromJson(jsonString);

import 'dart:convert';

ContentSerieParts contentSeriePartsFromJson(String str) => ContentSerieParts.fromJson(json.decode(str));

String contentSeriePartsToJson(ContentSerieParts data) => json.encode(data.toJson());

class ContentSerieParts {
  ContentSerieParts({
    this.count,
    this.items,
    this.page,
    this.pageSize,
    this.totalPage,
  });

  int count;
  List<ItemPart> items;
  int page;
  int pageSize;
  int totalPage;

  factory ContentSerieParts.fromJson(Map<String, dynamic> json) => ContentSerieParts(
    count: json["count"] == null ? null : json["count"],
    items: json["items"] == null ? null : List<ItemPart>.from(json["items"].map((x) => ItemPart.fromJson(x))),
    page: json["page"] == null ? null : json["page"],
    pageSize: json["pageSize"] == null ? null : json["pageSize"],
    totalPage: json["totalPage"] == null ? null : json["totalPage"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "items": items == null ? null : List<dynamic>.from(items.map((x) => x.toJson())),
    "page": page == null ? null : page,
    "pageSize": pageSize == null ? null : pageSize,
    "totalPage": totalPage == null ? null : totalPage,
  };
}

class ItemPart {
  ItemPart({
    this.id,
    this.title,
    this.duration,
    this.producer,
    this.key,
    this.imageFileName,
    this.isPurchased,
    this.purchaseKeyword,
    this.purchaseNotice,
    this.contentSerieId,
    this.isBookmarked,
    this.contentSerieTitle,
    this.categoryTitle,
    this.isFree,
    this.playLinks,
    this.imageUrl,
  });

  int id;
  String title;
  String duration;
  dynamic producer;
  String key;
  String imageFileName;
  bool isPurchased;
  dynamic purchaseKeyword;
  dynamic purchaseNotice;
  int contentSerieId;
  bool isBookmarked;
  String contentSerieTitle;
  String categoryTitle;
  bool isFree;
  List<PlayLinkConentSeriePart> playLinks;
  String imageUrl;

  factory ItemPart.fromJson(Map<String, dynamic> json) => ItemPart(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    duration: json["duration"] == null ? null : json["duration"],
    producer: json["producer"],
    key: json["key"] == null ? null : json["key"],
    imageFileName: json["imageFileName"] == null ? null : json["imageFileName"],
    isPurchased: json["isPurchased"] == null ? null : json["isPurchased"],
    purchaseKeyword: json["purchaseKeyword"],
    purchaseNotice: json["purchaseNotice"],
    contentSerieId: json["contentSerieId"] == null ? null : json["contentSerieId"],
    isBookmarked: json["isBookmarked"] == null ? null : json["isBookmarked"],
    contentSerieTitle: json["contentSerieTitle"] == null ? null : json["contentSerieTitle"],
    categoryTitle: json["categoryTitle"] == null ? null : json["categoryTitle"],
    isFree: json["isFree"] == null ? null : json["isFree"],
    playLinks: json["playLinks"] == null ? null : List<PlayLinkConentSeriePart>.from(json["playLinks"].map((x) => PlayLinkConentSeriePart.fromJson(x))),
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "duration": duration == null ? null : duration,
    "producer": producer,
    "imageFileName": imageFileName == null ? null : imageFileName,
    "isPurchased": isPurchased == null ? null : isPurchased,
    "purchaseKeyword": purchaseKeyword,
    "purchaseNotice": purchaseNotice,
    "contentSerieId": contentSerieId == null ? null : contentSerieId,
    "isBookmarked": isBookmarked == null ? null : isBookmarked,
    "categoryTitle": categoryTitle == null ? null : categoryTitle,
    "isFree": isFree == null ? null : isFree,
    "playLinks": playLinks == null ? null : List<dynamic>.from(playLinks.map((x) => x.toJson())),
    "imageUrl": imageUrl == null ? null : imageUrl,
  };
}


class PlayLinkConentSeriePart {
  PlayLinkConentSeriePart({
    this.name,
    this.link,
  });

  Name name;
  String link;

  factory PlayLinkConentSeriePart.fromJson(Map<String, dynamic> json) => PlayLinkConentSeriePart(
    name: json["name"] == null ? null : nameValues.map[json["name"]],
    link: json["link"] == null ? null : json["link"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : nameValues.reverse[name],
    "link": link == null ? null : link,
  };
}

enum Name { ADAPTIVE }

final nameValues = EnumValues({
  "Adaptive": Name.ADAPTIVE
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
