// To parse this JSON data, do
//
//     final forgotPasswordResponse = forgotPasswordResponseFromJson(jsonString);

import 'dart:convert';

ForgotPasswordResponse forgotPasswordResponseFromJson(String str) => ForgotPasswordResponse.fromJson(json.decode(str));

String forgotPasswordResponseToJson(ForgotPasswordResponse data) => json.encode(data.toJson());

class ForgotPasswordResponse {
  ForgotPasswordResponse({
    this.userLikes,
    this.userRates,
    this.msisdn,
    this.password,
    this.email,
    this.nickName,
    this.unlimitedPackagePurchaseState,
    this.id,
    this.creationDateTime,
    this.isDeleted,
    this.isSavedOnDatabase,
  });

  List<dynamic> userLikes;
  List<dynamic> userRates;
  String msisdn;
  String password;
  dynamic email;
  dynamic nickName;
  int unlimitedPackagePurchaseState;
  int id;
  DateTime creationDateTime;
  bool isDeleted;
  bool isSavedOnDatabase;

  factory ForgotPasswordResponse.fromJson(Map<String, dynamic> json) => ForgotPasswordResponse(
    userLikes: json["userLikes"] == null ? null : List<dynamic>.from(json["userLikes"].map((x) => x)),
    userRates: json["userRates"] == null ? null : List<dynamic>.from(json["userRates"].map((x) => x)),
    msisdn: json["msisdn"] == null ? null : json["msisdn"],
    password: json["password"] == null ? null : json["password"],
    email: json["email"],
    nickName: json["nickName"],
    unlimitedPackagePurchaseState: json["unlimitedPackagePurchaseState"] == null ? null : json["unlimitedPackagePurchaseState"],
    id: json["id"] == null ? null : json["id"],
    creationDateTime: json["creationDateTime"] == null ? null : DateTime.parse(json["creationDateTime"]),
    isDeleted: json["isDeleted"] == null ? null : json["isDeleted"],
    isSavedOnDatabase: json["isSavedOnDatabase"] == null ? null : json["isSavedOnDatabase"],
  );

  Map<String, dynamic> toJson() => {
    "userLikes": userLikes == null ? null : List<dynamic>.from(userLikes.map((x) => x)),
    "userRates": userRates == null ? null : List<dynamic>.from(userRates.map((x) => x)),
    "msisdn": msisdn == null ? null : msisdn,
    "password": password == null ? null : password,
    "email": email,
    "nickName": nickName,
    "unlimitedPackagePurchaseState": unlimitedPackagePurchaseState == null ? null : unlimitedPackagePurchaseState,
    "id": id == null ? null : id,
    "creationDateTime": creationDateTime == null ? null : creationDateTime.toIso8601String(),
    "isDeleted": isDeleted == null ? null : isDeleted,
    "isSavedOnDatabase": isSavedOnDatabase == null ? null : isSavedOnDatabase,
  };
}
