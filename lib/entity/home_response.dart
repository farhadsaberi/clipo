// To parse this JSON data, do
//
//     final homeResponse = homeResponseFromJson(jsonString);

import 'dart:convert';

HomeResponse homeResponseFromJson(String str) => HomeResponse.fromJson(json.decode(str));

String homeResponseToJson(HomeResponse data) => json.encode(data.toJson());

class HomeResponse {
  HomeResponse({
    this.bannerSections,
    this.contentSerieSections,
    this.contentSections,
    this.producerSection,
  });

  List<BannerSection> bannerSections;
  List<ContentSerieSection> contentSerieSections;
  List<dynamic> contentSections;
  dynamic producerSection;

  factory HomeResponse.fromJson(Map<String, dynamic> json) => HomeResponse(
    bannerSections: json["bannerSections"] == null ? null : List<BannerSection>.from(json["bannerSections"].map((x) => BannerSection.fromJson(x))),
    contentSerieSections: json["contentSerieSections"] == null ? null : List<ContentSerieSection>.from(json["contentSerieSections"].map((x) => ContentSerieSection.fromJson(x))),
    contentSections: json["contentSections"] == null ? null : List<dynamic>.from(json["contentSections"].map((x) => x)),
    producerSection: json["producerSection"],
  );

  Map<String, dynamic> toJson() => {
    "bannerSections": bannerSections == null ? null : List<dynamic>.from(bannerSections.map((x) => x.toJson())),
    "contentSerieSections": contentSerieSections == null ? null : List<dynamic>.from(contentSerieSections.map((x) => x.toJson())),
    "contentSections": contentSections == null ? null : List<dynamic>.from(contentSections.map((x) => x)),
    "producerSection": producerSection,
  };
}

class BannerSection {
  BannerSection({
    this.index,
    this.sectionId,
    this.type,
    this.banners,
  });

  int index;
  int sectionId;
  String type;
  List<Banner> banners;

  factory BannerSection.fromJson(Map<String, dynamic> json) => BannerSection(
    index: json["index"] == null ? null : json["index"],
    sectionId: json["sectionId"] == null ? null : json["sectionId"],
    type: json["type"] == null ? null : json["type"],
    banners: json["banners"] == null ? null : List<Banner>.from(json["banners"].map((x) => Banner.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "index": index == null ? null : index,
    "sectionId": sectionId == null ? null : sectionId,
    "type": type == null ? null : type,
    "banners": banners == null ? null : List<dynamic>.from(banners.map((x) => x.toJson())),
  };
}

class Banner {
  Banner({
    this.id,
    this.type,
    this.imageUrl,
    this.clickUrl,
  });

  int id;
  String type;
  String imageUrl;
  String clickUrl;

  factory Banner.fromJson(Map<String, dynamic> json) => Banner(
    id: json["id"] == null ? null : json["id"],
    type: json["type"] == null ? null : json["type"],
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
    clickUrl: json["clickUrl"] == null ? null : json["clickUrl"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "type": type == null ? null : type,
    "imageUrl": imageUrl == null ? null : imageUrl,
    "clickUrl": clickUrl == null ? null : clickUrl,
  };
}

class ContentSerieSection {
  ContentSerieSection({
    this.id,
    this.title,
    this.index,
    this.isActive,
    this.key,
    this.type,
    this.clickUrl,
    this.pagedContentSeries,
    this.pagedContents,
    this.banners,
  });

  int id;
  String title;
  int index;
  bool isActive;
  String key;
  int type;
  String clickUrl;
  PagedContentSeries pagedContentSeries;
  dynamic pagedContents;
  dynamic banners;

  factory ContentSerieSection.fromJson(Map<String, dynamic> json) => ContentSerieSection(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    index: json["index"] == null ? null : json["index"],
    isActive: json["isActive"] == null ? null : json["isActive"],
    key: json["key"] == null ? null : json["key"],
    type: json["type"] == null ? null : json["type"],
    clickUrl: json["clickUrl"] == null ? null : json["clickUrl"],
    pagedContentSeries: json["pagedContentSeries"] == null ? null : PagedContentSeries.fromJson(json["pagedContentSeries"]),
    pagedContents: json["pagedContents"],
    banners: json["banners"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "index": index == null ? null : index,
    "isActive": isActive == null ? null : isActive,
    "key": key == null ? null : key,
    "type": type == null ? null : type,
    "clickUrl": clickUrl == null ? null : clickUrl,
    "pagedContentSeries": pagedContentSeries == null ? null : pagedContentSeries.toJson(),
    "pagedContents": pagedContents,
    "banners": banners,
  };
}

class PagedContentSeries {
  PagedContentSeries({
    this.count,
    this.items,
    this.page,
    this.pageSize,
    this.totalPage,
  });

  int count;
  List<ItemHome> items;
  int page;
  int pageSize;
  int totalPage;

  factory PagedContentSeries.fromJson(Map<String, dynamic> json) => PagedContentSeries(
    count: json["count"] == null ? null : json["count"],
    items: json["items"] == null ? null : List<ItemHome>.from(json["items"].map((x) => ItemHome.fromJson(x))),
    page: json["page"] == null ? null : json["page"],
    pageSize: json["pageSize"] == null ? null : json["pageSize"],
    totalPage: json["totalPage"] == null ? null : json["totalPage"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "items": items == null ? null : List<dynamic>.from(items.map((x) => x.toJson())),
    "page": page == null ? null : page,
    "pageSize": pageSize == null ? null : pageSize,
    "totalPage": totalPage == null ? null : totalPage,
  };
}

class ItemHome {
  ItemHome({
    this.id,
    this.key,
    this.name,
    this.title,
    this.description,
    this.imageFileName,
    this.isPurchased,
    this.isPublished,
    this.partsCount,
    this.producer,
    this.producerId,
    this.totalDuration,
    this.categoryTitle,
    this.creationDateTime,
    this.purchaseNotice,
    this.priceString,
    this.packageId,
    this.purchaseKeyword,
    this.purchaseOffKeyword,
    this.userLikesCount,
    this.averageRate,
    this.rateParticipantsCount,
    this.videoCounts,
    this.isFree,
    this.contents,
    this.categories,
    this.tags,
    this.comments,
    this.imageUrl,
  });

  int id;
  Key key;
  String name;
  String title;
  dynamic description;
  ImageFileName imageFileName;
  bool isPurchased;
  bool isPublished;
  int partsCount;
  Producer producer;
  int producerId;
  dynamic totalDuration;
  String categoryTitle;
  String creationDateTime;
  dynamic purchaseNotice;
  dynamic priceString;
  int packageId;
  dynamic purchaseKeyword;
  dynamic purchaseOffKeyword;
  int userLikesCount;
  double averageRate;
  int rateParticipantsCount;
  int videoCounts;
  bool isFree;
  List<Content> contents;
  List<Category> categories;
  List<dynamic> tags;
  List<dynamic> comments;
  String imageUrl;

  factory ItemHome.fromJson(Map<String, dynamic> json) => ItemHome(
    id: json["id"] == null ? null : json["id"],
    key: json["key"] == null ? null : keyValues.map[json["key"]],
    name: json["name"] == null ? null : json["name"],
    title: json["title"] == null ? null : json["title"],
    description: json["description"],
    imageFileName: json["imageFileName"] == null ? null : imageFileNameValues.map[json["imageFileName"]],
    isPurchased: json["isPurchased"] == null ? null : json["isPurchased"],
    isPublished: json["isPublished"] == null ? null : json["isPublished"],
    partsCount: json["partsCount"] == null ? null : json["partsCount"],
    producer: json["producer"] == null ? null : producerValues.map[json["producer"]],
    producerId: json["producerId"] == null ? null : json["producerId"],
    totalDuration: json["totalDuration"],
    categoryTitle: json["categoryTitle"] == null ? null : json["categoryTitle"],
    creationDateTime: json["creationDateTime"] == null ? null : json["creationDateTime"],
    purchaseNotice: json["purchaseNotice"],
    priceString: json["priceString"],
    packageId: json["packageId"] == null ? null : json["packageId"],
    purchaseKeyword: json["purchaseKeyword"],
    purchaseOffKeyword: json["purchaseOffKeyword"],
    userLikesCount: json["userLikesCount"] == null ? null : json["userLikesCount"],
    averageRate: json["averageRate"] == null ? null : json["averageRate"],
    rateParticipantsCount: json["rate_ParticipantsCount"] == null ? null : json["rate_ParticipantsCount"],
    videoCounts: json["videoCounts"] == null ? null : json["videoCounts"],
    isFree: json["isFree"] == null ? null : json["isFree"],
    contents: json["contents"] == null ? null : List<Content>.from(json["contents"].map((x) => Content.fromJson(x))),
    categories: json["categories"] == null ? null : List<Category>.from(json["categories"].map((x) => Category.fromJson(x))),
    tags: json["tags"] == null ? null : List<dynamic>.from(json["tags"].map((x) => x)),
    comments: json["comments"] == null ? null : List<dynamic>.from(json["comments"].map((x) => x)),
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "key": key == null ? null : keyValues.reverse[key],
    "name": name == null ? null : name,
    "title": title == null ? null : title,
    "description": description,
    "imageFileName": imageFileName == null ? null : imageFileNameValues.reverse[imageFileName],
    "isPurchased": isPurchased == null ? null : isPurchased,
    "isPublished": isPublished == null ? null : isPublished,
    "partsCount": partsCount == null ? null : partsCount,
    "producer": producer == null ? null : producerValues.reverse[producer],
    "producerId": producerId == null ? null : producerId,
    "totalDuration": totalDuration,
    "categoryTitle": categoryTitle == null ? null : categoryTitle,
    "creationDateTime": creationDateTime == null ? null : creationDateTime,
    "purchaseNotice": purchaseNotice,
    "priceString": priceString,
    "packageId": packageId == null ? null : packageId,
    "purchaseKeyword": purchaseKeyword,
    "purchaseOffKeyword": purchaseOffKeyword,
    "userLikesCount": userLikesCount == null ? null : userLikesCount,
    "averageRate": averageRate == null ? null : averageRate,
    "rate_ParticipantsCount": rateParticipantsCount == null ? null : rateParticipantsCount,
    "videoCounts": videoCounts == null ? null : videoCounts,
    "isFree": isFree == null ? null : isFree,
    "contents": contents == null ? null : List<dynamic>.from(contents.map((x) => x.toJson())),
    "categories": categories == null ? null : List<dynamic>.from(categories.map((x) => x.toJson())),
    "tags": tags == null ? null : List<dynamic>.from(tags.map((x) => x)),
    "comments": comments == null ? null : List<dynamic>.from(comments.map((x) => x)),
    "imageUrl": imageUrl == null ? null : imageUrl,
  };
}

class Category {
  Category({
    this.id,
    this.name,
    this.title,
    this.key,
    this.imageFileName,
    this.webImageFileName,
    this.contentSerieCount,
    this.contentCount,
    this.subCategories,
    this.parent,
    this.parentId,
    this.imageUrl,
    this.webImageUrl,
  });

  int id;
  CategoryTitleEnum name;
  CategoryTitleEnum title;
  String key;
  String imageFileName;
  String webImageFileName;
  int contentSerieCount;
  int contentCount;
  dynamic subCategories;
  dynamic parent;
  dynamic parentId;
  String imageUrl;
  String webImageUrl;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : categoryTitleEnumValues.map[json["name"]],
    title: json["title"] == null ? null : categoryTitleEnumValues.map[json["title"]],
    key: json["key"] == null ? null : json["key"],
    imageFileName: json["imageFileName"] == null ? null : json["imageFileName"],
    webImageFileName: json["webImageFileName"] == null ? null : json["webImageFileName"],
    contentSerieCount: json["contentSerieCount"] == null ? null : json["contentSerieCount"],
    contentCount: json["contentCount"] == null ? null : json["contentCount"],
    subCategories: json["subCategories"],
    parent: json["parent"],
    parentId: json["parentId"],
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
    webImageUrl: json["webImageUrl"] == null ? null : json["webImageUrl"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : categoryTitleEnumValues.reverse[name],
    "title": title == null ? null : categoryTitleEnumValues.reverse[title],
    "key": key == null ? null : key,
    "imageFileName": imageFileName == null ? null : imageFileName,
    "webImageFileName": webImageFileName == null ? null : webImageFileName,
    "contentSerieCount": contentSerieCount == null ? null : contentSerieCount,
    "contentCount": contentCount == null ? null : contentCount,
    "subCategories": subCategories,
    "parent": parent,
    "parentId": parentId,
    "imageUrl": imageUrl == null ? null : imageUrl,
    "webImageUrl": webImageUrl == null ? null : webImageUrl,
  };
}

enum CategoryTitleEnum { EMPTY, ECONOMY_AND_BUSINESS, ART, COOKING, ENTERTAINMENT, TECHNOLOGY, KID }

final categoryTitleEnumValues = EnumValues({
  "Art": CategoryTitleEnum.ART,
  "Cooking": CategoryTitleEnum.COOKING,
  "Economy and Business": CategoryTitleEnum.ECONOMY_AND_BUSINESS,
  "": CategoryTitleEnum.EMPTY,
  "Entertainment": CategoryTitleEnum.ENTERTAINMENT,
  "kid": CategoryTitleEnum.KID,
  "Technology": CategoryTitleEnum.TECHNOLOGY
});

class Content {
  Content({
    this.id,
    this.title,
    this.isPurchased,
    this.contentSerieLikeCount,
    this.playLinks,
    this.contentSerieRate,
    this.isUserLike,
    this.description,
    this.duration,
    this.isMain,
    this.imageFileName,
    this.contentSerieImageFileName,
    this.contentSerieKey,
    this.key,
    this.sequence,
    this.isPublished,
    this.type,
    this.contentSerieId,
    this.isFree,
    this.categoryTitle,
    this.contentSerieTitle,
    this.isBookmarked,
    this.producer,
    this.alias,
    this.shareLink,
    this.tags,
    this.isDeleted,
    this.contentSerieResult,
    this.imageUrl,
    this.previewLink,
  });

  int id;
  String title;
  bool isPurchased;
  int contentSerieLikeCount;
  dynamic playLinks;
  dynamic contentSerieRate;
  bool isUserLike;
  String description;
  String duration;
  bool isMain;
  String imageFileName;
  ImageFileName contentSerieImageFileName;
  Key contentSerieKey;
  String key;
  int sequence;
  bool isPublished;
  int type;
  int contentSerieId;
  bool isFree;
  CategoryTitleEnum categoryTitle;
  String contentSerieTitle;
  bool isBookmarked;
  dynamic producer;
  String alias;
  String shareLink;
  List<dynamic> tags;
  bool isDeleted;
  dynamic contentSerieResult;
  String imageUrl;
  String previewLink;

  factory Content.fromJson(Map<String, dynamic> json) => Content(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    isPurchased: json["isPurchased"] == null ? null : json["isPurchased"],
    contentSerieLikeCount: json["contentSerieLikeCount"] == null ? null : json["contentSerieLikeCount"],
    playLinks: json["playLinks"],
    contentSerieRate: json["contentSerieRate"] == null ? null : json["contentSerieRate"],
    isUserLike: json["isUserLike"] == null ? null : json["isUserLike"],
    description: json["description"] == null ? null : json["description"],
    duration: json["duration"] == null ? null : json["duration"],
    isMain: json["isMain"] == null ? null : json["isMain"],
    imageFileName: json["imageFileName"] == null ? null : json["imageFileName"],
    contentSerieImageFileName: json["contentSerieImageFileName"] == null ? null : imageFileNameValues.map[json["contentSerieImageFileName"]],
    contentSerieKey: json["contentSerieKey"] == null ? null : keyValues.map[json["contentSerieKey"]],
    key: json["key"] == null ? null : json["key"],
    sequence: json["sequence"] == null ? null : json["sequence"],
    isPublished: json["isPublished"] == null ? null : json["isPublished"],
    type: json["type"] == null ? null : json["type"],
    contentSerieId: json["contentSerieId"] == null ? null : json["contentSerieId"],
    isFree: json["isFree"] == null ? null : json["isFree"],
    categoryTitle: json["categoryTitle"] == null ? null : categoryTitleEnumValues.map[json["categoryTitle"]],
    contentSerieTitle: json["contentSerieTitle"] == null ? null : json["contentSerieTitle"],
    isBookmarked: json["isBookmarked"] == null ? null : json["isBookmarked"],
    producer: json["producer"],
    alias: json["alias"] == null ? null : json["alias"],
    shareLink: json["shareLink"] == null ? null : json["shareLink"],
    tags: json["tags"] == null ? null : List<dynamic>.from(json["tags"].map((x) => x)),
    isDeleted: json["isDeleted"] == null ? null : json["isDeleted"],
    contentSerieResult: json["contentSerieResult"],
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
    previewLink: json["previewLink"] == null ? null : json["previewLink"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "isPurchased": isPurchased == null ? null : isPurchased,
    "contentSerieLikeCount": contentSerieLikeCount == null ? null : contentSerieLikeCount,
    "playLinks": playLinks,
    "contentSerieRate": contentSerieRate == null ? null : contentSerieRate,
    "isUserLike": isUserLike == null ? null : isUserLike,
    "description": description == null ? null : description,
    "duration": duration == null ? null : duration,
    "isMain": isMain == null ? null : isMain,
    "imageFileName": imageFileName == null ? null : imageFileName,
    "contentSerieImageFileName": contentSerieImageFileName == null ? null : imageFileNameValues.reverse[contentSerieImageFileName],
    "contentSerieKey": contentSerieKey == null ? null : keyValues.reverse[contentSerieKey],
    "key": key == null ? null : key,
    "sequence": sequence == null ? null : sequence,
    "isPublished": isPublished == null ? null : isPublished,
    "type": type == null ? null : type,
    "contentSerieId": contentSerieId == null ? null : contentSerieId,
    "isFree": isFree == null ? null : isFree,
    "categoryTitle": categoryTitle == null ? null : categoryTitleEnumValues.reverse[categoryTitle],
    "contentSerieTitle": contentSerieTitle == null ? null : contentSerieTitle,
    "isBookmarked": isBookmarked == null ? null : isBookmarked,
    "producer": producer,
    "alias": alias == null ? null : alias,
    "shareLink": shareLink == null ? null : shareLink,
    "tags": tags == null ? null : List<dynamic>.from(tags.map((x) => x)),
    "isDeleted": isDeleted == null ? null : isDeleted,
    "contentSerieResult": contentSerieResult,
    "imageUrl": imageUrl == null ? null : imageUrl,
    "previewLink": previewLink == null ? null : previewLink,
  };
}

enum ImageFileName { HEALTH_AND_FOOD_PNG, BUSINESS_PNG, PHOTOGRAPHY_TIPS_PNG, CRYPTOCURRENCY_PNG, HEALTH_CONTENT_SERIE_PNG, MAIN_COURSE_PNG, PHOTOSHOP_TUTORIAL_PNG, ANIMALS_AND_HUMAN_PNG, ICON_CLIPO_10_PNG, FUNNY_ANIMAL_VIDEOS_PNG, BABY_CARE_PNG }

final imageFileNameValues = EnumValues({
  "Animals and Human.png": ImageFileName.ANIMALS_AND_HUMAN_PNG,
  "baby care.png": ImageFileName.BABY_CARE_PNG,
  "Business.png": ImageFileName.BUSINESS_PNG,
  "cryptocurrency.png": ImageFileName.CRYPTOCURRENCY_PNG,
  "Funny Animal Videos.png": ImageFileName.FUNNY_ANIMAL_VIDEOS_PNG,
  "Health and Food.png": ImageFileName.HEALTH_AND_FOOD_PNG,
  "Health Content serie.png": ImageFileName.HEALTH_CONTENT_SERIE_PNG,
  "icon-clipo-10.png": ImageFileName.ICON_CLIPO_10_PNG,
  "main course.png": ImageFileName.MAIN_COURSE_PNG,
  "photography tips.png": ImageFileName.PHOTOGRAPHY_TIPS_PNG,
  "photoshop tutorial.png": ImageFileName.PHOTOSHOP_TUTORIAL_PNG
});

enum Key { HEALTH_AND_FOOD, BUSINESS, PHOTOGRAPHY_TIPS, CRYPTOCURRENCY, HEALTH, MAIN_COURSE, PHOTOSHOP_TUTORIAL, WILDLIFE_AND_HUMAN, CARS, FUNNY_ANIMAL_VIDEOS, BABY_CARE }

final keyValues = EnumValues({
  "baby care": Key.BABY_CARE,
  "business": Key.BUSINESS,
  "cars": Key.CARS,
  "Cryptocurrency": Key.CRYPTOCURRENCY,
  "Funny animal videos": Key.FUNNY_ANIMAL_VIDEOS,
  "health": Key.HEALTH,
  "Health and Food": Key.HEALTH_AND_FOOD,
  "Main Course": Key.MAIN_COURSE,
  "photography tips": Key.PHOTOGRAPHY_TIPS,
  "photoshop tutorial": Key.PHOTOSHOP_TUTORIAL,
  "wildlife and human": Key.WILDLIFE_AND_HUMAN
});


enum Producer { MULTIMEDIA }

final producerValues = EnumValues({
  "Multimedia": Producer.MULTIMEDIA
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
