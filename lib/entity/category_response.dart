// To parse this JSON data, do
//
//     final categoryResponse = categoryResponseFromJson(jsonString);

import 'dart:convert';

List<CategoryResponse> categoryResponseFromJson(String str) => List<CategoryResponse>.from(json.decode(str).map((x) => CategoryResponse.fromJson(x)));

String categoryResponseToJson(List<CategoryResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CategoryResponse {
  CategoryResponse({
    this.id,
    this.name,
    this.title,
    this.key,
    this.imageFileName,
    this.webImageFileName,
    this.contentSerieCount,
    this.contentCount,
    this.subCategories,
    this.parent,
    this.parentId,
    this.imageUrl,
    this.webImageUrl,
  });

  int id;
  String name;
  String title;
  String key;
  String imageFileName;
  String webImageFileName;
  int contentSerieCount;
  int contentCount;
  dynamic subCategories;
  dynamic parent;
  dynamic parentId;
  String imageUrl;
  String webImageUrl;

  factory CategoryResponse.fromJson(Map<String, dynamic> json) => CategoryResponse(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    title: json["title"] == null ? null : json["title"],
    key: json["key"] == null ? null : json["key"],
    imageFileName: json["imageFileName"] == null ? null : json["imageFileName"],
    webImageFileName: json["webImageFileName"] == null ? null : json["webImageFileName"],
    contentSerieCount: json["contentSerieCount"] == null ? null : json["contentSerieCount"],
    contentCount: json["contentCount"] == null ? null : json["contentCount"],
    subCategories: json["subCategories"],
    parent: json["parent"],
    parentId: json["parentId"],
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
    webImageUrl: json["webImageUrl"] == null ? null : json["webImageUrl"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "title": title == null ? null : title,
    "key": key == null ? null : key,
    "imageFileName": imageFileName == null ? null : imageFileName,
    "webImageFileName": webImageFileName == null ? null : webImageFileName,
    "contentSerieCount": contentSerieCount == null ? null : contentSerieCount,
    "contentCount": contentCount == null ? null : contentCount,
    "subCategories": subCategories,
    "parent": parent,
    "parentId": parentId,
    "imageUrl": imageUrl == null ? null : imageUrl,
    "webImageUrl": webImageUrl == null ? null : webImageUrl,
  };
}
