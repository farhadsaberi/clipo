// To parse this JSON data, do
//
//     final baseResponse = baseResponseFromJson(jsonString);

import 'dart:convert';

BaseResponse baseResponseFromJson(String str) => BaseResponse.fromJson(json.decode(str));

String baseResponseToJson(BaseResponse data) => json.encode(data.toJson());

class BaseResponse {
  BaseResponse({
    this.code,
    this.messageCode,
    this.translatedMessage,
  });

  int code;
  String messageCode;
  String translatedMessage;

  factory BaseResponse.fromJson(Map<String, dynamic> json) => BaseResponse(
    code: json["code"] == null ? null : json["code"],
    messageCode: json["messageCode"] == null ? null : json["messageCode"],
    translatedMessage: json["translatedMessage"] == null ? null : json["translatedMessage"],
  );

  Map<String, dynamic> toJson() => {
    "code": code == null ? null : code,
    "messageCode": messageCode == null ? null : messageCode,
    "translatedMessage": translatedMessage == null ? null : translatedMessage,
  };
}
