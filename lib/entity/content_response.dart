// To parse this JSON data, do
//
//     final contentResponse = contentResponseFromJson(jsonString);

import 'dart:convert';

ContentResponse contentResponseFromJson(String str) => ContentResponse.fromJson(json.decode(str));

String contentResponseToJson(ContentResponse data) => json.encode(data.toJson());

class ContentResponse {
  ContentResponse({
    this.content,
    this.pagedContentParts,
    this.contentSerie,
    this.pagedComments,
    this.pagedRelatedContentSeries,
  });

  Content content;
  Paged pagedContentParts;
  ContentSerieContentResponse contentSerie;
  Paged pagedComments;
  Paged pagedRelatedContentSeries;

  factory ContentResponse.fromJson(Map<String, dynamic> json) => ContentResponse(
    content: json["content"] == null ? null : Content.fromJson(json["content"]),
    pagedContentParts: json["pagedContentParts"] == null ? null : Paged.fromJson(json["pagedContentParts"]),
    contentSerie: json["contentSerie"] == null ? null : ContentSerieContentResponse.fromJson(json["contentSerie"]),
    pagedComments: json["pagedComments"] == null ? null : Paged.fromJson(json["pagedComments"]),
    pagedRelatedContentSeries: json["pagedRelatedContentSeries"] == null ? null : Paged.fromJson(json["pagedRelatedContentSeries"]),
  );

  Map<String, dynamic> toJson() => {
    "content": content == null ? null : content.toJson(),
    "pagedContentParts": pagedContentParts == null ? null : pagedContentParts.toJson(),
    "contentSerie": contentSerie == null ? null : contentSerie.toJson(),
    "pagedComments": pagedComments == null ? null : pagedComments.toJson(),
    "pagedRelatedContentSeries": pagedRelatedContentSeries == null ? null : pagedRelatedContentSeries.toJson(),
  };
}

class Content {
  Content({
    this.id,
    this.title,
    this.isPurchased,
    this.contentSerieLikeCount,
    this.playLinks,
    this.contentSerieRate,
    this.isUserLike,
    this.description,
    this.duration,
    this.isMain,
    this.imageFileName,
    this.contentSerieImageFileName,
    this.contentSerieKey,
    this.key,
    this.sequence,
    this.isPublished,
    this.type,
    this.contentSerieId,
    this.isFree,
    this.categoryTitle,
    this.contentSerieTitle,
    this.isBookmarked,
    this.producer,
    this.alias,
    this.shareLink,
    this.tags,
    this.isDeleted,
    this.contentSerieResult,
    this.imageUrl,
    this.previewLink,
  });

  int id;
  String title;
  bool isPurchased;
  int contentSerieLikeCount;
  List<PlayLink> playLinks;
  dynamic contentSerieRate;
  bool isUserLike;
  String description;
  String duration;
  bool isMain;
  String imageFileName;
  ImageFileName contentSerieImageFileName;
  Key contentSerieKey;
  String key;
  int sequence;
  bool isPublished;
  int type;
  int contentSerieId;
  bool isFree;
  CategoryTitle categoryTitle;
  ContentSerieTitle contentSerieTitle;
  bool isBookmarked;
  String producer;
  String alias;
  String shareLink;
  List<dynamic> tags;
  bool isDeleted;
  dynamic contentSerieResult;
  String imageUrl;
  String previewLink;

  factory Content.fromJson(Map<String, dynamic> json) => Content(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    isPurchased: json["isPurchased"] == null ? null : json["isPurchased"],
    contentSerieLikeCount: json["contentSerieLikeCount"] == null ? null : json["contentSerieLikeCount"],
    playLinks: json["playLinks"] == null ? null : List<PlayLink>.from(json["playLinks"].map((x) => PlayLink.fromJson(x))),
    contentSerieRate: json["contentSerieRate"] == null ? null : json["contentSerieRate"],
    isUserLike: json["isUserLike"] == null ? null : json["isUserLike"],
    description: json["description"] == null ? null : json["description"],
    duration: json["duration"] == null ? null : json["duration"],
    isMain: json["isMain"] == null ? null : json["isMain"],
    imageFileName: json["imageFileName"] == null ? null : json["imageFileName"],
    contentSerieImageFileName: json["contentSerieImageFileName"] == null ? null : imageFileNameValues.map[json["contentSerieImageFileName"]],
    contentSerieKey: json["contentSerieKey"] == null ? null : keyValues.map[json["contentSerieKey"]],
    key: json["key"] == null ? null : json["key"],
    sequence: json["sequence"] == null ? null : json["sequence"],
    isPublished: json["isPublished"] == null ? null : json["isPublished"],
    type: json["type"] == null ? null : json["type"],
    contentSerieId: json["contentSerieId"] == null ? null : json["contentSerieId"],
    isFree: json["isFree"] == null ? null : json["isFree"],
    categoryTitle: json["categoryTitle"] == null ? null : categoryTitleValues.map[json["categoryTitle"]],
    contentSerieTitle: json["contentSerieTitle"] == null ? null : contentSerieTitleValues.map[json["contentSerieTitle"]],
    isBookmarked: json["isBookmarked"] == null ? null : json["isBookmarked"],
    producer: json["producer"] == null ? null : json["producer"],
    alias: json["alias"] == null ? null : json["alias"],
    shareLink: json["shareLink"] == null ? null : json["shareLink"],
    tags: json["tags"] == null ? null : List<dynamic>.from(json["tags"].map((x) => x)),
    isDeleted: json["isDeleted"] == null ? null : json["isDeleted"],
    contentSerieResult: json["contentSerieResult"],
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
    previewLink: json["previewLink"] == null ? null : json["previewLink"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "isPurchased": isPurchased == null ? null : isPurchased,
    "contentSerieLikeCount": contentSerieLikeCount == null ? null : contentSerieLikeCount,
    "playLinks": playLinks == null ? null : List<dynamic>.from(playLinks.map((x) => x.toJson())),
    "contentSerieRate": contentSerieRate == null ? null : contentSerieRate,
    "isUserLike": isUserLike == null ? null : isUserLike,
    "description": description == null ? null : description,
    "duration": duration == null ? null : duration,
    "isMain": isMain == null ? null : isMain,
    "imageFileName": imageFileName == null ? null : imageFileName,
    "contentSerieImageFileName": contentSerieImageFileName == null ? null : imageFileNameValues.reverse[contentSerieImageFileName],
    "contentSerieKey": contentSerieKey == null ? null : keyValues.reverse[contentSerieKey],
    "key": key == null ? null : key,
    "sequence": sequence == null ? null : sequence,
    "isPublished": isPublished == null ? null : isPublished,
    "type": type == null ? null : type,
    "contentSerieId": contentSerieId == null ? null : contentSerieId,
    "isFree": isFree == null ? null : isFree,
    "categoryTitle": categoryTitle == null ? null : categoryTitleValues.reverse[categoryTitle],
    "contentSerieTitle": contentSerieTitle == null ? null : contentSerieTitleValues.reverse[contentSerieTitle],
    "isBookmarked": isBookmarked == null ? null : isBookmarked,
    "producer": producer == null ? null : producer,
    "alias": alias == null ? null : alias,
    "shareLink": shareLink == null ? null : shareLink,
    "tags": tags == null ? null : List<dynamic>.from(tags.map((x) => x)),
    "isDeleted": isDeleted == null ? null : isDeleted,
    "contentSerieResult": contentSerieResult,
    "imageUrl": imageUrl == null ? null : imageUrl,
    "previewLink": previewLink == null ? null : previewLink,
  };
}

enum CategoryTitle { ECONOMY_AND_BUSINESS }

final categoryTitleValues = EnumValues({
  "Economy and Business": CategoryTitle.ECONOMY_AND_BUSINESS
});

enum ImageFileName { BUSINESS_PNG, CRYPTOCURRENCY_PNG }

final imageFileNameValues = EnumValues({
  "Business.png": ImageFileName.BUSINESS_PNG,
  "cryptocurrency.png": ImageFileName.CRYPTOCURRENCY_PNG
});

enum Key { BUSINESS, CRYPTOCURRENCY }

final keyValues = EnumValues({
  "business": Key.BUSINESS,
  "Cryptocurrency": Key.CRYPTOCURRENCY
});

enum ContentSerieTitle { BUSINESS, CRYPTOCURRENCY }

final contentSerieTitleValues = EnumValues({
  "Business": ContentSerieTitle.BUSINESS,
  "Cryptocurrency": ContentSerieTitle.CRYPTOCURRENCY
});

class PlayLink {
  PlayLink({
    this.name,
    this.link,
  });

  String name;
  String link;

  factory PlayLink.fromJson(Map<String, dynamic> json) => PlayLink(
    name: json["name"] == null ? null : json["name"],
    link: json["link"] == null ? null : json["link"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "link": link == null ? null : link,
  };
}

class ContentSerieContentResponse {
  ContentSerieContentResponse({
    this.id,
    this.key,
    this.name,
    this.title,
    this.description,
    this.imageFileName,
    this.isPurchased,
    this.isPublished,
    this.partsCount,
    this.producer,
    this.producerId,
    this.totalDuration,
    this.categoryTitle,
    this.creationDateTime,
    this.purchaseNotice,
    this.priceString,
    this.packageId,
    this.purchaseKeyword,
    this.purchaseOffKeyword,
    this.userLikesCount,
    this.averageRate,
    this.rateParticipantsCount,
    this.videoCounts,
    this.isFree,
    this.contents,
    this.categories,
    this.tags,
    this.comments,
    this.imageUrl,
  });

  int id;
  String key;
  String name;
  String title;
  dynamic description;
  ImageFileName imageFileName;
  bool isPurchased;
  bool isPublished;
  int partsCount;
  String producer;
  int producerId;
  dynamic totalDuration;
  String categoryTitle;
  String creationDateTime;
  dynamic purchaseNotice;
  dynamic priceString;
  int packageId;
  dynamic purchaseKeyword;
  dynamic purchaseOffKeyword;
  int userLikesCount;
  dynamic averageRate;
  int rateParticipantsCount;
  int videoCounts;
  bool isFree;
  List<Content> contents;
  List<Category> categories;
  List<dynamic> tags;
  List<dynamic> comments;
  String imageUrl;

  factory ContentSerieContentResponse.fromJson(Map<String, dynamic> json) => ContentSerieContentResponse(
    id: json["id"] == null ? null : json["id"],
    key: json["key"] == null ? null : json["key"],
    name: json["name"] == null ? null :json["name"],
    title: json["title"] == null ? null : json["title"],
    description: json["description"],
    imageFileName: json["imageFileName"] == null ? null : imageFileNameValues.map[json["imageFileName"]],
    isPurchased: json["isPurchased"] == null ? null : json["isPurchased"],
    isPublished: json["isPublished"] == null ? null : json["isPublished"],
    partsCount: json["partsCount"] == null ? null : json["partsCount"],
    producer: json["producer"] == null ? null : json["producer"],
    producerId: json["producerId"] == null ? null : json["producerId"],
    totalDuration: json["totalDuration"],
    categoryTitle: json["categoryTitle"] == null ? null : json["categoryTitle"],
    creationDateTime: json["creationDateTime"] == null ? null : json["creationDateTime"],
    purchaseNotice: json["purchaseNotice"],
    priceString: json["priceString"],
    packageId: json["packageId"] == null ? null : json["packageId"],
    purchaseKeyword: json["purchaseKeyword"],
    purchaseOffKeyword: json["purchaseOffKeyword"],
    userLikesCount: json["userLikesCount"] == null ? null : json["userLikesCount"],
    averageRate: json["averageRate"] == null ? null : json["averageRate"],
    rateParticipantsCount: json["rate_ParticipantsCount"] == null ? null : json["rate_ParticipantsCount"],
    videoCounts: json["videoCounts"] == null ? null : json["videoCounts"],
    isFree: json["isFree"] == null ? null : json["isFree"],
    contents: json["contents"] == null ? null : List<Content>.from(json["contents"].map((x) => Content.fromJson(x))),
    categories: json["categories"] == null ? null : List<Category>.from(json["categories"].map((x) => Category.fromJson(x))),
    tags: json["tags"] == null ? null : List<dynamic>.from(json["tags"].map((x) => x)),
    comments: json["comments"] == null ? null : List<dynamic>.from(json["comments"].map((x) => x)),
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "key": key == null ? null : key,
    "name": name == null ? null : name,
    "title": title == null ? null : title,
    "description": description,
    "imageFileName": imageFileName == null ? null : imageFileNameValues.reverse[imageFileName],
    "isPurchased": isPurchased == null ? null : isPurchased,
    "isPublished": isPublished == null ? null : isPublished,
    "partsCount": partsCount == null ? null : partsCount,
    "producer": producer == null ? null : producer,
    "producerId": producerId == null ? null : producerId,
    "totalDuration": totalDuration,
    "categoryTitle": categoryTitle == null ? null : categoryTitle,
    "creationDateTime": creationDateTime == null ? null : creationDateTime,
    "purchaseNotice": purchaseNotice,
    "priceString": priceString,
    "packageId": packageId == null ? null : packageId,
    "purchaseKeyword": purchaseKeyword,
    "purchaseOffKeyword": purchaseOffKeyword,
    "userLikesCount": userLikesCount == null ? null : userLikesCount,
    "averageRate": averageRate == null ? null : averageRate,
    "rate_ParticipantsCount": rateParticipantsCount == null ? null : rateParticipantsCount,
    "videoCounts": videoCounts == null ? null : videoCounts,
    "isFree": isFree == null ? null : isFree,
    "contents": contents == null ? null : List<dynamic>.from(contents.map((x) => x.toJson())),
    "categories": categories == null ? null : List<dynamic>.from(categories.map((x) => x.toJson())),
    "tags": tags == null ? null : List<dynamic>.from(tags.map((x) => x)),
    "comments": comments == null ? null : List<dynamic>.from(comments.map((x) => x)),
    "imageUrl": imageUrl == null ? null : imageUrl,
  };
}

class Category {
  Category({
    this.id,
    this.name,
    this.title,
    this.key,
    this.imageFileName,
    this.webImageFileName,
    this.contentSerieCount,
    this.contentCount,
    this.subCategories,
    this.parent,
    this.parentId,
    this.imageUrl,
    this.webImageUrl,
  });

  int id;
  CategoryTitle name;
  CategoryTitle title;
  CategoryTitle key;
  String imageFileName;
  String webImageFileName;
  int contentSerieCount;
  int contentCount;
  dynamic subCategories;
  dynamic parent;
  dynamic parentId;
  String imageUrl;
  String webImageUrl;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : categoryTitleValues.map[json["name"]],
    title: json["title"] == null ? null : categoryTitleValues.map[json["title"]],
    key: json["key"] == null ? null : categoryTitleValues.map[json["key"]],
    imageFileName: json["imageFileName"] == null ? null : json["imageFileName"],
    webImageFileName: json["webImageFileName"] == null ? null : json["webImageFileName"],
    contentSerieCount: json["contentSerieCount"] == null ? null : json["contentSerieCount"],
    contentCount: json["contentCount"] == null ? null : json["contentCount"],
    subCategories: json["subCategories"],
    parent: json["parent"],
    parentId: json["parentId"],
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
    webImageUrl: json["webImageUrl"] == null ? null : json["webImageUrl"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : categoryTitleValues.reverse[name],
    "title": title == null ? null : categoryTitleValues.reverse[title],
    "key": key == null ? null : categoryTitleValues.reverse[key],
    "imageFileName": imageFileName == null ? null : imageFileName,
    "webImageFileName": webImageFileName == null ? null : webImageFileName,
    "contentSerieCount": contentSerieCount == null ? null : contentSerieCount,
    "contentCount": contentCount == null ? null : contentCount,
    "subCategories": subCategories,
    "parent": parent,
    "parentId": parentId,
    "imageUrl": imageUrl == null ? null : imageUrl,
    "webImageUrl": webImageUrl == null ? null : webImageUrl,
  };
}

class Paged {
  Paged({
    this.count,
    this.items,
    this.page,
    this.pageSize,
    this.totalPage,
  });

  int count;
  List<Item> items;
  int page;
  int pageSize;
  int totalPage;

  factory Paged.fromJson(Map<String, dynamic> json) => Paged(
    count: json["count"] == null ? null : json["count"],
    items: json["items"] == null ? null : List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
    page: json["page"] == null ? null : json["page"],
    pageSize: json["pageSize"] == null ? null : json["pageSize"],
    totalPage: json["totalPage"] == null ? null : json["totalPage"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "items": items == null ? null : List<dynamic>.from(items.map((x) => x.toJson())),
    "page": page == null ? null : page,
    "pageSize": pageSize == null ? null : pageSize,
    "totalPage": totalPage == null ? null : totalPage,
  };
}

class Item {
  Item({
    this.id,
    this.title,
    this.userNickName,
    this.text,
    this.duration,
    this.producer,
    this.key,
    this.imageFileName,
    this.isPurchased,
    this.purchaseKeyword,
    this.purchaseNotice,
    this.contentSerieId,
    this.isBookmarked,
    this.contentSerieTitle,
    this.categoryTitle,
    this.isFree,
    this.playLinks,
    this.imageUrl,
    this.name,
    this.description,
    this.isPublished,
    this.partsCount,
    this.producerId,
    this.totalDuration,
    this.creationDateTime,
    this.priceString,
    this.packageId,
    this.purchaseOffKeyword,
    this.userLikesCount,
    this.averageRate,
    this.rateParticipantsCount,
    this.videoCounts,
    this.contents,
    this.categories,
    this.tags,
    this.comments,
  });

  int id;
  String title;
  String text;
  String userNickName;
  String duration;
  String producer;
  String key;
  String imageFileName;
  bool isPurchased;
  dynamic purchaseKeyword;
  dynamic purchaseNotice;
  int contentSerieId;
  bool isBookmarked;
  ContentSerieTitle contentSerieTitle;
  String categoryTitle;
  bool isFree;
  List<PlayLink> playLinks;
  String imageUrl;
  ContentSerieTitle name;
  dynamic description;
  bool isPublished;
  int partsCount;
  int producerId;
  dynamic totalDuration;
  String creationDateTime;
  dynamic priceString;
  int packageId;
  dynamic purchaseOffKeyword;
  int userLikesCount;
  dynamic averageRate;
  int rateParticipantsCount;
  int videoCounts;
  List<Content> contents;
  List<Category> categories;
  List<dynamic> tags;
  List<dynamic> comments;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    text: json["text"] == null ? null : json["text"],
    userNickName: json["userNickName"] == null ? null : json["userNickName"],
    duration: json["duration"] == null ? null : json["duration"],
    producer: json["producer"] == null ? null : json["producer"],
    key: json["key"] == null ? null : json["key"],
    imageFileName: json["imageFileName"] == null ? null : json["imageFileName"],
    isPurchased: json["isPurchased"] == null ? null : json["isPurchased"],
    purchaseKeyword: json["purchaseKeyword"],
    purchaseNotice: json["purchaseNotice"],
    contentSerieId: json["contentSerieId"] == null ? null : json["contentSerieId"],
    isBookmarked: json["isBookmarked"] == null ? null : json["isBookmarked"],
    contentSerieTitle: json["contentSerieTitle"] == null ? null : contentSerieTitleValues.map[json["contentSerieTitle"]],
    categoryTitle: json["categoryTitle"] == null ? null : json["categoryTitle"],
    isFree: json["isFree"] == null ? null : json["isFree"],
    playLinks: json["playLinks"] == null ? null : List<PlayLink>.from(json["playLinks"].map((x) => PlayLink.fromJson(x))),
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
    name: json["name"] == null ? null : contentSerieTitleValues.map[json["name"]],
    description: json["description"],
    isPublished: json["isPublished"] == null ? null : json["isPublished"],
    partsCount: json["partsCount"] == null ? null : json["partsCount"],
    producerId: json["producerId"] == null ? null : json["producerId"],
    totalDuration: json["totalDuration"],
    creationDateTime: json["creationDateTime"] == null ? null : json["creationDateTime"],
    priceString: json["priceString"],
    packageId: json["packageId"] == null ? null : json["packageId"],
    purchaseOffKeyword: json["purchaseOffKeyword"],
    userLikesCount: json["userLikesCount"] == null ? null : json["userLikesCount"],
    averageRate: json["averageRate"] == null ? null : json["averageRate"],
    rateParticipantsCount: json["rate_ParticipantsCount"] == null ? null : json["rate_ParticipantsCount"],
    videoCounts: json["videoCounts"] == null ? null : json["videoCounts"],
    contents: json["contents"] == null ? null : List<Content>.from(json["contents"].map((x) => Content.fromJson(x))),
    categories: json["categories"] == null ? null : List<Category>.from(json["categories"].map((x) => Category.fromJson(x))),
    tags: json["tags"] == null ? null : List<dynamic>.from(json["tags"].map((x) => x)),
    comments: json["comments"] == null ? null : List<dynamic>.from(json["comments"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "duration": duration == null ? null : duration,
    "producer": producer == null ? null : producer,
    "key": key == null ? null : key,
    "imageFileName": imageFileName == null ? null : imageFileName,
    "isPurchased": isPurchased == null ? null : isPurchased,
    "purchaseKeyword": purchaseKeyword,
    "purchaseNotice": purchaseNotice,
    "contentSerieId": contentSerieId == null ? null : contentSerieId,
    "isBookmarked": isBookmarked == null ? null : isBookmarked,
    "contentSerieTitle": contentSerieTitle == null ? null : contentSerieTitleValues.reverse[contentSerieTitle],
    "categoryTitle": categoryTitle == null ? null : categoryTitle,
    "isFree": isFree == null ? null : isFree,
    "playLinks": playLinks == null ? null : List<dynamic>.from(playLinks.map((x) => x.toJson())),
    "imageUrl": imageUrl == null ? null : imageUrl,
    "name": name == null ? null : contentSerieTitleValues.reverse[name],
    "description": description,
    "isPublished": isPublished == null ? null : isPublished,
    "partsCount": partsCount == null ? null : partsCount,
    "producerId": producerId == null ? null : producerId,
    "totalDuration": totalDuration,
    "creationDateTime": creationDateTime == null ? null : creationDateTime,
    "priceString": priceString,
    "packageId": packageId == null ? null : packageId,
    "purchaseOffKeyword": purchaseOffKeyword,
    "userLikesCount": userLikesCount == null ? null : userLikesCount,
    "averageRate": averageRate == null ? null : averageRate,
    "rate_ParticipantsCount": rateParticipantsCount == null ? null : rateParticipantsCount,
    "videoCounts": videoCounts == null ? null : videoCounts,
    "contents": contents == null ? null : List<dynamic>.from(contents.map((x) => x.toJson())),
    "categories": categories == null ? null : List<dynamic>.from(categories.map((x) => x.toJson())),
    "tags": tags == null ? null : List<dynamic>.from(tags.map((x) => x)),
    "comments": comments == null ? null : List<dynamic>.from(comments.map((x) => x)),
  };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
