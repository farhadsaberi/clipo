// To parse this JSON data, do
//
//     final subCategoryResponse = subCategoryResponseFromJson(jsonString);

import 'dart:convert';

SubCategoryResponse subCategoryResponseFromJson(String str) => SubCategoryResponse.fromJson(json.decode(str));

String subCategoryResponseToJson(SubCategoryResponse data) => json.encode(data.toJson());

class SubCategoryResponse {
  SubCategoryResponse({
    this.count,
    this.items,
    this.page,
    this.pageSize,
    this.totalPage,
  });

  int count;
  List<ItemSubCategory> items;
  int page;
  int pageSize;
  int totalPage;

  factory SubCategoryResponse.fromJson(Map<String, dynamic> json) => SubCategoryResponse(
    count: json["count"] == null ? null : json["count"],
    items: json["items"] == null ? null : List<ItemSubCategory>.from(json["items"].map((x) => ItemSubCategory.fromJson(x))),
    page: json["page"] == null ? null : json["page"],
    pageSize: json["pageSize"] == null ? null : json["pageSize"],
    totalPage: json["totalPage"] == null ? null : json["totalPage"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "items": items == null ? null : List<dynamic>.from(items.map((x) => x.toJson())),
    "page": page == null ? null : page,
    "pageSize": pageSize == null ? null : pageSize,
    "totalPage": totalPage == null ? null : totalPage,
  };
}

class ItemSubCategory {
  ItemSubCategory({
    this.id,
    this.key,
    this.name,
    this.title,
    this.description,
    this.imageFileName,
    this.isPurchased,
    this.isPublished,
    this.partsCount,
    this.producer,
    this.producerId,
    this.totalDuration,
    this.categoryTitle,
    this.creationDateTime,
    this.purchaseNotice,
    this.priceString,
    this.packageId,
    this.purchaseKeyword,
    this.purchaseOffKeyword,
    this.userLikesCount,
    this.averageRate,
    this.rateParticipantsCount,
    this.videoCounts,
    this.isFree,
    this.contents,
    this.categories,
    this.tags,
    this.comments,
    this.imageUrl,
  });

  int id;
  String key;
  String name;
  String title;
  dynamic description;
  String imageFileName;
  bool isPurchased;
  bool isPublished;
  int partsCount;
  String producer;
  int producerId;
  dynamic totalDuration;
  String categoryTitle;
  String creationDateTime;
  dynamic purchaseNotice;
  dynamic priceString;
  int packageId;
  dynamic purchaseKeyword;
  dynamic purchaseOffKeyword;
  int userLikesCount;
  dynamic averageRate;
  int rateParticipantsCount;
  int videoCounts;
  bool isFree;
  List<ContentSubCategory> contents;
  List<Category> categories;
  List<dynamic> tags;
  List<dynamic> comments;
  String imageUrl;

  factory ItemSubCategory.fromJson(Map<String, dynamic> json) => ItemSubCategory(
    id: json["id"] == null ? null : json["id"],
    key: json["key"] == null ? null : json["key"],
    name: json["name"] == null ? null :json["name"],
    title: json["title"] == null ? null : json["title"],
    description: json["description"],
    imageFileName: json["imageFileName"] == null ? null : json["imageFileName"],
    isPurchased: json["isPurchased"] == null ? null : json["isPurchased"],
    isPublished: json["isPublished"] == null ? null : json["isPublished"],
    partsCount: json["partsCount"] == null ? null : json["partsCount"],
    producer: json["producer"] == null ? null : json["producer"],
    producerId: json["producerId"] == null ? null : json["producerId"],
    totalDuration: json["totalDuration"],
    categoryTitle: json["categoryTitle"] == null ? null : json["categoryTitle"],
    creationDateTime: json["creationDateTime"] == null ? null : json["creationDateTime"],
    purchaseNotice: json["purchaseNotice"],
    priceString: json["priceString"],
    packageId: json["packageId"] == null ? null : json["packageId"],
    purchaseKeyword: json["purchaseKeyword"],
    purchaseOffKeyword: json["purchaseOffKeyword"],
    userLikesCount: json["userLikesCount"] == null ? null : json["userLikesCount"],
    averageRate: json["averageRate"] == null ? null : json["averageRate"],
    rateParticipantsCount: json["rate_ParticipantsCount"] == null ? null : json["rate_ParticipantsCount"],
    videoCounts: json["videoCounts"] == null ? null : json["videoCounts"],
    isFree: json["isFree"] == null ? null : json["isFree"],
    contents: json["contents"] == null ? null : List<ContentSubCategory>.from(json["contents"].map((x) => ContentSubCategory.fromJson(x))),
    categories: json["categories"] == null ? null : List<Category>.from(json["categories"].map((x) => Category.fromJson(x))),
    tags: json["tags"] == null ? null : List<dynamic>.from(json["tags"].map((x) => x)),
    comments: json["comments"] == null ? null : List<dynamic>.from(json["comments"].map((x) => x)),
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "key": key == null ? null :key,
    "name": name == null ? null : name,
    "title": title == null ? null : title,
    "description": description,
    "imageFileName": imageFileName == null ? null : imageFileName,
    "isPurchased": isPurchased == null ? null : isPurchased,
    "isPublished": isPublished == null ? null : isPublished,
    "partsCount": partsCount == null ? null : partsCount,
    "producer": producer == null ? null : producer,
    "producerId": producerId == null ? null : producerId,
    "totalDuration": totalDuration,
    "categoryTitle": categoryTitle == null ? null : categoryTitle,
    "creationDateTime": creationDateTime == null ? null : creationDateTime,
    "purchaseNotice": purchaseNotice,
    "priceString": priceString,
    "packageId": packageId == null ? null : packageId,
    "purchaseKeyword": purchaseKeyword,
    "purchaseOffKeyword": purchaseOffKeyword,
    "userLikesCount": userLikesCount == null ? null : userLikesCount,
    "averageRate": averageRate == null ? null : averageRate,
    "rate_ParticipantsCount": rateParticipantsCount == null ? null : rateParticipantsCount,
    "videoCounts": videoCounts == null ? null : videoCounts,
    "isFree": isFree == null ? null : isFree,
    "contents": contents == null ? null : List<dynamic>.from(contents.map((x) => x.toJson())),
    "categories": categories == null ? null : List<dynamic>.from(categories.map((x) => x.toJson())),
    "tags": tags == null ? null : List<dynamic>.from(tags.map((x) => x)),
    "comments": comments == null ? null : List<dynamic>.from(comments.map((x) => x)),
    "imageUrl": imageUrl == null ? null : imageUrl,
  };
}

class Category {
  Category({
    this.id,
    this.name,
    this.title,
    this.key,
    this.imageFileName,
    this.webImageFileName,
    this.contentSerieCount,
    this.contentCount,
    this.subCategories,
    this.parent,
    this.parentId,
    this.imageUrl,
    this.webImageUrl,
  });

  int id;
  String name;
  String title;
  String key;
  String imageFileName;
  String webImageFileName;
  int contentSerieCount;
  int contentCount;
  dynamic subCategories;
  dynamic parent;
  dynamic parentId;
  String imageUrl;
  String webImageUrl;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    title: json["title"] == null ? null : json["title"],
    key: json["key"] == null ? null :json["key"],
    imageFileName: json["imageFileName"] == null ? null : json["imageFileName"],
    webImageFileName: json["webImageFileName"] == null ? null : json["webImageFileName"],
    contentSerieCount: json["contentSerieCount"] == null ? null : json["contentSerieCount"],
    contentCount: json["contentCount"] == null ? null : json["contentCount"],
    subCategories: json["subCategories"],
    parent: json["parent"],
    parentId: json["parentId"],
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
    webImageUrl: json["webImageUrl"] == null ? null : json["webImageUrl"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null :name,
    "title": title == null ? null : title,
    "key": key == null ? null : key,
    "imageFileName": imageFileName == null ? null : imageFileName,
    "webImageFileName": webImageFileName == null ? null : webImageFileName,
    "contentSerieCount": contentSerieCount == null ? null : contentSerieCount,
    "contentCount": contentCount == null ? null : contentCount,
    "subCategories": subCategories,
    "parent": parent,
    "parentId": parentId,
    "imageUrl": imageUrl == null ? null : imageUrl,
    "webImageUrl": webImageUrl == null ? null : webImageUrl,
  };
}


class ContentSubCategory {
  ContentSubCategory({
    this.id,
    this.title,
    this.isPurchased,
    this.contentSerieLikeCount,
    this.playLinks,
    this.contentSerieRate,
    this.isUserLike,
    this.description,
    this.duration,
    this.isMain,
    this.imageFileName,
    this.contentSerieImageFileName,
    this.contentSerieKey,
    this.key,
    this.sequence,
    this.isPublished,
    this.type,
    this.contentSerieId,
    this.isFree,
    this.categoryTitle,
    this.contentSerieTitle,
    this.isBookmarked,
    this.producer,
    this.alias,
    this.shareLink,
    this.tags,
    this.isDeleted,
    this.contentSerieResult,
    this.imageUrl,
    this.previewLink,
  });

  int id;
  String title;
  bool isPurchased;
  int contentSerieLikeCount;
  dynamic playLinks;
  dynamic contentSerieRate;
  bool isUserLike;
  String description;
  String duration;
  bool isMain;
  String imageFileName;
  String contentSerieImageFileName;
  String contentSerieKey;
  String key;
  int sequence;
  bool isPublished;
  int type;
  int contentSerieId;
  bool isFree;
  String categoryTitle;
  String contentSerieTitle;
  bool isBookmarked;
  dynamic producer;
  String alias;
  String shareLink;
  List<dynamic> tags;
  bool isDeleted;
  dynamic contentSerieResult;
  String imageUrl;
  String previewLink;

  factory ContentSubCategory.fromJson(Map<String, dynamic> json) => ContentSubCategory(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    isPurchased: json["isPurchased"] == null ? null : json["isPurchased"],
    contentSerieLikeCount: json["contentSerieLikeCount"] == null ? null : json["contentSerieLikeCount"],
    playLinks: json["playLinks"],
    contentSerieRate: json["contentSerieRate"] == null ? null : json["contentSerieRate"],
    isUserLike: json["isUserLike"] == null ? null : json["isUserLike"],
    description: json["description"] == null ? null : json["description"],
    duration: json["duration"] == null ? null : json["duration"],
    isMain: json["isMain"] == null ? null : json["isMain"],
    imageFileName: json["imageFileName"] == null ? null : json["imageFileName"],
    contentSerieImageFileName: json["contentSerieImageFileName"] == null ? null : json["contentSerieImageFileName"],
    contentSerieKey: json["contentSerieKey"] == null ? null : json["contentSerieKey"],
    key: json["key"] == null ? null : json["key"],
    sequence: json["sequence"] == null ? null : json["sequence"],
    isPublished: json["isPublished"] == null ? null : json["isPublished"],
    type: json["type"] == null ? null : json["type"],
    contentSerieId: json["contentSerieId"] == null ? null : json["contentSerieId"],
    isFree: json["isFree"] == null ? null : json["isFree"],
    categoryTitle: json["categoryTitle"] == null ? null : json["categoryTitle"],
    contentSerieTitle: json["contentSerieTitle"] == null ? null : json["contentSerieTitle"],
    isBookmarked: json["isBookmarked"] == null ? null : json["isBookmarked"],
    producer: json["producer"],
    alias: json["alias"] == null ? null : json["alias"],
    shareLink: json["shareLink"] == null ? null : json["shareLink"],
    tags: json["tags"] == null ? null : List<dynamic>.from(json["tags"].map((x) => x)),
    isDeleted: json["isDeleted"] == null ? null : json["isDeleted"],
    contentSerieResult: json["contentSerieResult"],
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
    previewLink: json["previewLink"] == null ? null : json["previewLink"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "isPurchased": isPurchased == null ? null : isPurchased,
    "contentSerieLikeCount": contentSerieLikeCount == null ? null : contentSerieLikeCount,
    "playLinks": playLinks,
    "contentSerieRate": contentSerieRate == null ? null : contentSerieRate,
    "isUserLike": isUserLike == null ? null : isUserLike,
    "description": description == null ? null : description,
    "duration": duration == null ? null : duration,
    "isMain": isMain == null ? null : isMain,
    "imageFileName": imageFileName == null ? null : imageFileName,
    "contentSerieImageFileName": contentSerieImageFileName == null ? null : contentSerieImageFileName,
    "contentSerieKey": contentSerieKey == null ? null : contentSerieKey,
    "key": key == null ? null : key,
    "sequence": sequence == null ? null : sequence,
    "isPublished": isPublished == null ? null : isPublished,
    "type": type == null ? null : type,
    "contentSerieId": contentSerieId == null ? null : contentSerieId,
    "isFree": isFree == null ? null : isFree,
    "categoryTitle": categoryTitle == null ? null : categoryTitle,
    "contentSerieTitle": contentSerieTitle == null ? null : contentSerieTitle,
    "isBookmarked": isBookmarked == null ? null : isBookmarked,
    "producer": producer,
    "alias": alias == null ? null : alias,
    "shareLink": shareLink == null ? null : shareLink,
    "tags": tags == null ? null : List<dynamic>.from(tags.map((x) => x)),
    "isDeleted": isDeleted == null ? null : isDeleted,
    "contentSerieResult": contentSerieResult,
    "imageUrl": imageUrl == null ? null : imageUrl,
    "previewLink": previewLink == null ? null : previewLink,
  };
}


