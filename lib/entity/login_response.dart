// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    this.accessToken,
    this.refreshToken,
    this.profile,
  });

  AccessToken accessToken;
  AccessToken refreshToken;
  Profile profile;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    accessToken: json["accessToken"] == null ? null : AccessToken.fromJson(json["accessToken"]),
    refreshToken: json["refreshToken"] == null ? null : AccessToken.fromJson(json["refreshToken"]),
    profile: json["profile"] == null ? null : Profile.fromJson(json["profile"]),
  );

  Map<String, dynamic> toJson() => {
    "accessToken": accessToken == null ? null : accessToken.toJson(),
    "refreshToken": refreshToken,
    "profile": profile == null ? null : profile.toJson(),
  };
}

class AccessToken {
  AccessToken({
    this.expireAt,
    this.token,
  });

  DateTime expireAt;
  String token;

  factory AccessToken.fromJson(Map<String, dynamic> json) => AccessToken(
    expireAt: json["expireAt"] == null ? null : DateTime.parse(json["expireAt"]),
    token: json["token"] == null ? null : json["token"],
  );

  Map<String, dynamic> toJson() => {
    "expireAt": expireAt == null ? null : expireAt.toIso8601String(),
    "token": token == null ? null : token,
  };
}

class Profile {
  Profile({
    this.msisdn,
    this.isPasswordSet,
    this.nickName,
  });

  String msisdn;
  bool isPasswordSet;
  dynamic nickName;

  factory Profile.fromJson(Map<String, dynamic> json) => Profile(
    msisdn: json["msisdn"] == null ? null : json["msisdn"],
    isPasswordSet: json["isPasswordSet"] == null ? null : json["isPasswordSet"],
    nickName: json["nickName"],
  );

  Map<String, dynamic> toJson() => {
    "msisdn": msisdn == null ? null : msisdn,
    "isPasswordSet": isPasswordSet == null ? null : isPasswordSet,
    "nickName": nickName,
  };
}
