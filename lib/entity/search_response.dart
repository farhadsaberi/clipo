// To parse this JSON data, do
//
//     final searchResponse = searchResponseFromJson(jsonString);

import 'dart:convert';

SearchResponse searchResponseFromJson(String str) => SearchResponse.fromJson(json.decode(str));

String searchResponseToJson(SearchResponse data) => json.encode(data.toJson());

class SearchResponse {
  SearchResponse({
    this.contents,
    this.contentSeries,
    this.categories,
  });

  Categories contents;
  Categories contentSeries;
  Categories categories;

  factory SearchResponse.fromJson(Map<String, dynamic> json) => SearchResponse(
    contents: json["contents"] == null ? null : Categories.fromJson(json["contents"]),
    contentSeries: json["contentSeries"] == null ? null : Categories.fromJson(json["contentSeries"]),
    categories: json["categories"] == null ? null : Categories.fromJson(json["categories"]),
  );

  Map<String, dynamic> toJson() => {
    "contents": contents == null ? null : contents.toJson(),
    "contentSeries": contentSeries == null ? null : contentSeries.toJson(),
    "categories": categories == null ? null : categories.toJson(),
  };
}

class Categories {
  Categories({
    this.count,
    this.items,
    this.page,
    this.pageSize,
    this.totalPage,
  });

  int count;
  List<ItemSearch> items;
  int page;
  int pageSize;
  int totalPage;

  factory Categories.fromJson(Map<String, dynamic> json) => Categories(
    count: json["count"] == null ? null : json["count"],
    items: json["items"] == null ? null : List<ItemSearch>.from(json["items"].map((x) => ItemSearch.fromJson(x))),
    page: json["page"] == null ? null : json["page"],
    pageSize: json["pageSize"] == null ? null : json["pageSize"],
    totalPage: json["totalPage"] == null ? null : json["totalPage"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "items": items == null ? null : List<dynamic>.from(items.map((x) => x.toJson())),
    "page": page == null ? null : page,
    "pageSize": pageSize == null ? null : pageSize,
    "totalPage": totalPage == null ? null : totalPage,
  };
}

class ItemSearch {
  ItemSearch({
    this.id,
    this.title,
    this.duration,
    this.producer,
    this.key,
    this.imageFileName,
    this.isPurchased,
    this.purchaseKeyword,
    this.purchaseNotice,
    this.contentSerieId,
    this.isBookmarked,
    this.contentSerieTitle,
    this.categoryTitle,
    this.isFree,
    this.playLinks,
    this.imageUrl,
  });

  int id;
  String title;
  String duration;
  String producer;
  String key;
  String imageFileName;
  bool isPurchased;
  dynamic purchaseKeyword;
  dynamic purchaseNotice;
  int contentSerieId;
  bool isBookmarked;
  String contentSerieTitle;
  String categoryTitle;
  bool isFree;
  List<String> playLinks;
  String imageUrl;

  factory ItemSearch.fromJson(Map<String, dynamic> json) => ItemSearch(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    duration: json["duration"] == null ? null : json["duration"],
    producer: json["producer"] == null ? null : json["producer"],
    key: json["key"] == null ? null : json["key"],
    imageFileName: json["imageFileName"] == null ? null : json["imageFileName"],
    isPurchased: json["isPurchased"] == null ? null : json["isPurchased"],
    purchaseKeyword: json["purchaseKeyword"],
    purchaseNotice: json["purchaseNotice"],
    contentSerieId: json["contentSerieId"] == null ? null : json["contentSerieId"],
    isBookmarked: json["isBookmarked"] == null ? null : json["isBookmarked"],
    contentSerieTitle: json["contentSerieTitle"] == null ? null : json["contentSerieTitle"],
    categoryTitle: json["categoryTitle"] == null ? null : json["categoryTitle"],
    isFree: json["isFree"] == null ? null : json["isFree"],
//    playLinks: json["playLinks"] == null ? null : List<String>.from(json["playLinks"].map((x) => String.fromJson(x))),
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "duration": duration == null ? null : duration,
    "producer": producer == null ? null :producer,
    "key": key == null ? null : key,
    "imageFileName": imageFileName == null ? null : imageFileName,
    "isPurchased": isPurchased == null ? null : isPurchased,
    "purchaseKeyword": purchaseKeyword,
    "purchaseNotice": purchaseNotice,
    "contentSerieId": contentSerieId == null ? null : contentSerieId,
    "isBookmarked": isBookmarked == null ? null : isBookmarked,
    "contentSerieTitle": contentSerieTitle == null ? null : contentSerieTitle,
    "categoryTitle": categoryTitle == null ? null : categoryTitle,
    "isFree": isFree == null ? null : isFree,
//    "playLinks": playLinks == null ? null : List<dynamic>.from(playLinks.map((x) => x.toJson())),
    "imageUrl": imageUrl == null ? null : imageUrl,
  };
}
