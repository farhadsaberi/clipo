// To parse this JSON data, do
//
//     final bookmarkResponse = bookmarkResponseFromJson(jsonString);

import 'dart:convert';

BookmarkResponse bookmarkResponseFromJson(String str) => BookmarkResponse.fromJson(json.decode(str));

String bookmarkResponseToJson(BookmarkResponse data) => json.encode(data.toJson());

class BookmarkResponse {
  BookmarkResponse({
    this.count,
    this.items,
    this.page,
    this.pageSize,
    this.totalPage,
  });

  int count;
  List<ItemBookmark> items;
  int page;
  int pageSize;
  int totalPage;

  factory BookmarkResponse.fromJson(Map<String, dynamic> json) => BookmarkResponse(
    count: json["count"] == null ? null : json["count"],
    items: json["items"] == null ? null : List<ItemBookmark>.from(json["items"].map((x) => ItemBookmark.fromJson(x))),
    page: json["page"] == null ? null : json["page"],
    pageSize: json["pageSize"] == null ? null : json["pageSize"],
    totalPage: json["totalPage"] == null ? null : json["totalPage"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "items": items == null ? null : List<dynamic>.from(items.map((x) => x.toJson())),
    "page": page == null ? null : page,
    "pageSize": pageSize == null ? null : pageSize,
    "totalPage": totalPage == null ? null : totalPage,
  };
}

class ItemBookmark {
  ItemBookmark({
    this.id,
    this.title,
    this.duration,
    this.producer,
    this.key,
    this.imageFileName,
    this.isPurchased,
    this.purchaseKeyword,
    this.purchaseNotice,
    this.contentSerieId,
    this.isBookmarked,
    this.contentSerieTitle,
    this.categoryTitle,
    this.isFree,
    this.playLinks,
    this.imageUrl,
  });

  int id;
  String title;
  dynamic duration;
  String producer;
  String key;
  String imageFileName;
  bool isPurchased;
  String purchaseKeyword;
  dynamic purchaseNotice;
  int contentSerieId;
  bool isBookmarked;
  String contentSerieTitle;
  String categoryTitle;
  bool isFree;
  List<PlayLinkBookmark> playLinks;
  String imageUrl;

  factory ItemBookmark.fromJson(Map<String, dynamic> json) => ItemBookmark(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    duration: json["duration"],
    producer: json["producer"] == null ? null : json["producer"],
    key: json["key"] == null ? null : json["key"],
    imageFileName: json["imageFileName"] == null ? null : json["imageFileName"],
    isPurchased: json["isPurchased"] == null ? null : json["isPurchased"],
    purchaseKeyword: json["purchaseKeyword"] == null ? null : json["purchaseKeyword"],
    purchaseNotice: json["purchaseNotice"],
    contentSerieId: json["contentSerieId"] == null ? null : json["contentSerieId"],
    isBookmarked: json["isBookmarked"] == null ? null : json["isBookmarked"],
    contentSerieTitle: json["contentSerieTitle"] == null ? null : json["contentSerieTitle"],
    categoryTitle: json["categoryTitle"] == null ? null : json["categoryTitle"],
    isFree: json["isFree"] == null ? null : json["isFree"],
    playLinks: json["playLinks"] == null ? null : List<PlayLinkBookmark>.from(json["playLinks"].map((x) => PlayLinkBookmark.fromJson(x))),
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "duration": duration,
    "producer": producer == null ? null : producer,
    "key": key == null ? null : key,
    "imageFileName": imageFileName == null ? null : imageFileName,
    "isPurchased": isPurchased == null ? null : isPurchased,
    "purchaseKeyword": purchaseKeyword == null ? null : purchaseKeyword,
    "purchaseNotice": purchaseNotice,
    "contentSerieId": contentSerieId == null ? null : contentSerieId,
    "isBookmarked": isBookmarked == null ? null : isBookmarked,
    "contentSerieTitle": contentSerieTitle == null ? null : contentSerieTitle,
    "categoryTitle": categoryTitle == null ? null : categoryTitle,
    "isFree": isFree == null ? null : isFree,
    "playLinks": playLinks == null ? null : List<dynamic>.from(playLinks.map((x) => x.toJson())),
    "imageUrl": imageUrl == null ? null : imageUrl,
  };
}

class PlayLinkBookmark {
  PlayLinkBookmark({
    this.name,
    this.link,
  });

  String name;
  String link;

  factory PlayLinkBookmark.fromJson(Map<String, dynamic> json) => PlayLinkBookmark(
    name: json["name"] == null ? null : json["name"],
    link: json["link"] == null ? null : json["link"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "link": link == null ? null : link,
  };
}
