// To parse this JSON data, do
//
//     final registerResponse = registerResponseFromJson(jsonString);

import 'dart:convert';

RegisterResponse registerResponseFromJson(String str) => RegisterResponse.fromJson(json.decode(str));

String registerResponseToJson(RegisterResponse data) => json.encode(data.toJson());

class RegisterResponse {
  RegisterResponse({
    this.code,
    this.expirationDateTime,
    this.creationDateTime,
    this.msisdn,
    this.type,
    this.userId,
  });

  String code;
  DateTime expirationDateTime;
  DateTime creationDateTime;
  String msisdn;
  int type;
  int userId;

  factory RegisterResponse.fromJson(Map<String, dynamic> json) => RegisterResponse(
    code: json["code"] == null ? null : json["code"],
    expirationDateTime: json["expirationDateTime"] == null ? null : DateTime.parse(json["expirationDateTime"]),
    creationDateTime: json["creationDateTime"] == null ? null : DateTime.parse(json["creationDateTime"]),
    msisdn: json["msisdn"] == null ? null : json["msisdn"],
    type: json["type"] == null ? null : json["type"],
    userId: json["userId"] == null ? null : json["userId"],
  );

  Map<String, dynamic> toJson() => {
    "code": code == null ? null : code,
    "expirationDateTime": expirationDateTime == null ? null : expirationDateTime.toIso8601String(),
    "creationDateTime": creationDateTime == null ? null : creationDateTime.toIso8601String(),
    "msisdn": msisdn == null ? null : msisdn,
    "type": type == null ? null : type,
    "userId": userId == null ? null : userId,
  };
}
