import 'package:clipo/entity/bookmark_response.dart';
import 'package:clipo/features/bookmark/bookmark_provider.dart';
import 'package:clipo/features/contentserie/parts/content_serie_parts_page.dart';
import 'package:clipo/features/login/ui/register_screen.dart';
import 'package:clipo/features/player/player_page.dart';
import 'package:clipo/generated/l10n.dart';
import 'package:clipo/provider/provider_widget.dart';
import 'package:clipo/provider/view_state_widget.dart';
import 'package:clipo/res/gaps.dart';
import 'package:clipo/utils/image_utils.dart';
import 'package:clipo/utils/navigator.dart';
import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:clipo/utils/widgets/custom_footer_smart_refresh.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class BookmarkPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(
          isBack: true,
          title: S.of(context).bookmarked,
        ),
        body: CustomProviderWidget<BookmarkProvider>(
          provider: BookmarkProvider(),
          onProviderReady: (provider) {
            provider.initData();
          },
          builder: (__, model, _) {
            if (model.isLoading) {
              return ViewStateLoadingWidget();
            } else if (model.isError && model.list.isEmpty) {
              return ViewStateErrorWidget(
                  error: model.viewStateError,
                  onPressed: () {
                    model.getData(isRefresh: true);
                  });
            } else if (model.isEmpty) {
              return ViewStateEmptyWidget(onPressed: () {
                model.getData(isRefresh: true);
              });
            }
            return SmartRefresher(
              controller: model.refreshController,
              onLoading: () {
                model.setRefresh();
                model.getData(loadMore: true);
              },
              onRefresh: () {
                model.setRefresh();
                model.getData(isRefresh: true);
              },
              footer: CustomFooterSmartRefresh(),
              enablePullDown: true,
              enablePullUp: true,
              header: WaterDropHeader(
                complete: Icon(Icons.check, color: Colors.green),
              ),
              child: ListView.separated(
                scrollDirection: Axis.vertical,
                separatorBuilder: (context, index) {
                  return Divider();
                },
                itemCount: model.list.length,
                itemBuilder: (_, index) {
                  final d = model.list[index];
                  return _Cell(
                    data: d,
                    onTap: () => {
                      NavigatorUtils.push(context,
                          custom: true,
                          withNavBar: true,
                          screen: PlayerPage(
                            d.title,
                            d.contentSerieId,
                            contentId: d.id,
                          ))
                    },
                  );
                },
              ),
            );
          },
        ));
  }
}

class _Cell extends StatelessWidget {
  final ItemBookmark data;
  final Function onTap;

  const _Cell({@required this.data, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Row(
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(100),
            height: ScreenUtil().setHeight(80),
            margin: EdgeInsets.only(left: 2, right: 2, top: 4),
            decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(ScreenUtil().setWidth(7)),
              image: DecorationImage(
                fit: BoxFit.contain,
                image: ImageUtils.getImageProvider(data.imageUrl),
              ),
            ),
          ),
          Gaps.hGap12,
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  data.title,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  //textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(16),
                  ),
                ),
                Gaps.vGap8,
                Row(
                  children: <Widget>[
                    Text(
                      data.producer,
                      maxLines: 1,
                      //textAlign: TextAlign.center,
                      style: TextStyle(
                        fontStyle: FontStyle.normal,
                        fontSize: ScreenUtil().setSp(13),
                      ),
                    ),
                    Expanded(
                      child: SizedBox.shrink(),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 10, left: 10),
                      child: InkWell(
                        onTap: () {
                          Provider.of<BookmarkProvider>(context, listen: false)
                              .removeBookmark(data);
                        },
                        child: Icon(Icons.close),
                      ),
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
