import 'package:clipo/entity/bookmark_response.dart';
import 'package:clipo/provider/base_view_state.dart';
import 'package:clipo/repository/Repository.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class BookmarkProvider extends BaseViewState {
  Repository _commonRepository;
  List<ItemBookmark> _list = [];

  List<ItemBookmark> get list => _list;

  int _currentPage = 1;
  int pageSize = 20;
  int totalPage = 0;

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  RefreshController get refreshController => _refreshController;

  void initData() {
    _commonRepository = Repository();
    setRefresh();
    getData(isRefresh: false);
  }

  void getData({bool isRefresh = false, loadMore = false}) {
    if (isRefresh) {
      _currentPage = 1;
    }
    if (loadMore && _currentPage > totalPage) {
      _refreshController.loadNoData();
      return;
    }
    _commonRepository.getBookmarks(_currentPage, pageSize).then((result) {
      if (result.items != null && result.items.isNotEmpty) {
        if (isRefresh) {
          _list.clear();
          _refreshController.loadComplete();
          _refreshController.refreshCompleted();
        }
        if (loadMore) {
          _refreshController.loadComplete();
        }
        totalPage = result.totalPage;
        _list.addAll(result.items);
        _currentPage++;
        setSuccess();
      } else {
        setEmpty();
      }
    }).catchError((e) {
      setError(e);
    });
  }

  void removeBookmark(ItemBookmark item) {
    _commonRepository.removeBookmark(item.id.toString()).then((result) {
      _list.removeAt(_list.indexOf(item));
      if (_list.isEmpty) {
        setEmpty();
      } else{
        setSuccess();
      }
    }).catchError((e, s) {
    });
  }
}
