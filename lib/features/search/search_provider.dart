import 'package:clipo/entity/content_serie_part.dart';
import 'package:clipo/entity/search_response.dart';
import 'package:clipo/provider/base_view_state.dart';
import 'package:clipo/repository/Repository.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SearchProvider extends BaseViewState {
  Repository _commonRepository;
  List<ItemSearch> _list = [];

  SearchProvider() {
    print("fahrad init");
    initData();
  }

  List<ItemSearch> get list => _list;

  int _currentPage = 1;
  int pageSize = 20;
  int totalPage = 0;
  String query = "";
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  RefreshController get refreshController => _refreshController;

  void initData() {
    _commonRepository = Repository();
  }

  void nextPage() {
    _currentPage++;
  }

  void setQuery(String query) {
    this.query = query;
  }

  void getData({bool isRefresh = false, bool loadMore = false}) {
    if (isRefresh) {
      _currentPage = 1;
    }
    if (loadMore && (_currentPage > totalPage)) {
      _refreshController.loadNoData();
      return;
    }
    _commonRepository.search(query, _currentPage, pageSize).then((result) {
      if (result != null) {
        if (isRefresh) {
          _list.clear();
          _refreshController.loadComplete();
          _refreshController.refreshCompleted();
        }
        if (loadMore) {
          _refreshController.loadComplete();
        }
        if (result.contents.items.isEmpty) {
          setEmpty();
        } else {
          _list.addAll(result.contents.items);
          totalPage = result.contents.totalPage;
          nextPage();
          setSuccess();
        }
      } else {
        setEmpty();
      }
    }).catchError((e) {
      setError(e);
    });
  }
}
