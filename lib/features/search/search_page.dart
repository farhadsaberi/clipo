import 'package:clipo/entity/search_response.dart';
import 'package:clipo/features/player/player_page.dart';
import 'package:clipo/features/search/search_provider.dart';
import 'package:clipo/generated/l10n.dart';
import 'package:clipo/provider/provider_widget.dart';
import 'package:clipo/provider/view_state_widget.dart';
import 'package:clipo/res/colors.dart';
import 'package:clipo/res/gaps.dart';
import 'package:clipo/utils/image_utils.dart';
import 'package:clipo/utils/navigator.dart';
import 'package:clipo/utils/toast.dart';
import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:clipo/utils/widgets/custom_footer_smart_refresh.dart';
import 'package:clipo/utils/widgets/search_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SearchPage extends StatelessWidget {
  String _keyword = "";
  final FocusNode _nodeText1 = FocusNode();

  @override
  Widget build(BuildContext context) {
    _nodeText1.unfocus();
    return CustomProviderWidget<SearchProvider>(
      provider: SearchProvider(),
      onProviderReady: (provider) {},
      builder: (__, model, _) {
        return Scaffold(
          resizeToAvoidBottomPadding: false,
          backgroundColor: ColorRes.withe,
          appBar: CustomAppBar(
            isBack: false,
            centerTitle: S.of(context).search,
          ),
          body: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                color: Color(0xFFEEEEEE),
                height: ScreenUtil().setHeight(50),
                child: SearchBar(
                  focusNode: _nodeText1,
                  hintText: S.of(context).search + " ...",
                  onPressed: (text) {
                    if (text.isEmpty) {
                      Toast.show('Search keyword cannot be empty！');
                      return;
                    }
                    _keyword = text;
                    model.setQuery(_keyword);
                    model.setRefresh();
                    model.getData(isRefresh: true);
                  },
                ),
              ),
              Expanded(
                child: Consumer<SearchProvider>(builder: (_, model, __) {
                  if (model.isLoading) {
                    return ViewStateLoadingWidget();
                  } else if (model.isError && model == null) {
                    return ViewStateErrorWidget(
                        error: model.viewStateError,
                        onPressed: () {
                          model.getData(isRefresh: true);
                        });
                  } else if (model.isEmpty) {
                    return ViewStateEmptyWidget(onPressed: () {
                      model.getData(isRefresh: true);
                    });
                  }
                  return _SearchList();
                }),
              ),
            ],
          ),
        );
      },
    );
  }
}

class _SearchList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<SearchProvider>(context);
    return SmartRefresher(
      controller: provider.refreshController,
      onLoading: () {
        provider.getData(loadMore: true);
      },
      onRefresh: () {
        provider.getData(isRefresh: true);
      },
      enablePullDown: true,
      enablePullUp: true,
      header: WaterDropHeader(
        complete: Icon(Icons.check, color: Colors.green),
      ),
      child: ListView.separated(
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(15)),
        scrollDirection: Axis.vertical,
        shrinkWrap: false,
        separatorBuilder: (_, index) {
          return Divider();
        },
        itemCount: provider.list.length,
        itemBuilder: (_, index) {
          final data = provider.list[index];
          return _Cell(
              data: data,
              onTap: () {
                NavigatorUtils.push(context,
                    custom: true,
                    withNavBar: true,
                    screen: PlayerPage(
                      data.title,
                      data.contentSerieId,
                      contentId: data.id,
                    ));
              });
        },
      ),
    );
  }
}

class _Cell extends StatelessWidget {
  final ItemSearch data;
  final Function onTap;

  const _Cell({@required this.data, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Row(
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(100),
            height: ScreenUtil().setHeight(56),
            margin: EdgeInsets.only(left: 4, right: 4, top: 4),
            decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(ScreenUtil().setWidth(7)),
              image: DecorationImage(
                fit: BoxFit.contain,
                image: ImageUtils.getImageProvider(data.imageUrl),
              ),
            ),
          ),
          Gaps.hGap12,
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  data.title,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  //textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(15),
                  ),
                ),
                Gaps.vGap4,
                Row(
                  children: <Widget>[
                    Text(
                      data.producer,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      //textAlign: TextAlign.center,
                      style: TextStyle(
                        fontStyle: FontStyle.normal,
                        fontSize: ScreenUtil().setSp(13),
                      ),
                    ),
                    Expanded(
                      child: SizedBox.shrink(),
                    ),
                    Visibility(
                      visible: !data.isPurchased,
                      child: Container(
                        margin: EdgeInsets.only(right: 10, left: 10),
                        child: Icon(
                          Icons.lock,
                          color: ColorRes.purpleColor,
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
