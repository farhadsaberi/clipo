import 'package:clipo/entity/content_serie.dart';
import 'package:clipo/features/contentserie/parts/content_serie_parts_page.dart';
import 'package:clipo/provider/provider_widget.dart';
import 'package:clipo/provider/view_state_widget.dart';
import 'package:clipo/res/gaps.dart';
import 'package:clipo/utils/image_utils.dart';
import 'package:clipo/utils/navigator.dart';
import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:clipo/utils/widgets/custom_footer_smart_refresh.dart';
import 'package:clipo/utils/widgets/custom_scroll_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'content_serie_provider.dart';

class ContentSeriePage extends StatelessWidget {
  final String title;
  final String _key;
  final List<ItemContentSerie> outPutData;

  ContentSeriePage(this.title, this._key, {this.outPutData});

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [
      _BodyWidget(),
    ];
    return Scaffold(
        appBar: CustomAppBar(
          isBack: true,
          title: title,
        ),
        body: CustomProviderWidget<ConentSerieProvider>(
          provider: ConentSerieProvider(),
          onProviderReady: (provider) {
            if (outPutData != null) {
              provider.setOutPutData(outPutData);
            } else {
              provider.initData(_key);
            }
          },
          builder: (__, model, _) {
            if (model.isLoading) {
              return ViewStateLoadingWidget();
            } else if (model.isError && model.list.isEmpty) {
              return ViewStateErrorWidget(
                  error: model.viewStateError,
                  onPressed: () {
                    model.setRefresh();
                    model.getData(isRefresh: true);
                  });
            } else if (model.isEmpty) {
              return ViewStateEmptyWidget(onPressed: () {
                model.setRefresh();
                model.getData(isRefresh: true);
              });
            }
            return SmartRefresher(
              controller: model.refreshController,
              onLoading: () {
                model.getData(loadMore: true);
              },
              onRefresh: () {
                model.getData(isRefresh: true);
              },
              footer: CustomFooterSmartRefresh(),
              enablePullDown: outPutData == null,
              enablePullUp: outPutData == null,
              header: WaterDropHeader(
                complete: Icon(Icons.check, color: Colors.green),
              ),
              child: ListView.separated(
                scrollDirection: Axis.vertical,
                separatorBuilder: (context, index) {
                  return Divider();
                },
                itemCount: model.list.length,
                itemBuilder: (_, index) {
                  final d = model.list[index];
                  return _Cell(
                    data: d,
                    onTap: () => {
                      NavigatorUtils.push(context,
                          custom: true,
                          withNavBar: true,
                          screen: ContentSeriePartsPage(
                            d.title,
                            d.id,
                          ))
                    },
                  );
                },
              ),
            );
          },
        ));
  }
}

class _BodyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Consumer<ConentSerieProvider>(builder: (_, itemModel, __) {
        if (itemModel.isLoading) {
          return ViewStateLoadingWidget();
        } else if (itemModel.isSuccess) {
          return ListView.separated(
            padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(5)),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            separatorBuilder: (context, index) {
              return Divider();
            },
            itemCount: itemModel.list.length,
            itemBuilder: (_, index) {
              final d = itemModel.list[index];
              return _Cell(
                data: d,
                onTap: () => {
                  NavigatorUtils.push(context,
                      custom: true,
                      withNavBar: true,
                      screen: ContentSeriePartsPage(d.title, d.id))
                },
              );
            },
          );
        } else {
          return Container();
        }
      }),
    );
  }
}

class _Cell extends StatelessWidget {
  final ItemContentSerie data;
  final Function onTap;

  const _Cell({@required this.data, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Row(
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(40),
            height: ScreenUtil().setHeight(40),
            margin: EdgeInsets.only(left: 2, right: 2, top: 4),
            decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(ScreenUtil().setWidth(7)),
              image: DecorationImage(
                fit: BoxFit.contain,
                image: ImageUtils.getImageProvider(data.imageUrl),
              ),
            ),
          ),
          Gaps.hGap12,
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                data.title,
                maxLines: 2,
                //textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(16),
                ),
              ),
              Gaps.vGap4,
              Text(
                data.producer,
                maxLines: 2,
                //textAlign: TextAlign.center,
                style: TextStyle(
                  fontStyle: FontStyle.normal,
                  fontSize: ScreenUtil().setSp(11),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
