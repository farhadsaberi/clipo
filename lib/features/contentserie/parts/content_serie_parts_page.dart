import 'package:clipo/entity/content_serie_part.dart';
import 'package:clipo/features/player/player_page.dart';
import 'package:clipo/generated/l10n.dart';
import 'package:clipo/provider/provider_widget.dart';
import 'package:clipo/provider/view_state_widget.dart';
import 'package:clipo/res/colors.dart';
import 'package:clipo/res/gaps.dart';
import 'package:clipo/utils/image_utils.dart';
import 'package:clipo/utils/navigator.dart';
import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:clipo/utils/widgets/custom_footer_smart_refresh.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'content_serie_parts_provider.dart';

class ContentSeriePartsPage extends StatelessWidget {
  final String title;
  final int contentSerieId;
  final List<ItemPart> outPutData;

  ContentSeriePartsPage(this.title, this.contentSerieId, {this.outPutData});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorRes.bg_color,
        appBar: CustomAppBar(
          isBack: true,
          title: title,
        ),
        body: CustomProviderWidget<ConentSeriePartsProvider>(
          provider: ConentSeriePartsProvider(),
          onProviderReady: (provider) {
            if (outPutData != null) {
              provider.setOutPutData(outPutData);
            } else {
              provider.initData(contentSerieId);
            }
          },
          builder: (__, model, _) {
            if (model.isLoading) {
              return ViewStateLoadingWidget();
            } else if (model.isError && model.list.isEmpty) {
              return ViewStateErrorWidget(
                  error: model.viewStateError,
                  onPressed: () {
                    model.setRefresh();
                    model.getData();
                  });
            } else if (model.isEmpty) {
              return ViewStateEmptyWidget(onPressed: () {
                model.setRefresh();
                model.getData();
              });
            }
            return SmartRefresher(
              controller: model.refreshController,
              onLoading: () {
                model.getData(loadMore: true);
              },
              onRefresh: () {
                model.getData(isRefresh: true);
              },
              footer: CustomFooterSmartRefresh(),
              enablePullDown: outPutData == null,
              enablePullUp: outPutData == null,
              header: WaterDropHeader(
                complete: Icon(Icons.check, color: Colors.green),
              ),
              child: ListView.separated(
                scrollDirection: Axis.vertical,
                separatorBuilder: (context, index) {
                  return Divider();
                },
                itemCount: model.list.length,
                itemBuilder: (_, index) {
                  final data = model.list[index];
                  if (index == 0) {
                    return (_InformationContent(title, model.totalVideo));
                  }
                  return _Cell(
                    data: data,
                    onTap: () => {
                      NavigatorUtils.push(context,
                          custom: true,
                          withNavBar: true,
                          screen: PlayerPage(data.title,contentSerieId,contentId: data.id,))
                    },
                  );
                },
              ),
            );
          },
        ));
  }
}

class _InformationContent extends StatelessWidget {
  final String title;
  final int totalVideo;

  _InformationContent(this.title, this.totalVideo);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20),
      color: ColorRes.withe,
      height: ScreenUtil().setHeight(97),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            title,
            maxLines: 1,
            //textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: ScreenUtil().setSp(16),
            ),
          ),
          Gaps.vGap8,
          Text(
            totalVideo.toString() + " " + S.of(context).videos,
            maxLines: 1,
            //textAlign: TextAlign.center,
            style: TextStyle(
              fontStyle: FontStyle.normal,
              fontSize: ScreenUtil().setSp(13),
              color: ColorRes.text_gray,
            ),
          ),
        ],
      ),
    );
  }
}

class _Cell extends StatelessWidget {
  final ItemPart data;
  final Function onTap;

  const _Cell({@required this.data, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Row(
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(100),
            height: ScreenUtil().setHeight(56),
            margin: EdgeInsets.only(left: 4, right: 4, top: 4),
            decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(ScreenUtil().setWidth(7)),
              image: DecorationImage(
                fit: BoxFit.contain,
                image: ImageUtils.getImageProvider(data.imageUrl),
              ),
            ),
          ),
          Gaps.hGap12,
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  data.title,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  //textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(15),
                  ),
                ),
                Gaps.vGap4,
                Row(
                  children: <Widget>[
                    Text(
                      data.categoryTitle == null
                          ? data.key == null ? "" : data.key
                          : data.categoryTitle,
                      maxLines: 2,
                      //textAlign: TextAlign.center,
                      style: TextStyle(
                        fontStyle: FontStyle.normal,
                        fontSize: ScreenUtil().setSp(13),
                      ),
                    ),
                    Expanded(
                      child: SizedBox.shrink(),
                    ),
                    Visibility(
                      visible:data.isFree?false:data.isPurchased?false:true,
                      child: Container(
                        margin: EdgeInsets.only(right: 10, left: 10),
                        child: Icon(
                          Icons.lock,
                          color: ColorRes.purpleColor,
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
