import 'package:clipo/entity/content_serie.dart';
import 'package:clipo/provider/base_view_state.dart';
import 'package:clipo/repository/Repository.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ConentSerieProvider extends BaseViewState {
  Repository _commonRepository;
  List<ItemContentSerie> _list = [];

  List<ItemContentSerie> get list => _list;

  int _currentPage = 1;
  int pageSize = 20;
  int totalPage = 0;

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  RefreshController get refreshController => _refreshController;

  String key ;

  void initData(String key) {
    this.key = key;
    _commonRepository = Repository();
    setRefresh();
    getData(isRefresh: false);
  }

  void setOutPutData(List<ItemContentSerie> data) {
    _list.clear();
    _list.addAll(data);
    totalPage = 1;
    setSuccess();
  }


  void getData({bool isRefresh = false, loadMore = false}) {
    if (isRefresh) {
      _currentPage = 1;
    }
    if (loadMore && _currentPage > totalPage) {
      _refreshController.loadNoData();
      return;
    }
    _commonRepository.getContentSerie(key,_currentPage,pageSize).then((result) {
      if (result.items != null &&
          result.items.isNotEmpty) {
        if (isRefresh) {
          _list.clear();
          _refreshController.loadComplete();
          _refreshController.refreshCompleted();
        }
        if (loadMore) {
          _refreshController.loadComplete();
        }
        totalPage = result.totalPage;
        _list.addAll(result.items);
        setSuccess();
      } else {
        setEmpty();
      }
    }).catchError((e) {
      setError(e);
    });
  }
}
