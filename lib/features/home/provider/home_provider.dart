import 'package:clipo/entity/home_response.dart';
import 'package:clipo/provider/base_view_state.dart';
import 'package:clipo/repository/Repository.dart';

class HomeProvider extends BaseViewState {
  Repository _commonRepository;
  List<ContentSerieSection> _list = [];

  List<ContentSerieSection> get homeList => _list;
  List<BannerSection> _listBanner = [];

  List<BannerSection> get listBanner => _listBanner;

  void initData() {
    _commonRepository = Repository();
    getData(isRefresh: false);
    setRefresh();
  }

  void getData({bool isRefresh}) {
    _commonRepository.getHomeList().then((result) {
      if (result.contentSerieSections != null &&
          result.contentSerieSections.isNotEmpty) {
        _list.clear();
        _list.addAll(result.contentSerieSections);
        _listBanner.clear();
        _listBanner.addAll(result.bannerSections);
        setSuccess();
      } else {
        setEmpty();
      }
    }).catchError((e) {
      setError(e);
    });
  }
}
