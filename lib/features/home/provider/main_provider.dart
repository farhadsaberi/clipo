import 'package:clipo/config/constant.dart';
import 'package:clipo/local/local_storage.dart';
import 'package:flutter/material.dart';

class MainProvider extends ChangeNotifier {
  bool _isGuest = true;

  bool get isGuest => _isGuest;

  void updateGuest(bool update) {
    this._isGuest = update;
    notifyListeners();
  }

  MainProvider() {
    checkIsLogin();
  }

  void checkIsLogin() async {
    var user = await LocalStorage.get(Constant.TOKEN_KEY);
    if (user != null) {
      updateGuest(false);
    } else {
      updateGuest(true);
    }
  }
}
