import 'package:clipo/features/category/category_page.dart';
import 'package:clipo/features/home/provider/main_provider.dart';
import 'package:clipo/features/language/language_provider.dart';
import 'package:clipo/features/profile/profile_page.dart';
import 'package:clipo/features/search/search_page.dart';
import 'package:clipo/generated/l10n.dart';
import 'package:clipo/res/colors.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:provider/provider.dart';

import 'home_page.dart';

class MainPage extends StatefulWidget {
  MainPage({Key key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  PersistentTabController _controller;

  List<Widget> _pageList;
  final PageController _pageController = PageController();

  @override
  void initState() {
    super.initState();
    _controller = PersistentTabController(initialIndex: 0);
    initData();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void initData() {
    _pageList = [
      HomePage(),
      CategoryPage(),
      SearchPage(),
      ProfilePage(),
    ];
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: Icon(Icons.home),
        title: S.of(context).home,
        activeColor: ColorRes.app_main,
        inactiveColor: ColorRes.unselected_item_color,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.list),
        title: S.of(context).category,
        activeColor: ColorRes.app_main,
        inactiveColor: ColorRes.unselected_item_color,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.search),
        title: S.of(context).search,
        activeColor: ColorRes.app_main,
        inactiveColor: ColorRes.unselected_item_color,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.person),
        title: S.of(context).profile,
        activeColor: ColorRes.app_main,
        inactiveColor: ColorRes.unselected_item_color,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(builder: (context, orientation) {
      return Consumer2<MainProvider,LanguageProvider>(builder: (context, model,lang, child) {
        return PersistentTabView(
          controller: _controller,
          screens: _pageList,
          items: _navBarsItems(),
          confineInSafeArea: true,
          backgroundColor: Colors.white,
          handleAndroidBackButtonPress: true,
          resizeToAvoidBottomInset: true,
          stateManagement: true,
          hideNavigationBarWhenKeyboardShows: true,
          popAllScreensOnTapOfSelectedTab: true,
          hideNavigationBar: orientation != Orientation.portrait,
          navBarStyle:
          NavBarStyle.simple, // Choose the nav bar style with this property
        );

      });
    });
  }
}
