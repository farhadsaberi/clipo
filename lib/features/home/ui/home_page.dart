import 'package:clipo/entity/home_response.dart';
import 'package:clipo/features/contentserie/content_serie_page.dart';
import 'package:clipo/features/contentserie/parts/content_serie_parts_page.dart';
import 'package:clipo/features/home/provider/home_provider.dart';
import 'package:clipo/generated/l10n.dart';
import 'package:clipo/provider/provider_widget.dart';
import 'package:clipo/provider/view_state_widget.dart';
import 'package:clipo/res/colors.dart';
import 'package:clipo/res/gaps.dart';
import 'package:clipo/utils/navigator.dart';
import 'package:clipo/utils/utils.dart';
import 'package:clipo/utils/widgets/banner_image.dart';
import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:clipo/utils/widgets/custom_scroll_view.dart';
import 'package:clipo/utils/widgets/item_content_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Widget> children = [
      BannerWidget(),
      BodyWidget(),
    ];
    return Scaffold(
        appBar: CustomAppBar(
          isBack: false,
          centerTitle: S.of(context).app_name,
        ),
        body: CustomProviderWidget<HomeProvider>(
          provider: HomeProvider(),
          onProviderReady: (provider) => provider.initData(),
          builder: (__, model, _) {
            if (model.isLoading) {
              return ViewStateLoadingWidget();
            } else if (model.isError && model.homeList.isEmpty) {
              return ViewStateErrorWidget(
                  error: model.viewStateError, onPressed: model.initData);
            } else if (model.isEmpty) {
              return ViewStateEmptyWidget(onPressed: model.initData);
            }
            return CustomScrollViewWidget(
              children: children,
            );
          },
        ));
  }
}

class BodyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).scaffoldBackgroundColor,
      ),
      child: Consumer<HomeProvider>(builder: (_, homeModel, __) {
        if (homeModel.isLoading) {
          return ViewStateLoadingWidget();
        } else if (homeModel.isSuccess) {
          var homeList = homeModel?.homeList ?? [];
          return ListView.builder(
            padding: EdgeInsets.only(
              top: 8.0,
            ),
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: homeList.length,
            itemBuilder: (context, indexTop) {
              return Column(
                children: <Widget>[
                  _FrontTitle(homeList[indexTop]),
                  Gaps.vGap4,
                  _FrontList(homeList[indexTop]),
                  Gaps.vGap8
                ],
              );
            },
          );
        } else {
          return Container();
        }
      }),
    );
  }
}

class _FrontTitle extends StatelessWidget {
  final ContentSerieSection contentSerieSection;

  const _FrontTitle(this.contentSerieSection);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(15)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            contentSerieSection.title,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(18), fontWeight: FontWeight.bold),
          ),
          GestureDetector(
              onTap: () => {
                    NavigatorUtils.push(context,
                        custom: true,
                        withNavBar: true,
                        screen: ContentSeriePage(
                            contentSerieSection.title, contentSerieSection.key))
                  },
              child: Container(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, top: 5, bottom: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(100.0)),
                      border:
                          Border.all(color: ColorRes.purpleColor, width: 1.5)),
                  child: Text(
                    S.of(context).more,
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(12),
                        color: ColorRes.purpleColor,
                        fontWeight: FontWeight.bold),
                  )))
        ],
      ),
    );
  }
}

class _FrontList extends StatelessWidget {
  final ContentSerieSection contentSerieSection;

  _FrontList(this.contentSerieSection);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(130),
      child: ListView.separated(
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(15)),
        scrollDirection: Axis.horizontal,
//        physics: PageScrollPhysics(),
        shrinkWrap: true,
        separatorBuilder: (_, index) =>
            SizedBox(width: ScreenUtil().setWidth(15)),
        itemCount: contentSerieSection.pagedContentSeries.items.length + 1,
        itemBuilder: (_, index) {
          if (index == contentSerieSection.pagedContentSeries.items.length)
            return _MoreCell(
                contentSerieSection.title, contentSerieSection.key);
          final d = contentSerieSection.pagedContentSeries.items[index];
          return ItemContentPlayer(data: d,onTap: (){
            NavigatorUtils.push(context,
                custom: true,
                withNavBar: true,
                screen: ContentSeriePartsPage(d.title, d.id));

          });
        },
      ),
    );
  }
}

class _MoreCell extends StatelessWidget {
  final String _key;
  final String title;

  _MoreCell(this.title, this._key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(
          8,
        ),
      ),
      child: InkWell(
        onTap: () {
          NavigatorUtils.push(context,
              custom: true,
              withNavBar: true,
              screen: ContentSeriePage(title, _key));
        },
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
                ),
                width: ScreenUtil().setWidth(120),
                height: ScreenUtil().setHeight(120),
                child: Center(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          S.of(context).more,
                          style: TextStyle(fontSize: ScreenUtil().setSp(18)),
                        ),
                        Icon(Icons.arrow_forward,
                            size: ScreenUtil().setWidth(18))
                      ]),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class BannerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(180),
      decoration: BoxDecoration(
        color: Theme.of(context).scaffoldBackgroundColor,
      ),
      child: Consumer<HomeProvider>(builder: (_, homeModel, __) {
        if (homeModel.isLoading) {
          return ViewStateLoadingWidget();
        } else {
          var banners = homeModel?.listBanner ?? [];
          if(banners.length==0){
            return Container();
          }
          return Swiper(
            loop: true,
            autoplay: true,
            autoplayDelay: 5000,
            pagination: SwiperPagination(),
            itemCount: banners.length,
            itemBuilder: (ctx, index) {
              return InkWell(
                  onTap: () {
                    var banner = banners[index];
                    if (banner.banners[0].clickUrl.isNotEmpty) {
                      Utils.openLink(banner.banners[0].clickUrl);
                    }
//                    Navigator.of(context).pushNamed(RouteName.articleDetail,
//                        arguments: Article()
//                          ..id = banner.id
//                          ..title = banner.title
//                          ..link = banner.url
//                          ..collect = false);
                  },
                  child: BannerImage(banners[index].banners[0].imageUrl));
            },
          );
        }
      }),
    );
  }
}
