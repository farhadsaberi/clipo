import 'dart:async';

import 'package:clipo/features/home/provider/main_provider.dart';
import 'package:clipo/res/colors.dart';
import 'package:clipo/routers/router_manger.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    _initSplash();
  }

  @override
  Widget build(BuildContext context) {
   ScreenUtil.init(context, width: 375, height: 667);
    return SafeArea(
      child: Scaffold(
        body: Container(
        ),
      ),
    );
  }

  void _initSplash() {
    final provider = Provider.of<MainProvider>(context,listen: false);
    Future.delayed(Duration(milliseconds: 500)).then((value) {
      if (provider.isGuest) {
        Navigator.of(context).pushReplacementNamed(RouteName.register);
      }else{
        Navigator.of(context).pushReplacementNamed(RouteName.home);
      }
    });
  }
}
