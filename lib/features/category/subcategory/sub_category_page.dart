import 'package:clipo/entity/content_serie_part.dart';
import 'package:clipo/entity/sub_category_response.dart';
import 'package:clipo/features/contentserie/parts/content_serie_parts_page.dart';
import 'package:clipo/provider/provider_widget.dart';
import 'package:clipo/provider/view_state_widget.dart';
import 'package:clipo/res/colors.dart';
import 'package:clipo/utils/navigator.dart';
import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:clipo/utils/widgets/custom_footer_smart_refresh.dart';
import 'package:clipo/utils/widgets/item_content_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'sub_category_provider.dart';

class SubCategoryPage extends StatelessWidget {
  final String title;
  final int contentId;

  SubCategoryPage(this.title, this.contentId);

  List<ItemPart> getItemPartList(List<ContentSubCategory> contentList) {
    List<ItemPart> list = [];
    contentList.forEach((element) {
      list.add(ItemPart()
        ..id = element.id
        ..key = element.key
        ..title = element.title
        ..producer = element.producer
        ..imageUrl = element.imageUrl
        ..categoryTitle = element.categoryTitle
        ..isFree = element.isFree
        ..isPurchased = element.isPurchased);
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorRes.bg_color,
        appBar: CustomAppBar(
          isBack: true,
          title: title,
        ),
        body: CustomProviderWidget<SubCategoryProvider>(
          provider: SubCategoryProvider(),
          onProviderReady: (provider) {
            provider.initData(contentId);
          },
          builder: (__, model, _) {
            if (model.isLoading) {
              return ViewStateLoadingWidget();
            } else if (model.isError && model.list.isEmpty) {
              return ViewStateErrorWidget(
                  error: model.viewStateError,
                  onPressed: () {
                    model.setRefresh();
                    model.getData();
                  });
            } else if (model.isEmpty) {
              return ViewStateEmptyWidget(onPressed: () {
                model.setRefresh();
                model.getData();
              });
            }
            return SmartRefresher(
                controller: model.refreshController,
                onLoading: () {
                  model.getData(loadMore: true);
                },
                footer: CustomFooterSmartRefresh(),
                onRefresh: () {
                  model.getData(isRefresh: true);
                },
                enablePullDown: true,
                enablePullUp: true,
                header: WaterDropHeader(
                  complete: Icon(Icons.check, color: Colors.green),
                ),
                child: GridView.count(
                    crossAxisCount: 2,
                    shrinkWrap: true,
                    childAspectRatio: ScreenUtil.screenWidth /
                        (ScreenUtil.screenHeight /2.7),
                    children: new List<Widget>.generate(
                      model.list.length,
                  (index) {
                    return _categoryButton(model.list[index], context);
                  },
                )));
          },
        ));
  }

  Widget _buildListView(List<ItemSubCategory> list, BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      child: SingleChildScrollView(
        child: Wrap(
          direction: Axis.horizontal,
          children: _getCategoriesList(list, context),
        ),
      ),
    );
  }

  List<Widget> _getCategoriesList(
      List<ItemSubCategory> list, BuildContext context) {
    if (list == null || list.isEmpty) return [];
    List<Widget> categoriesButton = [];
    for (int i = 0; i < list.length; i++) {
      categoriesButton.add(_categoryButton(list[i], context));
    }
    return categoriesButton;
  }

  Widget _categoryButton(ItemSubCategory category, BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 5, left: 5, bottom: 5),
      child: FittedBox(
        child: ItemContentPlayer(
            data: category,
            onTap: () {
              NavigatorUtils.push(context,
                  custom: true,
                  withNavBar: true,
                  screen: ContentSeriePartsPage(
                    category.title,
                    category.id,
                    outPutData: getItemPartList(category.contents),
                  ));
            }),
      ),
    );
  }
}
