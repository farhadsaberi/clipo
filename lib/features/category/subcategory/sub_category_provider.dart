import 'package:clipo/entity/content_serie_part.dart';
import 'package:clipo/entity/sub_category_response.dart';
import 'package:clipo/provider/base_view_state.dart';
import 'package:clipo/repository/Repository.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SubCategoryProvider extends BaseViewState {
  Repository _commonRepository;
  List<ItemSubCategory> _list = [];

  List<ItemSubCategory> get list => _list;

  int _currentPage = 1;
  int pageSize = 20;
  int totalPage = 0;
  int _totalVideo = 0;

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  RefreshController get refreshController => _refreshController;

  int get totalVideo => _totalVideo;

  int contentId = 0;

  void initData(int contentId) {
    this.contentId = contentId;
    _commonRepository = Repository();
    getData(isRefresh: false);
    setRefresh();
  }

  void nextPage() {
    _currentPage++;
  }

  void getData({bool isRefresh = false, bool loadMore = false}) {
    if (isRefresh) {
      _currentPage = 1;
    }
    if (loadMore && _currentPage > totalPage) {
      _refreshController.loadNoData();
      return;
    }
    _commonRepository
        .getSubCategory(contentId.toString(), _currentPage, pageSize)
        .then((result) {
      if (result.items != null && result.items.isNotEmpty) {
        if (isRefresh) {
          _list.clear();
          _refreshController.loadComplete();
          _refreshController.refreshCompleted();
        }
        if (loadMore) {
          _refreshController.loadComplete();
        }
        _totalVideo = result.count;
        _list.addAll(result.items);
        totalPage = result.totalPage;
        nextPage();
        setSuccess();
      } else {
        setEmpty();
      }
    }).catchError((e) {
      setError(e);
    });
  }
}
