import 'package:clipo/entity/category_response.dart';
import 'package:clipo/provider/base_view_state.dart';
import 'package:clipo/repository/Repository.dart';

class CategoryProvider extends BaseViewState {
  Repository _commonRepository;
  List<CategoryResponse> _list = [];

  List<CategoryResponse> get list => _list;


  void initData() {
    _commonRepository = Repository();
    setRefresh();
    getData();
  }

  void getData() {
    _commonRepository.getCategory().then((result) {
      if (result != null &&
          result.isNotEmpty) {
        _list.addAll(result);
        setSuccess();
      } else {
        setEmpty();
      }
    }).catchError((e) {
      setError(e);
    });
  }
}
