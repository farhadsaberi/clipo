import 'package:clipo/entity/category_response.dart';
import 'package:clipo/features/category/category_provider.dart';
import 'package:clipo/features/category/subcategory/sub_category_page.dart';
import 'package:clipo/features/contentserie/parts/content_serie_parts_page.dart';
import 'package:clipo/generated/l10n.dart';
import 'package:clipo/provider/provider_widget.dart';
import 'package:clipo/provider/view_state_widget.dart';
import 'package:clipo/res/colors.dart';
import 'package:clipo/utils/image_utils.dart';
import 'package:clipo/utils/navigator.dart';
import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CategoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorRes.bg_color,
        appBar: CustomAppBar(
          isBack: false,
          centerTitle: S.of(context).category,
        ),
        body: CustomProviderWidget<CategoryProvider>(
          provider: CategoryProvider(),
          onProviderReady: (provider) =>provider.initData(),
          builder: (__, model, _) {
            if (model.isLoading) {
              return ViewStateLoadingWidget();
            } else if (model.isError && model.list.isEmpty) {
              return ViewStateErrorWidget(
                  error: model.viewStateError,
                  onPressed: () {
                    model.setRefresh();
                    model.getData();
                  });
            } else if (model.isEmpty) {
              return ViewStateEmptyWidget(onPressed: () {
                model.setRefresh();
                model.getData();
              });
            }
            return Container(
              margin: EdgeInsets.only(top: 20),
              child: ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(5)),
                scrollDirection: Axis.vertical,
                itemCount: model.list.length,
                itemBuilder: (_, index) {
                  final d = model.list[index];
                  return _Cell(
                    data: d,
                    onTap: () => {
                      NavigatorUtils.push(context,
                          custom: true,
                          withNavBar: true,
                          screen: SubCategoryPage(d.title, d.id))
                    },
                  );
                },
              ),
            );
          },
        ));
  }
}


class _Cell extends StatelessWidget {
  final CategoryResponse data;
  final Function onTap;

  const _Cell({@required this.data, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: ScreenUtil().setHeight(100),
        child: Card(
          elevation: 3,
          margin: EdgeInsets.only(bottom: 20),
          child: Row(
            children: <Widget>[
              Container(
                width: ScreenUtil().setWidth(90),
                height: ScreenUtil().setHeight(70),
                margin: EdgeInsets.only(left: 18, right: 18, top: 4),
                decoration: BoxDecoration(
                  color: ColorRes.bg_gray_light,
                  borderRadius: BorderRadius.circular(ScreenUtil().setWidth(7)),
                  image: DecorationImage(
                    image: ImageUtils.getImageProvider(data.imageUrl),
                  ),
                ),
              ),
              Text(
                data.title,
                //textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(16),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
