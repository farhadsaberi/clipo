import 'package:clipo/config/constant.dart';
import 'package:clipo/entity/login_response.dart';
import 'package:clipo/local/local_storage.dart';
import 'package:clipo/net/result_data.dart';
import 'package:clipo/provider/base_view_state.dart';
import 'package:clipo/repository/Repository.dart';

class LoginProvider extends BaseViewState {
  Repository repository;

  LoginProvider() {
    repository = Repository();
  }

  void saveToStorage(LoginResponse response,String mobile,{String password} ){
    LocalStorage.save(Constant.MOBILE,mobile);
    LocalStorage.save(Constant.TOKEN_KEY, response.accessToken.token);
    LocalStorage.save(
        Constant.REFRESH_TOKEN_KEY, response.refreshToken.token);
  }

  Future updateNikName(String name) {
    return repository.setNickName(name).then((value) {
      return Future.value(true);
    }).catchError((error) {
      return Future.error(false);
    });
  }

  Future sendComment(String contentId, String comment) {
    return repository.setComment(contentId, comment).then((value) {
      return Future.value(true);
    }).catchError((error) {
      return Future.error(false);
    });
  }

  Future<dynamic> unSubscribe() {
    return repository.unSubscribe().then((value) {
      return Future.value();
    }).catchError((error) {
      return Future.error(error);
    });
  }

  Future changePassword(String msisdn, String oldPassword, String newPassword,
      String confirmPassword) {
    return repository
        .changePassword(msisdn, oldPassword, newPassword, confirmPassword)
        .then((value) {
      return Future.value(true);
    }).catchError((error) {
      return Future.error(false);
    });
  }

  void loginWithPassword(String msisdn, String password,
      {SuccessCallBack successCallBack,
        ErrorCallBack errorCallBack,
        LoadingCallBack loadingCallBack}) {
    loadingCallBack(true);
    repository.loginWithPassword(msisdn, password).then((value) {
      loadingCallBack(false);
      successCallBack(data: value);
    }).catchError((error) {
      loadingCallBack(false);
      errorCallBack(error);
    });
  }

  void loginWithMsinsdnAndPassword(String msisdn, String password,
      {SuccessCallBack successCallBack,
        ErrorCallBack errorCallBack,
        LoadingCallBack loadingCallBack}) {
    loadingCallBack(true);
    repository.loginWithMsinsdnAndPassword(msisdn, password).then((value) {
      loadingCallBack(false);
      successCallBack(data: value);
    }).catchError((error) {
      loadingCallBack(false);
      errorCallBack(error);
    });
  }

  void register(String msisdn,
      {SuccessCallBack successCallBack,
        ErrorCallBack errorCallBack,
        LoadingCallBack loadingCallBack}) {
    loadingCallBack(true);
    repository.register(msisdn).then((value) {
      loadingCallBack(false);
      successCallBack(data: value);
    }).catchError((error) {
      loadingCallBack(false);
      errorCallBack(error);
    });
  }

  void verificationCode(String msisdn, String code,
      {SuccessCallBack successCallBack,
        ErrorCallBack errorCallBack,
        LoadingCallBack loadingCallBack}) {
    loadingCallBack(true);
    repository.verificationCode(msisdn, code).then((value) {
      loadingCallBack(false);
      successCallBack(data: value);
    }).catchError((error) {
      loadingCallBack(false);
      errorCallBack(error);
    });
  }

  void forgotPassword(String msisdn,
      {SuccessCallBack successCallBack,
        ErrorCallBack errorCallBack,
        LoadingCallBack loadingCallBack}) {
    loadingCallBack(true);
    repository.forgotPassword(msisdn).then((value) {
      loadingCallBack(false);
      successCallBack();
    }).catchError((error) {
      loadingCallBack(false);
      errorCallBack(error);
    });
  }
}

typedef SuccessCallBack = Function({dynamic data});
typedef ErrorCallBack = Function(ResultData message);
typedef LoadingCallBack = Function(bool loading);
