import 'package:clipo/config/constant.dart';
import 'package:clipo/entity/country_item.dart';
import 'package:clipo/entity/login_response.dart';
import 'package:clipo/features/home/provider/main_provider.dart';
import 'package:clipo/features/language/language_page.dart';
import 'package:clipo/features/login/ui/login_screen.dart';
import 'package:clipo/features/login/ui/verification_code_screen.dart';
import 'package:clipo/generated/l10n.dart';
import 'package:clipo/res/colors.dart';
import 'package:clipo/res/gaps.dart';
import 'package:clipo/routers/router_manger.dart';
import 'package:clipo/utils/change_notifier_manage.dart';
import 'package:clipo/utils/navigator.dart';
import 'package:clipo/utils/toast.dart';
import 'package:clipo/utils/utils.dart';
import 'package:clipo/utils/widgets/button_progress_indicator.dart';
import 'package:clipo/utils/widgets/country_dropdown.dart';
import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:clipo/utils/widgets/custom_text_field.dart';
import 'package:clipo/utils/widgets/custom_button.dart';
import 'package:clipo/utils/widgets/custom_scroll_view.dart';
import 'package:clipo/utils/widgets/load_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';

import 'login_provider.dart';

class RegisterScreen extends StatefulWidget {
  final bool disableAction;

  RegisterScreen({this.disableAction});

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen>
    with ChangeNotifierMixin<RegisterScreen> {
  final TextEditingController _phoneController = TextEditingController();
  final FocusNode _nodeText1 = FocusNode();
  bool _clickable = false;
  CountryItem _selectedCountry = Utils.countryItems()[0];

  @override
  Map<ChangeNotifier, List<VoidCallback>> changeNotifier() {
    final List<VoidCallback> callbacks = [_verify];
    return {
      _phoneController: callbacks,
    };
  }

  void _verify() {
    final String name = _phoneController.text;
    bool clickable = true;
    if (name.isEmpty || name.length < 8) {
      clickable = false;
    }

    if (clickable != _clickable) {
      setState(() {
        _clickable = clickable;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorRes.bg_color,
      appBar: CustomAppBar(
        isBack: false,
        actionName:
            widget.disableAction == null ? S.of(context).login_guest : "",
        onPressed: () {
          if (widget.disableAction == null) {
            Navigator.pushReplacementNamed(context, RouteName.home);
          }
        },
      ),
//      body: CustomScrollViewWidget(
//        keyboardConfig:
//            Utils.getKeyboardActionsConfig(context, <FocusNode>[_nodeText1]),
//        crossAxisAlignment: CrossAxisAlignment.center,
//        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
//        children: _body(),
//      ),
//    );
      body: LoginScreen(),
    );
  }

  List<Widget> _body() {
    return <Widget>[
      Gaps.vGap40,
      Container(
        alignment: Alignment.center,
        child: LoadImage(
          Constant.assetsImages + "logo",
          width: ScreenUtil().setWidth(100),
          height: ScreenUtil().setWidth(100),
        ),
      ),
      Gaps.vGap24,
      Text(
        S.of(context).create_account,
        style: TextStyle(
            color: ColorRes.black_text,
            fontSize: ScreenUtil().setSp(24),
            fontWeight: FontWeight.bold),
      ),
      Gaps.vGap16,
      Text(
        S.of(context).registration_page_description,
        style: TextStyle(
            fontSize: ScreenUtil().setSp(16), color: ColorRes.text_gray),
      ),
      Gaps.vGap32,
      Directionality(
        textDirection: TextDirection.ltr,
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(16),
          ),
          child: Row(
            children: [
              Card(
                margin: EdgeInsetsDirectional.only(
                  end: ScreenUtil().setWidth(10),
                ),
                child: CountryDropdown(
                  onSubmitPressed: (data) {
                    _selectedCountry = data;
                  },
                ),
              ),
              Expanded(
                child: _inputPhoneTextField(),
              ),
            ],
          ),
        ),
      ),
      Gaps.vGap64,
      _purpleButton(S.of(context).activation),
      Gaps.vGap32,
      _alertRowButton(
          S.of(context).already_have_an_account, S.of(context).login),
      Gaps.vGap24,
      Text(
        S.of(context).information_vas,
        style: TextStyle(
            fontSize: ScreenUtil().setSp(14), color: ColorRes.text_gray),
      ),
      Gaps.vGap24,
      InkWell(
          onTap: () {
            NavigatorUtils.push(context, screen: LanguagePage());
          },
          child: Text(
            S.of(context).change_language,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(14), color: ColorRes.purpleColor),
          )),
    ];
  }

  Widget _inputPhoneTextField() {
    return CustomTextField(
      focusNode: _nodeText1,
      keyboardType: TextInputType.number,
      controller: _phoneController,
      hintText: S.of(context).phone_number_hint,
    );
  }

  Widget _purpleButton(String text) {
    final provider = Provider.of<LoginProvider>(context);
    return CustomButton(
      onPressed: _clickable ? _register : null,
      text: text,
      radius: 14,
      child: provider.isLoading ? ButtonProgressIndicator() : null,
    );
  }

  Widget _alertRowButton(String firsText, String secondText) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          firsText,
          style: TextStyle(
              fontSize: ScreenUtil().setSp(15), color: ColorRes.text_gray),
        ),
        Gaps.hGap4,
        InkWell(
            onTap: () {
              NavigatorUtils.push(context, screen: LoginScreen());
            },
            child: Text(
              secondText,
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(16),
                  color: ColorRes.purpleColor),
            )),
      ],
    );
  }

  void _register() {
    String mobile =
        _selectedCountry.name.replaceAll("+", "") + _phoneController.text;
    var provider = Provider.of<LoginProvider>(context, listen: false);
    provider.setRefresh();
    _clickable = false;
    provider.register(mobile, loadingCallBack: (bool) {},
        successCallBack: ({dynamic data}) {
      provider.setSuccess();
      _clickable = true;
      LoginResponse loginResponse = data;
      if (loginResponse.accessToken != null) {
        provider.saveToStorage(loginResponse, mobile);
        Navigator.of(context).pushNamedAndRemoveUntil(
            RouteName.home, (Route<dynamic> route) => false);
        Provider.of<MainProvider>(context, listen: false).checkIsLogin();
      } else {
        NavigatorUtils.push(context, screen: VerificationCodeScreen(mobile));
      }
    }, errorCallBack: (result) {
      _clickable = true;
      provider.setError(result);
      Toast.show(result.data);
    });
  }
}
