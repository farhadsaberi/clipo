import 'package:clipo/entity/forgot_password_response.dart';
import 'package:clipo/features/login/ui/verification_code_screen.dart';
import 'package:clipo/generated/l10n.dart';
import 'package:clipo/res/colors.dart';
import 'package:clipo/res/gaps.dart';
import 'package:clipo/utils/change_notifier_manage.dart';
import 'package:clipo/utils/navigator.dart';
import 'package:clipo/utils/toast.dart';
import 'package:clipo/utils/utils.dart';
import 'package:clipo/utils/widgets/button_progress_indicator.dart';
import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:clipo/utils/widgets/custom_text_field.dart';
import 'package:clipo/utils/widgets/custom_button.dart';
import 'package:clipo/utils/widgets/custom_scroll_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';

import 'login_provider.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<ForgotPasswordScreen>
    with ChangeNotifierMixin<ForgotPasswordScreen> {
  final TextEditingController _phoneController = TextEditingController();
  final FocusNode _nodeText1 = FocusNode();
  bool _clickable = false;

  @override
  Map<ChangeNotifier, List<VoidCallback>> changeNotifier() {
    final List<VoidCallback> callbacks = [_verify];
    return {
      _phoneController: callbacks,
      _nodeText1: null,
    };
  }

  void _verify() {
    final String name = _phoneController.text;
    bool clickable = true;
    if (name.isEmpty || name.length < 10) {
      clickable = false;
    }

    if (clickable != _clickable) {
      setState(() {
        _clickable = clickable;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: S.of(context).back,
      ),
      body: CustomScrollViewWidget(
        keyboardConfig:
            Utils.getKeyboardActionsConfig(context, <FocusNode>[_nodeText1]),
        crossAxisAlignment: CrossAxisAlignment.center,
        padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 20.0),
        children: _body(),
      ),
    );
  }

  List<Widget> _body() {
    return <Widget>[
      Gaps.vGap64,
      Gaps.vGap64,
      Text(
        S.of(context).forgot_password,
        style: TextStyle(
            fontSize: ScreenUtil().setSp(32), fontWeight: FontWeight.bold),
      ),
      Gaps.vGap16,
      Text(
        S.of(context).enter_your_mobile_code,
        textAlign: TextAlign.start,
        style: TextStyle(
            fontSize: ScreenUtil().setSp(16), color: ColorRes.text_gray),
      ),
      Gaps.vGap32,
      Gaps.vGap32,
      Padding(
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(16),
        ),
        child: _inputPhoneTextField(),
      ),
      Gaps.vGap64,
      Gaps.vGap64,
      Gaps.vGap64,
      Gaps.vGap32,
      _continueButton(S.of(context).continue_),
    ];
  }

  Widget _inputPhoneTextField() {
    return CustomTextField(
      focusNode: _nodeText1,
      keyboardType: TextInputType.number,
      controller: _phoneController,
      hintText: S.of(context).phone_number,
    );
  }

  Widget _continueButton(String text) {
    final provider = Provider.of<LoginProvider>(context);
    return CustomButton(
      onPressed: _clickable ? _verification : null,
      text: text,
      radius: 14,
      child: provider.isLoading ? ButtonProgressIndicator() : null,
    );
  }

  void _verification() {
    var provider = Provider.of<LoginProvider>(context, listen: false);
    provider.setRefresh();
    _clickable = false;
    provider.forgotPassword(
        Utils.removeFirstCharactersMobile(_phoneController.text),
        loadingCallBack: (bool) {}, successCallBack: ({dynamic data}) {
      provider.setSuccess();
      _clickable = true;
      ForgotPasswordResponse result = data;
      NavigatorUtils.push(context,
          screen: VerificationCodeScreen(
            _phoneController.text,
            code: result.password,
          ));
    }, errorCallBack: (result) {
      _clickable = true;
      provider.setError(result);
      Toast.show(result.data);
    });
  }
}
