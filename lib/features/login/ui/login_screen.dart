import 'package:clipo/config/constant.dart';
import 'package:clipo/entity/country_item.dart';
import 'package:clipo/entity/login_response.dart';
import 'package:clipo/features/home/provider/main_provider.dart';
import 'package:clipo/features/language/language_page.dart';
import 'package:clipo/features/login/ui/forgot_password_screen.dart';
import 'package:clipo/features/login/ui/login_provider.dart';
import 'package:clipo/features/login/ui/register_screen.dart';
import 'package:clipo/generated/l10n.dart';
import 'package:clipo/res/colors.dart';
import 'package:clipo/res/gaps.dart';
import 'package:clipo/routers/router_manger.dart';
import 'package:clipo/utils/change_notifier_manage.dart';
import 'package:clipo/utils/navigator.dart';
import 'package:clipo/utils/toast.dart';
import 'package:clipo/utils/utils.dart';
import 'package:clipo/utils/widgets/button_progress_indicator.dart';
import 'package:clipo/utils/widgets/country_dropdown.dart';
import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:clipo/utils/widgets/custom_text_field.dart';
import 'package:clipo/utils/widgets/custom_button.dart';
import 'package:clipo/utils/widgets/custom_scroll_view.dart';
import 'package:clipo/utils/widgets/load_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';

import 'forgot_password_2_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<LoginScreen>
    with ChangeNotifierMixin<LoginScreen> {
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FocusNode _nodeText1 = FocusNode();
  final FocusNode _nodeText2 = FocusNode();
  bool _clickable = false;

  CountryItem _selectedCountry = Utils.countryItems()[0];

  @override
  Map<ChangeNotifier, List<VoidCallback>> changeNotifier() {
    final List<VoidCallback> callbacks = [_verify];
    return {
      _phoneController: callbacks,
      _passwordController: callbacks,
      _nodeText1: null,
      _nodeText2: null,
    };
  }

  void _verify() {
    final String name = _phoneController.text;
    final String password = _passwordController.text;
    bool clickable = true;
    if (name.isEmpty || name.length < 8) {
      clickable = false;
    }

    if (password.isEmpty || !(password.length > 3)) {
      clickable = false;
    }

    if (clickable != _clickable) {
      setState(() {
        _clickable = clickable;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorRes.bg_color,
//      appBar: CustomAppBar(
//        isBack: false,
//      ),
      body: CustomScrollViewWidget(
        keyboardConfig: Utils.getKeyboardActionsConfig(
            context, <FocusNode>[_nodeText1, _nodeText2]),
        crossAxisAlignment: CrossAxisAlignment.center,
        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
        children: _body(),
      ),
    );
  }

  List<Widget> _body() {
    final provider = Provider.of<LoginProvider>(context);
    return <Widget>[
      Gaps.vGap40,
      Gaps.vGap40,
      Container(
        alignment: Alignment.center,
        child: LoadImage(
          Constant.assetsImages + "logo",
          width: ScreenUtil().setWidth(100),
          height: ScreenUtil().setWidth(100),
        ),
      ),
      Gaps.vGap24,
      Text(
        S.of(context).login,
        style: TextStyle(
            color: ColorRes.black_text,
            fontSize: ScreenUtil().setSp(24),
            fontWeight: FontWeight.bold),
      ),
      Gaps.vGap32,
      Directionality(
        textDirection: TextDirection.ltr,
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(16),
          ),
          child: Row(
            children: [
             SizedBox(
               width: ScreenUtil().setWidth(130),
               child: Card(
                 margin: EdgeInsetsDirectional.only(
                   end: ScreenUtil().setWidth(10),
                 ),
                 child: CountryDropdown(
                   onSubmitPressed: (data) {
                     _selectedCountry = data;
                   },
                 ),
               ),
             ),
              Expanded(
                child: _inputPhoneTextField(),
              ),
            ],
          ),
        ),
      ),
      Gaps.vGap8,
      Directionality(
        textDirection: TextDirection.ltr,
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(16),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
             SizedBox(
               width: ScreenUtil().setWidth(130),
             ),
              Expanded(
                child: _passwordTextFiled(),
              ),
            ],
          ),
        ),
      ),
      Gaps.vGap32,
      Gaps.vGap32,
      CustomButton(
        onPressed: _clickable ? _login : null,
        text: S.of(context).login,
        radius: 14,
        child: provider.isLoading ? ButtonProgressIndicator() : null,
      ),
      Gaps.vGap32,
      _alertRowButton(
          S.of(context).forgot_password, S.of(context).recover_it),
      Gaps.vGap32,
      InkWell(
          onTap: () {
            NavigatorUtils.push(context, screen: LanguagePage());
          },
          child: Text(
            S.of(context).change_language,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(16), color: ColorRes.purpleColor),
          )),
    ];
  }

  Widget _inputPhoneTextField() {
    return CustomTextField(
      focusNode: _nodeText1,
      keyboardType: TextInputType.number,
      controller: _phoneController,
      hintText: S.of(context).username_register,
    );
  }

  Widget _passwordTextFiled() {
    return CustomTextField(
      focusNode: _nodeText2,
      isInputPwd: true,
      keyboardType: TextInputType.number,
      controller: _passwordController,
      maxLength: 6,
      hintText: S.of(context).password_hint,
    );
  }

  Widget _alertRowButton(String firsText, String secondText) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          firsText,
          style: TextStyle(
              fontSize: ScreenUtil().setSp(15), color: ColorRes.text_gray),
        ),
        Gaps.hGap4,
        InkWell(
            onTap: () {
              NavigatorUtils.push(context, screen: ForgotPassword2Screen());
            },
            child: Text(
              secondText,
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(15),
                  color: ColorRes.purpleColor),
            )),
      ],
    );
  }

  void _login() {
    String mobile =
       _selectedCountry.name.replaceAll("+", "") +
            _phoneController.text;
    String password = _passwordController.text;
    var provider = Provider.of<LoginProvider>(context, listen: false);
    provider.setRefresh();
    _clickable = false;
    provider.loginWithMsinsdnAndPassword(mobile, password, loadingCallBack: (bool) {},
        successCallBack: ({dynamic data}) {
      provider.setSuccess();
      _clickable = true;
      LoginResponse loginResponse = data as LoginResponse;
      provider.saveToStorage(loginResponse, mobile, password: password);
      Navigator.of(context).pushNamedAndRemoveUntil(
          RouteName.home, (Route<dynamic> route) => false);
      Provider.of<MainProvider>(context, listen: false).checkIsLogin();
    }, errorCallBack: (result) {
      _clickable = true;
      provider.setError(result);
      if (result.code == 406) {
        Toast.show(S.of(context).error_phone_or_password);
      } else {
        Toast.show(result.data);
      }
    });
  }

  void _forgotPassword() {
    NavigatorUtils.push(context, screen: ForgotPasswordScreen());
  }
}
