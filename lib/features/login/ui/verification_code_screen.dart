import 'package:clipo/config/constant.dart';
import 'package:clipo/entity/login_response.dart';
import 'package:clipo/features/home/provider/main_provider.dart';
import 'package:clipo/generated/l10n.dart';
import 'package:clipo/local/local_storage.dart';
import 'package:clipo/res/colors.dart';
import 'package:clipo/res/gaps.dart';
import 'package:clipo/routers/router_manger.dart';
import 'package:clipo/utils/change_notifier_manage.dart';
import 'package:clipo/utils/toast.dart';
import 'package:clipo/utils/utils.dart';
import 'package:clipo/utils/widgets/button_progress_indicator.dart';
import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:clipo/utils/widgets/custom_scroll_view.dart';
import 'package:clipo/utils/widgets/custom_text_field.dart';
import 'package:clipo/utils/widgets/custom_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';

import 'login_provider.dart';

class VerificationCodeScreen extends StatefulWidget {
  final String phoneNumber;
  String code;

  VerificationCodeScreen(this.phoneNumber, {this.code});

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<VerificationCodeScreen>
    with ChangeNotifierMixin<VerificationCodeScreen> {
  final TextEditingController _codeController = TextEditingController();
  final FocusNode _nodeText1 = FocusNode();
  bool _clickable = false;

  @override
  Map<ChangeNotifier, List<VoidCallback>> changeNotifier() {
    final List<VoidCallback> callbacks = [_verify];
    return {
      _codeController: callbacks,
      _nodeText1: null,
    };
  }

  void _verify() {
    final String name = _codeController.text;
    bool clickable = true;
    if (name.isEmpty || !(name.length > 3)) {
      clickable = false;
    }

    if (clickable != _clickable) {
      setState(() {
        _clickable = clickable;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: S.of(context).back,
      ),
      body: CustomScrollViewWidget(
        keyboardConfig:
            Utils.getKeyboardActionsConfig(context, <FocusNode>[_nodeText1]),
        crossAxisAlignment: CrossAxisAlignment.center,
        padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 20.0),
        children: _body(),
      ),
    );
  }

  List<Widget> _body() {
    return <Widget>[
      Gaps.vGap64,
      Gaps.vGap64,
      Text(
        S.of(context).verification_code,
        style: TextStyle(
            fontSize: ScreenUtil().setSp(34), fontWeight: FontWeight.bold),
      ),
      Gaps.vGap16,
      Text(
        S.of(context).enter_code,
        style: TextStyle(
            fontSize: ScreenUtil().setSp(16), color: ColorRes.text_gray),
      ),
      Gaps.vGap32,
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            S.of(context).phone_number,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(15), color: ColorRes.black_text),
          ),
          Gaps.hGap4,
          Text(
            "+" + widget.phoneNumber ?? "",
            style: TextStyle(
                fontSize: ScreenUtil().setSp(15), color: ColorRes.black_text),
          )
        ],
      ),
      Gaps.vGap32,
      Padding(
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(16),
        ),
        child: _inputPhoneTextField(),
      ),
      Gaps.vGap64,
      Gaps.vGap64,
      Gaps.vGap64,
      Gaps.vGap32,
      _continueButton(S.of(context).continue_),
    ];
  }

  Widget _inputPhoneTextField() {
    return CustomTextField(
      focusNode: _nodeText1,
      keyboardType: TextInputType.number,
      controller: _codeController,
      vCodeText: S.of(context).try_again,
      maxLength: 6,
      hintText: S.of(context).code,
      getVCode: () async {
        _sendOtp();
        return true;
      },
    );
  }

  Widget _continueButton(String text) {
    final provider = Provider.of<LoginProvider>(context);
    return CustomButton(
      onPressed: _clickable ? _verificationCode : null,
      text: text,
      radius: 14,
      child: provider.isLoading ? ButtonProgressIndicator() : null,
    );
  }

  void _sendOtp() {
    var provider = Provider.of<LoginProvider>(context, listen: false);
    provider.setRefresh();
    _clickable = false;
    provider.register(Utils.removeFirstCharactersMobile(widget.phoneNumber),
        loadingCallBack: (bool) {}, successCallBack: ({dynamic data}) {
      widget.code = null;
      Toast.show(S.of(context).send_registration_code_again);
    }, errorCallBack: (result) {
      provider.setError(result);
      Toast.show(result.data);
    });
  }

  void _verificationCode() {
    if (widget.code != null && widget.code != _codeController.text) {
      Toast.show(S.of(context).error_code);
      return;
    }
    var provider = Provider.of<LoginProvider>(context, listen: false);
    provider.setRefresh();
    _clickable = false;
    provider.verificationCode(
        widget.phoneNumber, _codeController.text.toString(),
        loadingCallBack: (bool) {}, successCallBack: ({dynamic data}) {
      provider.setSuccess();
      _clickable = true;
      LoginResponse loginResponse = data as LoginResponse;
      provider.saveToStorage(loginResponse, widget.phoneNumber);
      Navigator.of(context).pushNamedAndRemoveUntil(
          RouteName.home, (Route<dynamic> route) => false);
      Provider.of<MainProvider>(context, listen: false).checkIsLogin();
    }, errorCallBack: (result) {
      _clickable = true;
      provider.setError(result);
      if (result.code == 406) {
        Toast.show(S.of(context).error_code);
      }
    });
  }
}
