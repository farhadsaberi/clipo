import 'package:clipo/config/constant.dart';
import 'package:clipo/features/login/ui/login_provider.dart';
import 'package:clipo/generated/l10n.dart';
import 'package:clipo/local/local_storage.dart';
import 'package:clipo/res/gaps.dart';
import 'package:clipo/res/strings.dart';
import 'package:clipo/utils/change_notifier_manage.dart';
import 'package:clipo/utils/toast.dart';
import 'package:clipo/utils/utils.dart';
import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:clipo/utils/widgets/custom_text_field.dart';
import 'package:clipo/utils/widgets/custom_button.dart';
import 'package:clipo/utils/widgets/custom_scroll_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';

class ChangePassword extends StatefulWidget {
  final bool disableAction;

  ChangePassword({this.disableAction});

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<ChangePassword>
    with ChangeNotifierMixin<ChangePassword> {
  final TextEditingController _phoneController1 = TextEditingController();
  final TextEditingController _phoneController2 = TextEditingController();
  final TextEditingController _phoneController3 = TextEditingController();
  final FocusNode _nodeText1 = FocusNode();
  final FocusNode _nodeText2 = FocusNode();
  final FocusNode _nodeText3 = FocusNode();
  bool _clickable = false;

  @override
  Map<ChangeNotifier, List<VoidCallback>> changeNotifier() {
    final List<VoidCallback> callbacks = [_verify];
    return {
      _phoneController1: callbacks,
      _phoneController2: callbacks,
      _phoneController3: callbacks,
      _nodeText1: null,
      _nodeText2: null,
      _nodeText3: null,
    };
  }

  void _verify() {
    final String name1 = _phoneController1.text;
    final String name2 = _phoneController2.text;
    final String name3 = _phoneController3.text;
    bool clickable = true;
    if (name1.isEmpty || !(name1.length > 3)) {
      clickable = false;
    }
    if (name2.isEmpty || !(name2.length > 3)) {
      clickable = false;
    }
    if (name3.isEmpty || !(name3.length > 3)) {
      clickable = false;
    }
    if (name2.trim() != name3.trim()) {
      clickable = false;
    }
    if (clickable != _clickable) {
      setState(() {
        _clickable = clickable;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        isBack: true,
        title: S.of(context).change_password,
      ),
      body: CustomScrollViewWidget(
        keyboardConfig:
            Utils.getKeyboardActionsConfig(context, <FocusNode>[_nodeText1]),
        crossAxisAlignment: CrossAxisAlignment.center,
        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
        children: _body(),
      ),
    );
  }

  List<Widget> _body() {
    return <Widget>[
      Gaps.vGap40,
      Padding(
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(16),
        ),
        child: _inputPhoneTextField(
            _phoneController1, _nodeText1, S.of(context).current_password_hint),
      ),
      Gaps.vGap15,
      Padding(
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(16),
        ),
        child: _inputPhoneTextField(
            _phoneController2, _nodeText2, S.of(context).new_password_hint),
      ),
      Gaps.vGap15,
      Padding(
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(16),
        ),
        child: _inputPhoneTextField(_phoneController3, _nodeText3,
            S.of(context).repeat_new_password_hint),
      ),
      Gaps.vGap32,
      _purpleButton(S.of(context).change_password),
      Gaps.vGap32,
    ];
  }

  Widget _inputPhoneTextField(
      TextEditingController controller, FocusNode focusNode, String hint) {
    return CustomTextField(
      focusNode: focusNode,
      keyboardType: TextInputType.number,
      controller: controller,
      hintText: hint,
    );
  }

  Widget _purpleButton(String text) {
//    final provider = Provider.of<LoginProvider>(context);
    return CustomButton(
      onPressed: _clickable ? _changePassword : null,
      text: text,
      radius: 14,
//      child: provider.isLoading ? ButtonProgressIndicator() : null,
    );
  }

  void _changePassword() async {
    String msisdn = await LocalStorage.get(Constant.MOBILE);
    Provider.of<LoginProvider>(context, listen: false)
        .changePassword(msisdn, _phoneController1.text, _phoneController2.text,
            _phoneController3.text)
        .then((value) {
      _clickable = true;
      Toast.show(S.of(context).change_password_successfully);
    }).catchError((error) {
      _clickable = true;
      Toast.show("Previous password is not correct");
    });
  }
}
