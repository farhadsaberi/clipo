import 'package:clipo/config/constant.dart';
import 'package:clipo/features/bookmark/bookmark_page.dart';
import 'package:clipo/features/home/provider/main_provider.dart';
import 'package:clipo/features/language/language_page.dart';
import 'package:clipo/features/login/ui/login_provider.dart';
import 'package:clipo/features/login/ui/login_screen.dart';
import 'package:clipo/features/profile/change_password.dart';
import 'package:clipo/features/profile/web_view_clipo.dart';
import 'package:clipo/generated/l10n.dart';
import 'package:clipo/local/local_storage.dart';
import 'package:clipo/provider/provider_widget.dart';
import 'package:clipo/res/colors.dart';
import 'package:clipo/res/gaps.dart';
import 'package:clipo/utils/dialog_helper.dart';
import 'package:clipo/utils/navigator.dart';
import 'package:clipo/utils/toast.dart';
import 'package:clipo/utils/utils.dart';
import 'package:clipo/utils/widgets/content_widget.dart';
import 'package:clipo/utils/widgets/comment_dialog.dart';
import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:clipo/utils/widgets/custom_scroll_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<MainProvider>(builder: (context, model, child) {
      if (model.isGuest) {
        return LoginScreen(
        );
      }
      return Scaffold(
          backgroundColor: ColorRes.bg_color,
          appBar: CustomAppBar(
            isBack: false,
            centerTitle: S.of(context).profile,
          ),
          body: _ProfileScreen());
    });
  }
}

class _ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    void showSuccessSendNickName() {
      Toast.show(S.of(context).nick_name_successfully_sent);
    }

    return CustomProviderWidget<LoginProvider>(
        reuseExisting: true,
        provider: LoginProvider(),
        onProviderReady: (provider) {},
        builder: (__, model, _) {
          return CustomScrollViewWidget(
            children: <Widget>[
              ListTile(
                title: Text(S.of(context).change_password),
                onTap: () {
                  NavigatorUtils.push(context, screen: ChangePassword());
                },
                leading: Icon(
                  Icons.lock,
                  color: ColorRes.dark_text_gray,
                ),
                trailing: Icon(Icons.chevron_right),
              ),
              ListTile(
                title: Text(S.of(context).submit_name),
                onTap: () {
                  showDialog(
                      context: context,
                      barrierDismissible: true,
                      builder: (context) {
                        return CommentDialog(
                          title: S.of(context).nick_name_description,
                          hint: S.of(context).username,
                          submitButton: S.of(context).submit,
                          onSubmitPressed: (String name) {
                            model.updateNikName(name).then((value) {
                              showSuccessSendNickName();
                            }).catchError((error) {});
                          },
                          onAlternativePressed: () {},
                        );
                      });
                },
                leading: Icon(
                  Icons.create,
                  color: ColorRes.dark_text_gray,
                ),
                trailing: Icon(Icons.chevron_right),
              ),
              ListTile(
                title: Text(S.of(context).language),
                onTap: () {
                  NavigatorUtils.push(context, screen: LanguagePage());
                },
                leading: Icon(
                  Icons.language,
                  color: ColorRes.dark_text_gray,
                ),
                trailing: Icon(Icons.chevron_right),
              ),
              ListTile(
                title: Text(S.of(context).bookmarked),
                onTap: () {
                  NavigatorUtils.push(context, screen: BookmarkPage());
                },
                leading: Icon(
                  Icons.bookmark,
                  color: ColorRes.dark_text_gray,
                ),
                trailing: Icon(Icons.chevron_right),
              ),
              ListTile(
                title: Text(S.of(context).about_us),
                onTap: () {
                  NavigatorUtils.push(context,
                      screen: getWidgetContent(S.of(context).about_us,
                          S.of(context).about_us_content));
                },
                leading: Icon(
                  Icons.supervisor_account,
                  color: ColorRes.dark_text_gray,
                ),
                trailing: Icon(Icons.chevron_right),
              ),
              ListTile(
                title: Text(S.of(context).user_privacy),
                onTap: () {
                  NavigatorUtils.push(context,
                      screen: getWidgetContent(S.of(context).user_privacy,
                          S.of(context).privacy_content));
                },
                leading: Icon(
                  Icons.security,
                  color: ColorRes.dark_text_gray,
                ),
                trailing: Icon(Icons.chevron_right),
              ),
              ListTile(
                title: Text(S.of(context).rules),
                onTap: () {
                  NavigatorUtils.push(context,
                      screen: getWidgetContent(
                          S.of(context).rules, S.of(context).policy_content));
                },
                leading: Icon(
                  Icons.pages,
                  color: ColorRes.dark_text_gray,
                ),
                trailing: Icon(Icons.chevron_right),
              ),
              ListTile(
                title: Text(S.of(context).unsubscribe),
                onTap: () {
//                  NavigatorUtils.push(context,
//                      withNavBar: false,
//                      screen: WebViewClipo());
                  Utils.openLink("http://he.clipo-app.com/unsub/request/1313/${Utils.getLanguageWebsite()}");
                  },
                leading: Icon(
                  Icons.indeterminate_check_box,
                  color: ColorRes.dark_text_gray,
                ),
                trailing: Icon(
                  Icons.chevron_right,
                  color: ColorRes.text_gray,
                ),
              ),
//              ListTile(
//                title: Text(
//                  S.of(context).unsubscribe,
//                ),
//                onTap: () {
//                  showDialog(
//                      context: context,
//                      barrierDismissible: true,
//                      builder: (contextDialog) {
//                        return UnsubscribeDialog(
//                          title: S.of(context).unsubscribe_message,
//                          submitButton: S.of(context).unsubscribe,
//                          onAlternativePressed: () {
//                            model.unSubscribe().then((value) {
//                              LocalStorage.remove(Constant.TOKEN_KEY);
//                              Provider.of<MainProvider>(context, listen: false)
//                                  .checkIsLogin();
//                            }).catchError((error) {
//                              if (error is ResultData) {
//                                Toast.show(error.data);
//                              } else {
////                                Toast.show(error.toString());
//                              }
//                            });
//                          },
//                        );
//                      });
//                },
//                leading: Icon(
//                  Icons.indeterminate_check_box,
//                  color: ColorRes.dark_text_gray,
//                ),
//                trailing: Icon(
//                  Icons.chevron_right,
//                  color: ColorRes.text_gray,
//                ),
//              ),
              Gaps.vGap32,
              Divider(),
              ListTile(
                title: Text(S.of(context).exit),
                onTap: () async {
                  if (await DialogHelper.showLoginDialog(
                      context,
                      S.of(context).are_u_sure_exit,
                      S.of(context).yes,
                      S.of(context).no)) {
                    LocalStorage.remove(Constant.TOKEN_KEY);
                    Provider.of<MainProvider>(context, listen: false)
                        .checkIsLogin();
                  }
                },
                leading: Icon(
                  Icons.exit_to_app,
                  color: ColorRes.dark_text_gray,
                ),
              ),
            ],
          );
        });
  }

  Widget getWidgetContent(String title, String content) {
    return ContentWidget(
      title: title,
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Text(content, style: TextStyle(height: 1.5)),
      ),
    );
  }
}
