import 'package:cached_network_image/cached_network_image.dart';
import 'package:clipo/config/constant.dart';
import 'package:clipo/entity/content_response.dart';
import 'package:clipo/entity/content_serie.dart';
import 'package:clipo/entity/content_serie_part.dart';
import 'package:clipo/features/contentserie/content_serie_page.dart';
import 'package:clipo/features/contentserie/parts/content_serie_parts_page.dart';
import 'package:clipo/features/home/provider/main_provider.dart';
import 'package:clipo/features/login/ui/login_provider.dart';
import 'package:clipo/features/login/ui/register_screen.dart';
import 'package:clipo/features/player/player_provider.dart';
import 'package:clipo/generated/l10n.dart';
import 'package:clipo/provider/view_state_widget.dart';
import 'package:clipo/res/colors.dart';
import 'package:clipo/res/gaps.dart';
import 'package:clipo/utils/dialog_helper.dart';
import 'package:clipo/utils/image_utils.dart';
import 'package:clipo/utils/navigator.dart';
import 'package:clipo/utils/toast.dart';
import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:clipo/utils/widgets/custom_scroll_view.dart';
import 'package:clipo/utils/widgets/item_content_player.dart';
import 'package:clipo/utils/widgets/load_image.dart';
import 'package:clipo/utils/widgets/player_widget.dart';
import 'package:clipo/utils/widgets/rate_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class PlayerPage extends StatelessWidget {
  final int contentId;
  final int contentSerie;
  final String title;

  PlayerPage(this.title, this.contentSerie, {this.contentId});

  PlayerProvider playerProvider = PlayerProvider();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<PlayerProvider>(
        create: (context) =>
            PlayerProvider()..initData(contentSerie, contentId: contentId),
        child: Consumer<PlayerProvider>(builder: (context, model, child) {
          if (model.isLoading) {
            return Scaffold(body: ViewStateLoadingWidget());
          } else if (model.isError && model.contentResponse == null) {
            return Scaffold(
              body: ViewStateErrorWidget(
                  error: model.viewStateError,
                  onPressed: () {
                    model.getData();
                  }),
            );
          } else if (model.isEmpty) {
            return Scaffold(
              body: ViewStateEmptyWidget(onPressed: () {
                model.getData();
              }),
            );
          }
          return Scaffold(
            backgroundColor: ColorRes.withe,
            appBar: model.hideToolbar
                ? null
                : CustomAppBar(
                    isBack: true,
                    title: model.currentTitleToolbar,
                  ),
            body: Column(
              children: <Widget>[
                PlayerWidget(),
                Expanded(
                  child: CustomScrollViewWidget(
                    children: <Widget>[
                      _ExpansionWidget(),
                      Visibility(
                        visible: !model.isCollapsed,
                        child: _FavoriteWidget(),
                      ),
                      _CommentWidget(contentId),
                      _RelatedTitle(),
                      Gaps.vGap8,
                      _RelatedList(),
                    ],
                  ),
                ),
              ],
            ),
          );
        }));
  }
}

class _CommentWidget extends StatelessWidget {
  final int contentId;

  _CommentWidget(this.contentId);

  @override
  Widget build(BuildContext context) {
    final TextEditingController _phoneController = TextEditingController();
    final FocusNode _nodeText1 = FocusNode();
    Widget _inputPhoneTextField() {
      return Container(
        child: new ConstrainedBox(
          constraints: BoxConstraints(
            maxHeight: 300.0,
          ),
          child: TextField(
            focusNode: _nodeText1,
            maxLines: null,
            controller: _phoneController,
            decoration: InputDecoration(
              hintText: S.of(context).write_comment,
              hintStyle: TextStyle(fontSize: 14),
            ),
          ),
        ),
      );
    }

    void showDialogLogin() async {
      try {
        if (await DialogHelper.showLoginDialog(
            context,
            S.of(context).need_to_sign_in,
            S.of(context).confirm,
            S.of(context).cancel)) {
          NavigatorUtils.push(context,
              withNavBar: false,
              screen: RegisterScreen(
                disableAction: true,
              ));
        }
      } catch (e) {}
    }

    return Consumer2<MainProvider, PlayerProvider>(
      builder: (_, model, player, __) {
        return Container(
          margin: EdgeInsets.only(left: 8, right: 8, top: 12),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text(
                    S.of(context).comments,
                    maxLines: 1,
                    style: TextStyle(
                      color: ColorRes.black_text,
                      fontWeight: FontWeight.bold,
                      fontSize: ScreenUtil().setSp(18),
                    ),
                  ),
                  Expanded(
                    child: SizedBox.shrink(),
                  ),
                  InkWell(
                    onTap: () {
                      _commentDialog(
                          context, player.contentResponse.pagedComments.items);
                    },
                    child: Text(
                      S.of(context).read_all,
                      maxLines: 1,
                      style: TextStyle(
                        color: ColorRes.text_gray,
                        fontStyle: FontStyle.normal,
                        fontSize: ScreenUtil().setSp(14),
                      ),
                    ),
                  )
                ],
              ),
              Gaps.vGap32,
              IntrinsicHeight(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
//                      height: ScreenUtil().setHeight(40),
                        width: ScreenUtil().setWidth(200),
                        alignment: Alignment.center,
                        padding: const EdgeInsets.only(
                            left: 10, right: 10, top: 5, bottom: 5),
                        decoration: BoxDecoration(
                            color: Color(0xffEEEEEE),
                            borderRadius:
                                BorderRadius.all(Radius.circular(4.0)),
                            border: Border.all(
                                color: Color(0xffEEEEEE), width: 1.5)),
                        child: _inputPhoneTextField()),
                    InkWell(
                      onTap: () {
                        if (!model.isGuest) {
                          if (_phoneController.text.length > 3) {
                            Provider.of<LoginProvider>(context, listen: false)
                                .sendComment(
                                    player
                                        .contentResponse.content.contentSerieId
                                        .toString(),
                                    _phoneController.text)
                                .then((value) {
                              Toast.show(
                                  S.of(context).comment_successfully_sent);
                            }).catchError((error) {
                              Toast.show(
                                  S.of(context).error_dto_default_message);
                            });
                          } else {
                            Toast.show(S.of(context).error_comment_length_less);
                          }
                        } else {
                          showDialogLogin();
                        }
                      },
                      child: Container(
                          height: ScreenUtil().setHeight(30),
                          width: ScreenUtil().setWidth(80),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: ColorRes.purpleColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.0)),
                              border: Border.all(
                                  color: ColorRes.purpleColor, width: 1.5)),
                          child: Text(
                            S.of(context).send,
                            style: TextStyle(
                                fontSize: ScreenUtil().setSp(12),
                                color: ColorRes.withe,
                                fontWeight: FontWeight.bold),
                          )),
                    ),
                  ],
                ),
              ),
              Gaps.vGap32,
              Divider(
                color: ColorRes.text_gray,
              ),
            ],
          ),
        );
      },
    );
  }
}

class _FavoriteWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    void showDialogLogin() async {
      if (await DialogHelper.showLoginDialog(
          context,
          S.of(context).need_to_sign_in,
          S.of(context).confirm,
          S.of(context).cancel)) {
        NavigatorUtils.push(context,
            withNavBar: false,
            screen: RegisterScreen(
              disableAction: true,
            ));
      }
    }

    return Container(
        color: ColorRes.bg_gray,
        child: Consumer2<PlayerProvider, MainProvider>(
            builder: (_, model, mainProvider, __) {
          return Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      IconButton(
                        padding: EdgeInsets.all(10),
                        icon: Icon(
                          model.isRate ? Icons.star : Icons.star_border,
                          color: model.contentResponse.content.isUserLike
                              ? ColorRes.purpleColor
                              : ColorRes.text_gray,
                          size: 22.0,
                        ),
                        onPressed: () {
                          if (!mainProvider.isGuest) {
                            showDialog(
                                context: context,
                                barrierDismissible: true,
                                builder: (context) {
                                  return RatingDialog(
                                    icon: LoadImage(
                                      Constant.assetsImages + "/logo",
                                      width: ScreenUtil().setWidth(80),
                                      height: ScreenUtil().setWidth(80),
                                    ),
                                    title: "",
                                    description: "",
                                    submitButton: S.of(context).submit_rate,
                                    accentColor: ColorRes.purpleColor,
                                    onSubmitPressed: (int rating) {
                                      model.addRating(
                                          model.contentResponse.content
                                              .contentSerieId
                                              .toString(),
                                          rating.toDouble());
                                    },
                                    onAlternativePressed: () {},
                                  );
                                });
                          } else {
                            showDialogLogin();
                          }
                        },
                      ),
                      Text(
                        S.of(context).rate,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          model.isBookmark
                              ? Icons.bookmark
                              : Icons.bookmark_border,
                          color: model.contentResponse.content.isUserLike
                              ? ColorRes.purpleColor
                              : ColorRes.text_gray,
                          size: 22.0,
                        ),
                        onPressed: () {
                          if (!mainProvider.isGuest) {
                            model.addOrRemoveBookmark(
                                model.contentResponse.content.id.toString());
                          } else {
                            showDialogLogin();
                          }
                        },
                      ),
                      Text(
                        S.of(context).save,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Divider(
                color: ColorRes.text_gray,
              ),
            ],
          );
        }));
  }
}

class _ExpansionWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        color: ColorRes.bg_gray,
        child: Consumer<PlayerProvider>(
          builder: (_, model, __) {
            return Container(
              child: ExpansionTile(
                title: Text(
                  model.contentResponse.contentSerie.title +
                      "    " +
                      model.contentResponse.contentSerie.videoCounts
                          .toString() +
                      " " +
                      S.of(context).videos,
                  style: TextStyle(color: ColorRes.text_gray),
                ),
                onExpansionChanged: (value) {
                  model.setCollapsed(value);
                },
                backgroundColor: ColorRes.bg_gray_light,
                children: <Widget>[
                  ListView.separated(
                    controller: ScrollController(),
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    separatorBuilder: (context, index) {
                      return Divider();
                    },
                    itemCount:
                        model.contentResponse.pagedContentParts.items.length +
                            1,
                    itemBuilder: (_, index) {
                      if (index ==
                          model.contentResponse.pagedContentParts.items.length)
                        return _MoreCell();
                      return _Cell(
                        data: model
                            .contentResponse.pagedContentParts.items[index],
                        onTap: () {
                          model.setPLayer(
                              model.contentResponse.pagedContentParts
                                  .items[index].playLinks[0],
                              model.contentResponse.pagedContentParts
                                  .items[index].imageUrl,
                              model.contentResponse.pagedContentParts
                                  .items[index].title);

                          model.canPlayVideo(model.contentResponse
                                  .pagedContentParts.items[index].isPurchased ||
                              model.contentResponse.pagedContentParts
                                  .items[index].isFree);
                        },
                      );
                    },
                  )
                ],
              ),
            );
          },
        ));
  }
}

class _MoreCell extends StatelessWidget {
  List<ItemPart> getItemPartList(List<Content> contentList) {
    List<ItemPart> list = [];
    contentList.forEach((element) {
      list.add(ItemPart()
        ..id = element.id
        ..key = element.key
        ..title = element.title
        ..producer = element.producer
        ..imageUrl = element.imageUrl
        ..isFree = element.isFree
        ..isPurchased = element.isPurchased);
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<PlayerProvider>(context);
    return Container(
      width: ScreenUtil().setWidth(100),
      height: ScreenUtil().setHeight(56),
      child: InkWell(
        onTap: () {
          NavigatorUtils.push(context,
              custom: true,
              withNavBar: true,
              screen: ContentSeriePartsPage(
                  provider.contentResponse.contentSerie.title,
                  provider.contentResponse.contentSerie.id,
                  outPutData: getItemPartList(
                      provider.contentResponse.contentSerie.contents)));
        },
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
                ),
                width: ScreenUtil().setWidth(120),
                height: ScreenUtil().setHeight(120),
                child: Center(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          S.of(context).more,
                          style: TextStyle(fontSize: ScreenUtil().setSp(18)),
                        ),
                        Icon(Icons.arrow_forward,
                            size: ScreenUtil().setWidth(18))
                      ]),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _Cell extends StatelessWidget {
  final Item data;
  final Function onTap;

  const _Cell({@required this.data, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Row(
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(100),
            height: ScreenUtil().setHeight(56),
            margin: EdgeInsets.only(left: 4, right: 4, top: 4),
            decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(ScreenUtil().setWidth(7)),
              image: DecorationImage(
                fit: BoxFit.contain,
                image: ImageUtils.getImageProvider(data.imageUrl),
              ),
            ),
          ),
          Gaps.hGap12,
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  data.title,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  //textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(15),
                  ),
                ),
                Gaps.vGap4,
                Row(
                  children: <Widget>[
                    Text(
                      data.key,
                      maxLines: 2,
                      //textAlign: TextAlign.center,
                      style: TextStyle(
                        fontStyle: FontStyle.normal,
                        fontSize: ScreenUtil().setSp(13),
                      ),
                    ),
                    Expanded(
                      child: SizedBox.shrink(),
                    ),
                    Visibility(
                      visible: !data.isPurchased,
                      child: Container(
                        margin: EdgeInsets.only(right: 10, left: 10),
                        child: Icon(
                          Icons.lock,
                          color: ColorRes.purpleColor,
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class _RelatedTitle extends StatelessWidget {
  List<ItemContentSerie> getItemContentSerieList(List<Item> items) {
    List<ItemContentSerie> list = [];
    items.forEach((element) {
      list.add(ItemContentSerie()
        ..title = element.title
        ..key = element.key
        ..imageUrl = element.imageUrl
        ..id = element.id
        ..producer = element.producer);
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<PlayerProvider>(context);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(8)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            S.of(context).similar_courses,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(18), fontWeight: FontWeight.bold),
          ),
          GestureDetector(
              onTap: () => {
                    NavigatorUtils.push(context,
                        custom: true,
                        withNavBar: true,
                        screen: ContentSeriePage(
                          provider.contentResponse.pagedRelatedContentSeries
                              .items[0].title,
                          provider.contentResponse.pagedRelatedContentSeries
                              .items[0].key,
                          outPutData: getItemContentSerieList(provider
                              .contentResponse.pagedRelatedContentSeries.items),
                        ))
                  },
              child: Container(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, top: 5, bottom: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(100.0)),
                      border:
                          Border.all(color: ColorRes.purpleColor, width: 1.5)),
                  child: Text(
                    S.of(context).more,
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(12),
                        color: ColorRes.purpleColor,
                        fontWeight: FontWeight.bold),
                  )))
        ],
      ),
    );
  }
}

class _RelatedList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<PlayerProvider>(context);
    return Container(
      height: ScreenUtil().setHeight(130),
      child: ListView.separated(
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(15)),
        scrollDirection: Axis.horizontal,
//        physics: PageScrollPhysics(),
        shrinkWrap: true,
        separatorBuilder: (_, index) =>
            SizedBox(width: ScreenUtil().setWidth(15)),
        itemCount:
            provider.contentResponse.pagedRelatedContentSeries.items.length,
        itemBuilder: (_, index) {
          final data =
              provider.contentResponse.pagedRelatedContentSeries.items[index];
          return ItemContentPlayer(
              data: data,
              onTap: () {
                NavigatorUtils.push(context,
                    custom: true,
                    withNavBar: true,
                    screen: ContentSeriePartsPage(data.title, data.id));
              });
        },
      ),
    );
  }
}

Future _commentDialog(BuildContext ctx, List<Item> comments) async {
  await Navigator.of(ctx).push(
    PageRouteBuilder(
        barrierColor: const Color(0xAA000000),
        fullscreenDialog: true,
        barrierDismissible: true,
        opaque: false,
        pageBuilder: (context, animation, subAnimation) {
          return SlideTransition(
            position: Tween<Offset>(begin: Offset(0, 1), end: Offset(0, 0.3))
                .animate(
                    CurvedAnimation(parent: animation, curve: Curves.ease)),
            child: _buildComment(comments),
          );
        }),
  );
}

Widget _buildComment(List<Item> comments) {
  return Builder(
    builder: (context) {
      return Scaffold(
        backgroundColor: const Color(0x00000000),
        body: Container(
          height: ScreenUtil().uiHeightPx.toDouble(),
          decoration: BoxDecoration(
            color: const Color(0xFFFFFFFF),
            borderRadius: BorderRadius.vertical(
                top: Radius.circular(ScreenUtil().setWidth(15))),
          ),
          child: Stack(children: [
            _CommentPanel(
              comments: comments,
              height: ScreenUtil().setHeight(ScreenUtil().uiHeightPx * 0.7),
//              scrollController: state.scrollController,
            ),
          ]),
        ),
      );
    },
  );
}

class _CommentCell extends StatelessWidget {
  final Item comment;

  const _CommentCell({this.comment});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: ScreenUtil().setWidth(40),
            height: ScreenUtil().setWidth(40),
            child: Icon(Icons.account_circle, size: 40),
          ),
          Gaps.hGap10,
          SizedBox(
            width: ScreenUtil.screenWidth - ScreenUtil().setWidth(190),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      comment.userNickName,
                      style: TextStyle(
                        color: ColorRes.text_gray,
                        fontWeight: FontWeight.w600,
                        fontSize: ScreenUtil().setSp(15),
                      ),
                    ),
                    Spacer(),
                    Text(
                      "", // date
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        color: const Color(0xFF717171),
                      ),
                    ),
                  ],
                ),
                Gaps.vGap5,
                Text(comment.text),
                Gaps.vGap10,
                Divider()
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _CommentPanel extends StatelessWidget {
  final double height;
  final ScrollController scrollController;
  final List<Item> comments;

  const _CommentPanel({this.comments, this.height, this.scrollController});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(40),
        vertical: ScreenUtil().setWidth(10),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                S.of(context).comments,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(20),
                  fontWeight: FontWeight.bold,
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Icon(Icons.keyboard_arrow_down),
              ),
            ],
          )),
          Gaps.vGap24,
          Expanded(
            child: comments.length == 0
                ? Center(
                    child: Text(S.of(context).view_state_message_empty),
                  )
                : ListView.separated(
                    controller: scrollController,
                    padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(25)),
                    separatorBuilder: (_, __) =>
                        SizedBox(height: ScreenUtil().setWidth(15)),
                    itemCount: comments.length,
                    itemBuilder: (context, index) {
                      return _CommentCell(comment: comments[index]);
                    },
                  ),
          ),
        ],
      ),
    );
  }
}
