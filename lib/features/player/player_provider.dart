import 'package:clipo/entity/content_response.dart';
import 'package:clipo/provider/base_view_state.dart';
import 'package:clipo/repository/Repository.dart';
import 'package:clipo/utils/toast.dart';
import 'package:flutter/material.dart';

class PlayerProvider extends BaseViewState {
  Repository _commonRepository;
  ContentResponse _contentResponse;

  ContentResponse get contentResponse => _contentResponse;

  int contentId;
  int contentSerie = 0;

  bool _isCollapsed = false;
  bool _hideToolbar = false;
  bool _hasBookmark = false;
  bool _hasRate = false;
  bool _canPlay = false;

  PlayLink _latestPlayer;

  String _imageUrl;

  String _currentToolbar = "";

  bool get isCollapsed => _isCollapsed;

  bool get hideToolbar => _hideToolbar;

  bool get isBookmark => _hasBookmark;

  bool get canPlay => _canPlay;

  bool get isRate => _hasRate;

  PlayLink get latestPlayer => _latestPlayer;

  String get imageVideo => _imageUrl;

  String get currentTitleToolbar => _currentToolbar;

  void initData(int contentSerie, {int contentId}) {
    setRefresh();
    this.contentSerie = contentSerie;
    if (contentId != null) {
      this.contentId = contentId;
    }
    _commonRepository = Repository();
    getData(isRefresh: false);
  }

  void getData({bool isRefresh}) {
    _commonRepository
        .content(contentSerie.toString(), contentId: contentId)
        .then((result) {
      if (result != null) {
        _contentResponse = result;
        updateBookmark(_contentResponse.content.isBookmarked);
        setPLayer(contentResponse.content.playLinks[0],
            contentResponse.content.imageUrl, contentResponse.content.title);
        canPlayVideo(_contentResponse.content.isFree ||
            _contentResponse.content.isPurchased);
        setSuccess();
      } else {
        setEmpty();
      }
    }).catchError((e) {
      setError(e);
    });
  }

  void setPLayer(PlayLink playLink, String imageVideo, String title) {
    this._latestPlayer = playLink;
    this._imageUrl = imageVideo;
    this._currentToolbar = title;
    notifyListeners();
  }

  void canPlayVideo(bool play) {
    _canPlay = play;
    notifyListeners();
  }

  void setHideToolbar(bool hide) {
    this._hideToolbar = hide;
    notifyListeners();
  }

  void addOrRemoveBookmark(String contentId) {
    if (_hasBookmark) {
      _commonRepository.removeBookmark(contentId).then((value) {
        updateBookmark(false);
      }).catchError((error) {
        updateBookmark(false);
        Toast.show(error.data);
      });
    } else {
      _commonRepository.addBookmark(contentId).then((value) {
        updateBookmark(true);
      }).catchError((error) {
        Toast.show(error.data);
      });
    }
  }

  void addRating(String contentId, double rate) {
    _commonRepository.postContentSeriesRate(contentId, rate).then((value) {
      updateRating(true);
    }).catchError((error) {
      Toast.show(error.data);
    });
  }

  void updateBookmark(bool update) {
    this._hasBookmark = update;
    notifyListeners();
  }

  void updateRating(bool update) {
    this._hasRate = update;
    notifyListeners();
  }

  void setCollapsed(bool collapsed) {
    _isCollapsed = collapsed;
    notifyListeners();
  }
}
