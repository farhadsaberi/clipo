import 'package:clipo/config/constant.dart';
import 'package:clipo/local/local_storage.dart';
import 'package:flutter/material.dart';

class LanguageProvider extends ChangeNotifier {
  Locale _appLocale = Locale('en','');
  LanguageProvider(){
    fetchLocale();
  }

  Locale get appLocal => _appLocale ?? Locale("en",'');
  fetchLocale() async {
    if (await LocalStorage.get(Constant.LANGUAGE_CODE)== null) {
      _appLocale = Locale('en','');
      langID = 1;
      return Null;
    }
    _appLocale = Locale(await LocalStorage.get(Constant.LANGUAGE_CODE),'');

    if (_appLocale == Locale("ar")) {
      langID = 2;
    } else {
      langID = 1;
    }
    notifyListeners();

    return Null;
  }


  void changeLanguage(Locale type) async {
    if (_appLocale == type) {
      return;
    }
    if (type == Locale("ar")) {
      _appLocale = Locale("ar");
      await LocalStorage.save(Constant.LANGUAGE_CODE, 'ar');
      langID = 2;
    } else {
      _appLocale = Locale("en");
      await LocalStorage.save(Constant.LANGUAGE_CODE, 'en');
      langID = 1;
    }
    notifyListeners();
  }
}
