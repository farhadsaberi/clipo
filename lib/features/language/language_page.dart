import 'package:clipo/config/constant.dart';
import 'package:clipo/features/language/language_provider.dart';
import 'package:clipo/generated/l10n.dart';
import 'package:clipo/utils/widgets/custom_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LanguagePage extends StatefulWidget {
  @override
  _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {
  List<String> _list = [];

  @override
  Widget build(BuildContext context) {
    _list = [S.of(context).lang_english, S.of(context).lang_arabic];
    String langMode;
    switch (langID) {
      case 1:
        langMode = S.of(context).lang_english;
        break;
      case 2:
        langMode = S.of(context).lang_arabic;
        break;
      default:
        langMode = S.of(context).lang_english;
        break;
    }
    return Scaffold(
      appBar: CustomAppBar(
        title: S.of(context).language,
      ),
      body: ListView.separated(
        itemCount: _list.length,
        separatorBuilder: (_, index) => const Divider(),
        itemBuilder: (_, index) {
          final model = Provider.of<LanguageProvider>(context);
          return InkWell(
            onTap: () {
              if (index==0) {
                model.changeLanguage(Locale("en"));
              }else{
                model.changeLanguage(Locale("ar"));
              }
            },
            child: Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              height: 50.0,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(_list[index]),
                  ),
                  Opacity(
                    opacity: langMode == _list[index]? 1 : 0,
                    child: Icon(Icons.done, color: Colors.blue),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
